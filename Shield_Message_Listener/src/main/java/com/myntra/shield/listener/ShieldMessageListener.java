package com.myntra.shield.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.myntra.shield.helper.Helper;
import com.myntra.shield.utils.Utils;

@Component
public class ShieldMessageListener implements MessageListener{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ShieldMessageListener.class);
	
	@Autowired
	Helper helper;
	
	@Override
	public void onMessage(Message message) {
		LOGGER.debug("Shield message received...");
		com.myntra.shield.messages.Message msg = Utils.getShieldMessage(message);
		if(msg != null)
			helper.writeToDB(msg);
		
		
	}

}
