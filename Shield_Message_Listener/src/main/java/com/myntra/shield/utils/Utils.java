package com.myntra.shield.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;

public class Utils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

	public static com.myntra.shield.messages.Message getShieldMessage(Message message) {
		String body = new String(message.getBody());
		LOGGER.debug("Shield message: " + body);
		
		try{
			GsonBuilder gsonBuilder = new GsonBuilder();
			Gson gson = gsonBuilder.create();
			return gson.fromJson(body, com.myntra.shield.messages.Message.class);
		}catch(JsonSyntaxException e){
			LOGGER.error("Exception caught. Could not parse object");
		}
		return null;
	}

	public static String getTimestamp() {
		return new SimpleDateFormat("YYYY-MM-dd-HH:mm:ss").format(new Date());
	}

	public static String formatTimestap(String timestamp) throws ParseException {
		Date dt = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").parse(timestamp);
		String ts = new SimpleDateFormat("YYYY-MM-dd-HH:mm:ss").format(dt);
		return ts;
	}

}
