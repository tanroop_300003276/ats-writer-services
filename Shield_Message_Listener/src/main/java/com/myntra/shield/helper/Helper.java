package com.myntra.shield.helper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.myntra.shield.entities.CODThrottling;
import com.myntra.shield.entities.CODThrottlingIncBulk;
import com.myntra.shield.entities.EmailValidation;
import com.myntra.shield.entities.LinkedAccounts;
import com.myntra.shield.entities.ReturnAbusers;
import com.myntra.shield.enums.MessageType;
import com.myntra.shield.messages.Message;
import com.myntra.shield.messages.MessageDetails;
import com.myntra.shield.repositories.CODThrottlingIncBulkRepository;
import com.myntra.shield.repositories.CODThrottlingRepository;
import com.myntra.shield.repositories.EmailValidationRepository;
import com.myntra.shield.repositories.LinkedAccountsRepository;
import com.myntra.shield.repositories.ReturnAbusersRepository;
import com.myntra.shield.utils.Utils;

@Component
public class Helper {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Helper.class);
	
	@Autowired
	EmailValidationRepository emailValidationRepository;
	
	@Autowired
	LinkedAccountsRepository linkedAccountsRepository;
	
	@Autowired
	CODThrottlingRepository codThrottlingRepository;
	
	@Autowired
	ReturnAbusersRepository returnAbusersRepository;
	
	@Autowired
	CODThrottlingIncBulkRepository codThrottlingIncBulkRepository;

	public void writeToDB(Message message) {
		MessageType messageType = message.getMessageType();
		LOGGER.debug("Saving message: " + message.getMessageType() + " Message details: " + message.getMessageDetails().toString());
		
		if(messageType.equals(MessageType.EMAIL_VALIDATION))
			saveEmailValidationMessage(message.getMessageDetails());
		
		else if(messageType.equals(MessageType.LINKED_ACCOUNTS))
			saveLinkedAccountsMessage(message.getMessageDetails());
		
		else if(messageType.equals(MessageType.COD_THROTTLING_INCREMENTAL) || messageType.equals(MessageType.COD_THROTTLING_BULK))
			saveCODThrottlingMessageIncAndBulk(message.getMessageDetails());
		
		else if(messageType.equals(MessageType.RETURN_ABUSER))
			saveReturnAbusersMessage(message.getMessageDetails());
		
		else if(messageType.equals(MessageType.COD_THROTTLING))
			saveCODThrottlingMessage(message.getMessageDetails());
		
		else if(messageType.equals(MessageType.FREE_USER))
			saveFreeUserMessage(message.getMessageDetails());
	}

	private void saveFreeUserMessage(MessageDetails messageDetails) {
		LinkedAccounts linkedAccounts = new LinkedAccounts();
		linkedAccounts.setUidx(messageDetails.getUidx());
		linkedAccounts.setIsFreeUser(Boolean.valueOf(true));
		linkedAccounts.setTimestamp(Utils.getTimestamp());
		
		linkedAccountsRepository.save(linkedAccounts);
		
	}

	private void saveEmailValidationMessage(MessageDetails messageDetails) {
		
		EmailValidation emailValidation = new EmailValidation();
		emailValidation.setUidx(messageDetails.getUidx());
		emailValidation.setIsFakeEmail(messageDetails.getIsFakeEmail());
		emailValidation.setTimestamp(Utils.getTimestamp());
		
		emailValidationRepository.save(emailValidation);
		
	}
	
	private void saveLinkedAccountsMessage(MessageDetails messageDetails) {
		
		LinkedAccounts linkedAccounts = new LinkedAccounts();
		linkedAccounts.setUidx(messageDetails.getUidx());
		linkedAccounts.setLinkedTo(messageDetails.getLinkedTo());
		linkedAccounts.setLinkedBy(messageDetails.getLinkedBy());
		linkedAccounts.setAbuserType(messageDetails.getAbuserType().toString());
		linkedAccounts.setIsFreeUser(Boolean.valueOf(false));
		linkedAccounts.setTimestamp(Utils.getTimestamp());
		
		linkedAccountsRepository.save(linkedAccounts);
		
	}
	
	private void saveCODThrottlingMessage(MessageDetails messageDetails) {
		
		CODThrottling codThrottling = new CODThrottling();
		codThrottling.setUidx(messageDetails.getUidx());
		codThrottling.setMinCod(messageDetails.getMaxCod());
		codThrottling.setMaxCod(messageDetails.getMaxCod());
		codThrottling.setRtoPercentageByRevenue(messageDetails.getRtoPercentageByRevenue());
		codThrottling.setRtoPercentageByVolume(messageDetails.getRtoPercentageByVolume());
		codThrottling.setIsRTOAbuser(messageDetails.getIsRTOAbuser());
		codThrottling.setTimestamp(Utils.getTimestamp());
		
		codThrottlingRepository.save(codThrottling);
		
	}
	
	private void saveCODThrottlingMessageIncAndBulk(MessageDetails messageDetails) {
		CODThrottlingIncBulk codThrottlingIncBulk = new CODThrottlingIncBulk();
		codThrottlingIncBulk.setUidx(messageDetails.getUidx());
		codThrottlingIncBulk.setMinCod(messageDetails.getMinCod());
		codThrottlingIncBulk.setMaxCod(messageDetails.getMaxCod());
		codThrottlingIncBulk.setRtoPercentageByRevenue(messageDetails.getRtoPercentageByRevenue());
		codThrottlingIncBulk.setRtoPercentageByVolume(messageDetails.getRtoPercentageByVolume());
		codThrottlingIncBulk.setIsRtoAbuser(messageDetails.getIsRTOAbuser());
		codThrottlingIncBulk.setCodReason(messageDetails.getCodReason());
		codThrottlingIncBulk.setTimestamp(Utils.getTimestamp());
		
		codThrottlingIncBulkRepository.save(codThrottlingIncBulk);
		
	}
	
	private void saveReturnAbusersMessage(MessageDetails messageDetails) {
		
		ReturnAbusers returnAbusers = new ReturnAbusers();
		returnAbusers.setUidx(messageDetails.getUidx());
		returnAbusers.setIsReturnAbuser(messageDetails.getIsReturnAbuser());
		returnAbusers.setNetReturnPercentage(messageDetails.getNetReturnPercentage());
		returnAbusers.setTimestamp(Utils.getTimestamp());
		
		returnAbusersRepository.save(returnAbusers);
		
	}

}
