package com.myntra.shield;

import java.util.List;
import javax.annotation.PostConstruct;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


import com.myntra.shield.entities.EmailValidation;
import com.myntra.shield.listener.ShieldMessageListener;
import com.myntra.shield.repositories.EmailValidationRepository;

@Configuration
public class AppConfig {

	@Value("${queue.queueName}")
    private String shieldMessageQueue;
	
	@Autowired
	ShieldMessageListener shieldMessageListener;
	
	@Autowired
	EmailValidationRepository emailValidationRepository;
	
	
	
	
//	@Value("${datasource.dbUrl}")
//    String dbUrl;
//	
//    @Value("${datasource.dbDriver}")
//    String dbDriver;
//    
//    @Value("${datasource.dbUsername}")
//    String dbUsername;
//    
//    @Value("${datasource.dbPassword}")
//    String dbPassword;
	
    @Bean
    SimpleMessageListenerContainer shieldMessageContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(shieldMessageQueue);
        container.setMessageListener(shieldMessagelistenerAdapter());
        return container;
    }
    

    @Bean
    MessageListenerAdapter shieldMessagelistenerAdapter() {
        return new MessageListenerAdapter(shieldMessageListener, "onMessage");
    }
	
	
    @PostConstruct
	public void init(){
    	
//    	EmailValidation entity  = new EmailValidation();
//    	entity.setUidx("e1");
//    	entity.setIsFakeEmail(true);
//    	entity.setTimestamp(Utils.getTimestamp());
//    	
//    	emailValidationRepository.save(entity);
//    	
//    	
//    	entity.setUidx("e2");
//    	entity.setIsFakeEmail(true);
//    	entity.setTimestamp("2017-10-10-11:10:41");
//    	
//    	emailValidationRepository.save(entity);
//    	
//    	
//    	entity.setUidx("e3");
//    	entity.setIsFakeEmail(true);
//    	entity.setTimestamp("2017-10-01-11:10:41");
//    	emailValidationRepository.save(entity);
    	
    	List<EmailValidation> evList = emailValidationRepository.getByTimestamp("2017-09-01-11:10:41", "2017-10-13-11:30:41");
    	System.out.println(evList.size());
    	
    	//EmailValidation en = emailValidationRepository.findOne("e1");
    	//System.out.println(en.getTimestamp());
		
		
	}
       
    
}