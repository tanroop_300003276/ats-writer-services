package com.myntra.shield.queue;

import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.myntra.shield.enums.ShieldExceptionMessage;
import com.myntra.shield.exception.ShieldException;
import com.myntra.shield.messages.Message;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;

@Component
public class QueueClient {
	
	@Value("${queue.queueName}")
	String queueName;
	
	@Value("${queue.exchange}")
	String exchange;
	
	@Value("${queue.routingKey}")
	String routingKey;
	
	@Autowired
	RabbitTemplate rabbitTemplate;
	
	@Autowired
	ConnectionFactory connectionFactory;
	
	Queue queue;
	
	@PostConstruct
	public void init(){
		queue = new Queue(queueName);
	}
	
	public void sendFakeEmailMessage(Message message) throws ShieldException{
		
		if(message.getMessageType() == null ||
			message.getMessageDetails() == null ||
			message.getMessageDetails().getUidx() == null ||
			message.getMessageDetails().getIsFakeEmail() == null)
		
			throw new ShieldException(ShieldExceptionMessage.DATA_MISSING_EMAIL_VALIDATION.getMessage());
			
			
		sendMessage(message);
		
	}
	
	public void sendLinkedAccountsMessage(Message message) throws ShieldException{
		
		if(message.getMessageType() == null ||
				message.getMessageDetails() == null ||
				message.getMessageDetails().getUidx() == null ||
				message.getMessageDetails().getLinkedTo() == null ||
				message.getMessageDetails().getLinkedBy() == null ||
				message.getMessageDetails().getAbuserType() == null)
			
				throw new ShieldException(ShieldExceptionMessage.DATA_MISSING_LINKED_ACCOUNTS.getMessage());
				
		sendMessage(message);
		
	}
	
	public void sendReturnAbuserMessage(Message message) throws ShieldException{
		
		if(message.getMessageType() == null ||
				message.getMessageDetails() == null ||
				message.getMessageDetails().getUidx() == null ||
				message.getMessageDetails().getNetReturnPercentage() == null ||
				message.getMessageDetails().getIsReturnAbuser() == null )
			
				throw new ShieldException(ShieldExceptionMessage.DATA_MISSING_RETURN_ABUSER.getMessage());

		sendMessage(message);
		
	}
	
	public void sendCODThrottlingMessage(Message message) throws ShieldException{
		if(message.getMessageType() == null ||
				message.getMessageDetails() == null ||
				message.getMessageDetails().getUidx() == null ||
				message.getMessageDetails().getMinCod() == null ||
				message.getMessageDetails().getMaxCod() == null ||
				message.getMessageDetails().getRtoPercentageByRevenue() == null ||
				message.getMessageDetails().getRtoPercentageByVolume() == null ||
				message.getMessageDetails().getIsRTOAbuser() == null ||
				message.getMessageDetails().getCodReason() == null)
			
				throw new ShieldException(ShieldExceptionMessage.DATA_MISSING_COD_THROTTLING.getMessage());

		sendMessage(message);
	}
	
	public void sendFreeAccountMessage(Message message) throws ShieldException{
		if(message.getMessageType() == null || 
				message.getMessageDetails() == null ||
				message.getMessageDetails().getUidx() == null)
			
			throw new ShieldException(ShieldExceptionMessage.DATA_MISSING_FREE_ACCOUNT.getMessage());
		
		sendMessage(message);
	}
	

	public void sendMessage(Message message) throws ShieldException{
		
		try{
			Gson gson = new Gson();
			String msg = gson.toJson(message);
			rabbitTemplate.convertAndSend(routingKey, msg);
		}catch(Exception e){
			throw new ShieldException(ShieldExceptionMessage.QUEUE_PUBLISH_ERROR.getMessage() + "Error returned is: " + e.getMessage());
		}
		
	}

}
