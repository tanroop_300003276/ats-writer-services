package com.myntra.shield.exception;

@SuppressWarnings("serial")
public class ShieldException extends Exception{
	
	public ShieldException(String message) {
        super(message);
    }


}
