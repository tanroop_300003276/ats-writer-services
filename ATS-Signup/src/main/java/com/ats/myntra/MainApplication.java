package com.ats.myntra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.myntra.ats.blocked.client.ATSClient;

@SpringBootApplication(exclude={HibernateJpaAutoConfiguration.class, FreeMarkerAutoConfiguration.class, DataSourceAutoConfiguration.class})
public class MainApplication extends SpringBootServletInitializer{
	
	@Value("${ats_service.baseUrl}")
	String baseUrl;
	
	@Value("${ats_service.authPassword}")
	String authPassword;
	
	@Value("${ats_service.authUser}")
	String authUser;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MainApplication.class);
	
	public static void main(String[] args){
		
		LOGGER.debug("Application context starting up...");
		SpringApplication.run(MainApplication.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(MainApplication.class);
	}
	
	@SuppressWarnings("static-access")
	@Bean
	public ATSClient atsClient(){
		ATSClient atsClient = new ATSClient();
		atsClient.setTimeout(6000);
		atsClient.setBaseUrl(baseUrl);
		atsClient.setAuthUser(authUser);
		atsClient.setAuthPassword(authPassword);
		return atsClient;
	}

}
