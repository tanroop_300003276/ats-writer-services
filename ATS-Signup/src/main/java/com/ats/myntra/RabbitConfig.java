package com.ats.myntra;

import javax.annotation.PostConstruct;

import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ats.myntra.signup.listener.EmailValidationListener;
import com.ats.myntra.signup.listener.LinkedAccountsListener;
import com.ats.myntra.statsd.StatsDClient;
import com.myntra.shield.queue.QueueClient;

@Configuration
public class RabbitConfig {

	@Value("${queue.emailvalidation}")
    private String emailvalidationqueue;
	
	@Value("${queue.linkedaccounts}")
    private String linkedaccountsqueue;
	
	@Autowired
	LinkedAccountsListener linkedAccountsListener;

	@Autowired
	EmailValidationListener emailValidationListener;
	
	@Autowired
	StatsDClient statsDClient;
	
    @Bean
    @ConditionalOnProperty(name="enabled.emailvalidation")
    SimpleMessageListenerContainer emailvalidationcontainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(emailvalidationqueue);
        container.setMessageListener(emailvalidationlistenerAdapter());
        return container;
    }
    
    @Bean
    @ConditionalOnProperty(name="enabled.linkedaccounts")
    SimpleMessageListenerContainer linkedaccountscontainer(ConnectionFactory connectionFactory,
            MessageListenerAdapter linkedaccountslistenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(linkedaccountsqueue);
        container.setMessageListener(linkedaccountslistenerAdapter);
        return container;
    }

    @Bean
    MessageListenerAdapter emailvalidationlistenerAdapter() {
        return new MessageListenerAdapter(emailValidationListener, "onMessage");
    }
    
    @Bean
    MessageListenerAdapter linkedaccountslistenerAdapter() {
        return new MessageListenerAdapter(linkedAccountsListener, "onMessage");
    }
    
    @PostConstruct
    void exceptionMetric(){
    	statsDClient.gauge("email.validation", 0);
    	statsDClient.gauge("linked.accounts", 0);
    }
    
    @Bean
    QueueClient queueClient(){
    	return new QueueClient();
    }
    
}