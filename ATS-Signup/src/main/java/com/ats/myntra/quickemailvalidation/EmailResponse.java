package com.ats.myntra.quickemailvalidation;

public class EmailResponse {
	
	private String result;
	
	private String reason;
	
	private Boolean disposable;
	
	private Boolean accept_all;
	
	private Boolean role;
	
	private String email;
	
	private String user;
	
	private String domain;
	
	private Boolean safe_to_send;
	
	private Boolean success;
	
	private String message;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public Boolean getDisposable() {
		return disposable;
	}

	public void setDisposable(Boolean disposable) {
		this.disposable = disposable;
	}

	public Boolean getAccept_all() {
		return accept_all;
	}

	public void setAccept_all(Boolean accept_all) {
		this.accept_all = accept_all;
	}

	public Boolean getRole() {
		return role;
	}

	public void setRole(Boolean role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public Boolean getSafe_to_send() {
		return safe_to_send;
	}

	public void setSafe_to_send(Boolean safe_to_send) {
		this.safe_to_send = safe_to_send;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	

}
