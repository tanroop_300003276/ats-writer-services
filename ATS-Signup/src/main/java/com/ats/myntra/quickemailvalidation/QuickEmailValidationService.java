package com.ats.myntra.quickemailvalidation;

import java.io.IOException;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ats.myntra.requests.Request;
import com.ats.myntra.signup.utils.Utils;
import com.ats.myntra.statsd.StatsDClient;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component
public class QuickEmailValidationService {
	
	@Value("${quickemail.endpoint}")
	String quickEmailEndPoint;
	
	@Value("${quickemail.apikey}")
	String quickEmailApiKey;
	
	@Autowired
	StatsDClient statsDClient;
	
	public static final Logger	LOGGER = Logger.getLogger(QuickEmailValidationService.class);
	
	public EmailValidationResponse validateEmail(String email) throws IOException {
		statsDClient.increment("signup.email.validate");
		StringBuilder url = new StringBuilder(quickEmailEndPoint);
        url.append("email=");
        url.append(email);
        url.append("&apikey=");
        url.append(quickEmailApiKey);
        
        long startTime = System.currentTimeMillis();
        String response = Request.getRequest(url.toString());
        long endTime = System.currentTimeMillis();
        long diff = endTime - startTime;
        Utils.logMessage("Total Response time from quickemail api: " + diff);
        
        
        GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		
		EmailResponse emailResponse = gson.fromJson(response.toString(), EmailResponse.class);
		
		EmailValidationResponse emailValidationResponse = emailResponse(emailResponse);
		
		return emailValidationResponse;
		
	}

	private EmailValidationResponse emailResponse(EmailResponse emailResponse) {
		EmailValidationResponse emailValidationResponse = new EmailValidationResponse();
		emailValidationResponse.setEmail(emailResponse.getEmail());
		
		if(emailResponse.getSafe_to_send()){
			emailValidationResponse.setValid(true);
		}
		
		else if(StringUtils.equals(emailResponse.getResult(), "invalid")){
			emailValidationResponse.setValid(false);
			emailValidationResponse.setReason(emailResponse.getReason());
		}
		else if(StringUtils.equals(emailResponse.getResult(), "valid")){
			if(emailResponse.getDisposable())
			{
				emailValidationResponse.setValid(false);
				emailValidationResponse.setReason("disposable_email");
			}
			else{
				emailValidationResponse.setValid(true);
			}
		}
		
		else if(StringUtils.equals(emailResponse.getResult(), "unknown")){
			if(emailResponse.getDisposable()){
				emailValidationResponse.setValid(false);
				emailValidationResponse.setReason("disposable_email");
			}
			else{
				emailValidationResponse.setValid(true);
			}
		}
		
		return emailValidationResponse;
	}


}
