package com.ats.myntra.helper;

import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ats.myntra.atsservice.enums.CODReason;
import com.ats.myntra.atsservice.enums.LinkedBy;
import com.ats.myntra.atsservice.enums.UserType;
import com.ats.myntra.quickemailvalidation.EmailValidationResponse;
import com.myntra.shield.enums.AbuserType;
import com.myntra.shield.enums.MessageType;
import com.myntra.shield.exception.ShieldException;
import com.myntra.shield.messages.Message;
import com.myntra.shield.messages.MessageDetails;
import com.myntra.shield.queue.QueueClient;

@Component
public class PublishBI {
	
	 private static final Logger LOGGER = LoggerFactory.getLogger(PublishBI.class);
	
	@Autowired
	QueueClient queueClient;
	
	public void sendMessageEmailValidation(EmailValidationResponse emailValidationResponse) throws ShieldException{
		
		LOGGER.debug("Sending message: " +  MessageType.EMAIL_VALIDATION.toString() + " uidx: " + emailValidationResponse.getUidx());
		
		Message message = new Message();
		message.setMessageType(MessageType.EMAIL_VALIDATION);
		
		MessageDetails messageDetails = new MessageDetails();
		messageDetails.setUidx(emailValidationResponse.getUidx());
		messageDetails.setIsFakeEmail(!emailValidationResponse.getValid());
		
		message.setMessageDetails(messageDetails);
		
		queueClient.sendFakeEmailMessage(message);
		
	}
	
	public void sendMessageLinkedAccounts(String uidx, String linkedTo, String linkedBy, String userType) throws ShieldException {
		
		LOGGER.debug("Sending message: " +  MessageType.LINKED_ACCOUNTS.toString() + " uidx: " + uidx);
		Message message = new Message();
		message.setMessageType(MessageType.LINKED_ACCOUNTS);
		
		MessageDetails messageDetails = new MessageDetails();
		messageDetails.setUidx(uidx);
		messageDetails.setLinkedTo(linkedTo);
		messageDetails.setLinkedBy(linkedBy);
		
		if(StringUtils.equals(userType, UserType.BLACKLISTED.toString()))
			messageDetails.setAbuserType(AbuserType.BLACKLISTED);
		
		else if(StringUtils.equals(userType, UserType.RETURN_ABUSER.toString()))
			messageDetails.setAbuserType(AbuserType.RETURN_ABUSER);
		
		else
			messageDetails.setAbuserType(AbuserType.RTO_ABUSER);
		message.setMessageDetails(messageDetails);
		
		queueClient.sendLinkedAccountsMessage(message);
		
	}

	public void sendMessageFreeAccount(String uidx) throws ShieldException {
		LOGGER.debug("Sending message: " +  MessageType.FREE_USER.toString() + " uidx: " + uidx);
		Message message = new Message();
		message.setMessageType(MessageType.FREE_USER);
		MessageDetails messageDetails = new MessageDetails();
		messageDetails.setUidx(uidx);
		message.setMessageDetails(messageDetails);
		
		//Insert this both in linked accounts as free user and cod table with default value 20k
		queueClient.sendFreeAccountMessage(message);
		
		message = new Message();
		message.setMessageType(MessageType.COD_THROTTLING_INCREMENTAL);
		
		messageDetails = new MessageDetails();
		messageDetails.setUidx(uidx);
		messageDetails.setIsRTOAbuser(false);
		messageDetails.setMinCod(Double.valueOf(299));
		messageDetails.setMaxCod(Double.valueOf(20000));
		messageDetails.setCodReason(CODReason.COD107.toString());
		messageDetails.setRtoPercentageByRevenue(Double.valueOf(0));
		messageDetails.setRtoPercentageByVolume(Double.valueOf(0));
		
		message.setMessageDetails(messageDetails);
		
		queueClient.sendCODThrottlingMessage(message);
		
		
	}

	public void sendMessageRTODetails(String uidx, String linkedTo, String linkedBy, String userType,
			String maxCod) throws ShieldException {
		Message message = new Message();
		message.setMessageType(MessageType.COD_THROTTLING_INCREMENTAL);
		MessageDetails msgDetails = new MessageDetails();
		msgDetails.setUidx(uidx);
		msgDetails.setRtoPercentageByRevenue(Double.valueOf(0.0));
		msgDetails.setRtoPercentageByVolume(Double.valueOf(0.0));
		
		if (StringUtils.equals(userType, UserType.RTO_ABUSER.toString())){
			msgDetails.setIsRTOAbuser(true);
			if(StringUtils.equals(linkedBy, LinkedBy.PHONE.toString())){
				msgDetails.setCodReason(CODReason.COD108.toString());
				
			}
			else if(StringUtils.equals(linkedBy, LinkedBy.DEVICE.toString())){
				msgDetails.setCodReason(CODReason.COD109.toString());
			}
			
			if(Double.valueOf(maxCod).equals(Double.valueOf(0.0)))
				msgDetails.setMinCod(Double.valueOf(0.0));
			else
				msgDetails.setMinCod(Double.valueOf(299.0));
			
			
			msgDetails.setMaxCod(Double.valueOf(maxCod));
			
		}
		else{
			msgDetails.setIsRTOAbuser(false);
			msgDetails.setMinCod(Double.valueOf(299.0));
			msgDetails.setMaxCod(Double.valueOf(20000.0));
			msgDetails.setCodReason(CODReason.COD107.toString());
		}
		
		
		message.setMessageDetails(msgDetails);
		
		queueClient.sendCODThrottlingMessage(message);
		
	}

}
