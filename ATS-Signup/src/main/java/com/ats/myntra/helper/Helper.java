package com.ats.myntra.helper;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.codec.binary.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.ats.myntra.atsservice.enums.CODLimit;
import com.ats.myntra.atsservice.enums.CODReason;
import com.ats.myntra.atsservice.enums.Exceptions;
import com.ats.myntra.atsservice.enums.LinkedBy;
import com.ats.myntra.atsservice.enums.UserType;
import com.ats.myntra.exceptions.SignupOperationsException;
import com.ats.myntra.model.LinkedAccountDetails;
import com.ats.myntra.model.LinkedAccountFlags;
import com.ats.myntra.model.UserProfile;
import com.ats.myntra.quickemailvalidation.EmailValidationResponse;
import com.ats.myntra.requests.Request;
import com.ats.myntra.signup.utils.FeatureGate;
import com.ats.myntra.signup.utils.Utils;
import com.ats.myntra.statsd.StatsDClient;
import com.myntra.ats.blocked.client.ATSClient;
import com.myntra.ats.blocked.client.response.LinkAccountsResponse;
import com.myntra.ats.blocked.client.response.LinkedCustIdea;
import com.myntra.ats.blocked.client.response.UserDetailsResponse;
import com.myntra.ats.blocked.client.response.UserPhoneDetailsResponse;
import com.myntra.ats.user.client.request.LinkedAccountsRequest;
import com.myntra.ats.user.client.request.UserDetailsRequest;
import com.myntra.ats.user.client.request.UserPhoneDetailsRequest;
import com.myntra.idea.client.IdeaClient;
import com.myntra.idea.entry.UserEntry;
import com.myntra.idea.response.ProfileResponse;
import com.myntra.shield.exception.ShieldException;

@SuppressWarnings({ "unchecked", "static-access", "rawtypes" })
@Component
public class Helper {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(Helper.class);
	
	@Autowired
	ATSClient atsClient;
	
	@Autowired
	PublishBI publishBI;
	
	IdeaClient ideaClient;
	
	@Autowired
	StatsDClient statsDClient;
	
	@Autowired
    FeatureGate featureGate;
	 
 	Map<String, String> labelsTOValues;
	Map<String, String> valuesTOLabels;
	@Value("${labels.label1}")
	String label1;
	@Value("${labels.label2}")
	String label2;
	@Value("${labels.label3}")
	String label3;
	@Value("${labels.label4}")
	String label4;
	@Value("${labels.label5}")
	String label5;
	@Value("${labels.label6}")
	String label6;
	@Value("${labels.label8}")
	String label8;
	@Value("${labels.label9}")
	String label9;
	@Value("${labels.value1}")
	String value1;
	@Value("${labels.value2}")
	String value2;
	@Value("${labels.value3}")
	String value3;
	@Value("${labels.value4}")
	String value4;
	@Value("${labels.value5}")
	String value5;
	@Value("${labels.value6}")
	String value6;
	@Value("${labels.value8}")
	String value8;
	@Value("${labels.value9}")
	String value9;
	
	
	@Value("${idea_service.url}")
	String idea_service_url;
	
	@Value("${link_abuser_enabled}")
	Boolean link_abuser_enabled;
	
	@Value("${retryCount}")
	int retryCount;

	@PostConstruct
	public void init() {
		labelsTOValues = new HashMap();
		labelsTOValues.put(label1, value1);
		labelsTOValues.put(label2, value2);
		labelsTOValues.put(label3, value3);
		labelsTOValues.put(label4, value4);
		labelsTOValues.put(label5, value5);
		labelsTOValues.put(label6, value6);
		labelsTOValues.put(label8, value8);
		labelsTOValues.put(label9, value9);

		valuesTOLabels = new HashMap();
		valuesTOLabels.put(value1, label1);
		valuesTOLabels.put(value2, label2);
		valuesTOLabels.put(value3, label3);
		valuesTOLabels.put(value4, label4);
		valuesTOLabels.put(value5, label5);
		valuesTOLabels.put(value6, label6);
		valuesTOLabels.put(value8, label8);
		valuesTOLabels.put(value9, label9);
		
		ideaClient = new IdeaClient(idea_service_url);
	}

		

	public void markEmailValidationResponse(EmailValidationResponse emailValidationResponse) throws UnsupportedEncodingException, SignupOperationsException, ShieldException {
		if(!emailValidationResponse.getValid()){
			Utils.logMessage("Marking invalid email: " + emailValidationResponse.getEmail());
			
			UserDetailsRequest userDetailsRequest = new UserDetailsRequest();
			userDetailsRequest.setLogin(emailValidationResponse.getUidx());
			userDetailsRequest.setIsFakeEmail(Boolean.valueOf(!emailValidationResponse.getValid()));
			userDetailsRequest.setReasonFakeEmail(emailValidationResponse.getReason());
			
			UserDetailsResponse  response = atsClient.setFakeEmail(userDetailsRequest);
			
			statsDClient.increment("signup.email.invalid");
			
			if (!StringUtils.equals(response.getLogin(), emailValidationResponse.getUidx())) {
				throw new SignupOperationsException(Exceptions.SET_FAKE_EMAIL_EXCEPTION.toString() + ". Login: " + emailValidationResponse.getUidx());
			}
		}
		
		if ((featureGate.fetchFeatureGate("publish.bi.email.validation", false, Boolean.class)))
			publishBI.sendMessageEmailValidation(emailValidationResponse);
		else
			LOGGER.debug("Feature for pushing email validation data to BI is currently switched off.");
		
	}

	
	public void markAsAbuser(LinkedAccountDetails linkedAccountDetails) throws Exception{
		
		String userType = linkedAccountDetails.getUserType();
		if(StringUtils.equals(userType, UserType.BLACKLISTED.toString())){
			blacklistUser(linkedAccountDetails.getUidx());
		}
		else if(StringUtils.equals(userType, UserType.RETURN_ABUSER.toString())){
			markReturnAbuser(linkedAccountDetails.getUidx(), linkedAccountDetails.getLinkedBy());
		}
		else
			setCOD(linkedAccountDetails.getUidx(), linkedAccountDetails.getMaxCod(), linkedAccountDetails.getLinkedBy());
		
		if ((featureGate.fetchFeatureGate("publish.bi.linked.accounts", false, Boolean.class))){
			publishBI.sendMessageLinkedAccounts(linkedAccountDetails.getUidx(), linkedAccountDetails.getLinkedTo(), linkedAccountDetails.getLinkedBy(), linkedAccountDetails.getUserType());
			publishBI.sendMessageRTODetails(linkedAccountDetails.getUidx(), linkedAccountDetails.getLinkedTo(), linkedAccountDetails.getLinkedBy(), linkedAccountDetails.getUserType(), linkedAccountDetails.getMaxCod());
		}
		else
			LOGGER.debug("Feature for pushing linked accounts data to BI is currently switched off.");
			
		statsDClient.increment(linkedAccountDetails.getLinkedBy().toLowerCase() + ".linked.account." + linkedAccountDetails.getUserType().toLowerCase());
		
	}
	
	public void linkUser(LinkedAccountDetails linkedAccountDetails) throws UnsupportedEncodingException, SignupOperationsException {
		
		LinkedAccountsRequest linkedAccountsRequest = new LinkedAccountsRequest();
		linkedAccountsRequest.setUidx1(linkedAccountDetails.getUidx());
		linkedAccountsRequest.setUidx2(linkedAccountDetails.getLinkedTo());
		linkedAccountsRequest.setLinkedBy(linkedAccountDetails.getLinkedBy());
		linkedAccountsRequest.setAbuserType(linkedAccountDetails.getLinkedBy());
		
		LinkAccountsResponse response = atsClient.linkAccounts(linkedAccountsRequest);
		
		if(!StringUtils.equals(response.getFrom(), linkedAccountDetails.getUidx()))
			throw new SignupOperationsException(Exceptions.LINK_ACCOUNTS_EXCEPTION.toString() + ". Login: " + linkedAccountDetails.getUidx());
		
	}

	private void setCOD(String uidx, String maxCod, String linkedBy) throws Exception {
		//JSONObject jsonParam = new JSONObject();

		String maxC = (String) valuesTOLabels.get(maxCod);

		if (maxC == null) {
			throw new Exception("COD label values not present for maxCod: " + maxCod);
		}
		
		UserDetailsRequest userDetailsRequest = new UserDetailsRequest();
		userDetailsRequest.setLogin(uidx);
		userDetailsRequest.setMaxCOD(maxC);
		
//		jsonParam.put("login", uidx);
//		jsonParam.put("maxCOD", maxC);
		if (StringUtils.equals(linkedBy, LinkedBy.DEVICE.toString())) {
			//jsonParam.put("reasonRTOAbuser", CODReason.COD109.toString());
			userDetailsRequest.setReasonRTOAbuser(CODReason.COD109.toString());
		} else
			userDetailsRequest.setReasonRTOAbuser(CODReason.COD108.toString());
			//jsonParam.put("reasonRTOAbuser", CODReason.COD108.toString());
		
		userDetailsRequest.setIsCODThrottled(Boolean.valueOf(false));
		//jsonParam.put("isCODThrottled", Boolean.valueOf(false));
		
		UserDetailsResponse response = atsClient.setCODLimit(userDetailsRequest);
		if (!StringUtils.equals(response.getLogin(), uidx)) {
			throw new SignupOperationsException(Exceptions.SET_COD_LIMIT_EXCEPTION.toString() + ". Login: " + uidx);
		}
		
	}

	private void markReturnAbuser(String uidx, String linkedBy) throws UnsupportedEncodingException, SignupOperationsException {
		UserDetailsRequest userDetailsRequest = new UserDetailsRequest();
		userDetailsRequest.setLogin(uidx);
		userDetailsRequest.setIsReturnAbuser(Boolean.valueOf(true));
		
		if (StringUtils.equals(linkedBy, LinkedBy.DEVICE.toString())) {
			userDetailsRequest.setReasonReturnAbuser(CODReason.COD109.getReason());
		} else
			userDetailsRequest.setReasonReturnAbuser(CODReason.COD108.getReason());
		
		UserDetailsResponse response = atsClient.setReturnAbuser(userDetailsRequest);
		if (!StringUtils.equals(response.getLogin(), uidx)) {
			throw new SignupOperationsException(Exceptions.SET_RETURN_ABUSER_EXCEPTION.toString() + ". Login: " + uidx);
		}
		
	}

	public void setPhone(String uidx, String phone) throws UnsupportedEncodingException, SignupOperationsException {
		
		if(phone == null)
			return;
		
		UserPhoneDetailsRequest userPhoneDetailsRequest = new UserPhoneDetailsRequest();
		userPhoneDetailsRequest.setUidx(uidx);
		userPhoneDetailsRequest.setPhone(phone);
		
		int hitCount = retryCount;
		while(hitCount > 0){
			UserPhoneDetailsResponse response = atsClient.setPhone(userPhoneDetailsRequest);
			if(StringUtils.equals(response.getUidx(), uidx))
				break;
			else 
				hitCount--;
//			if (!StringUtils.equals(response.getUidx(), uidx)) {
//				throw new SignupOperationsException(Exceptions.SET_PHONE_EXCEPTION.toString() + ". Uidx: " + uidx);
//			}
		}
		if(hitCount <=0)
			LOGGER.error("Error while setting phone of user. Uidx: {}, phone: {}, ", uidx, phone);

	}


	public List<String> getLinkedAccountsByDeviceId(String uidx) {
		LinkedCustIdea response = atsClient.getLinkedAccountsByDevice(uidx);
		return response.getUidx();
	}

	public List<String> getLinkedAccountsByPhone(String phone) {
		LinkedCustIdea response = atsClient.getLinkedAccountsByPhone(phone);
		return response.getUidx();
	}
	
	public UserDetailsResponse getUserDetails(String login){
//		UserDetailsResponse response = atsClient.getUserDetails(login);
		UserDetailsResponse response = atsClient.getAbuserDetails(login);
		return response;
	}
	
	
	public ProfileResponse getUserProfile(String uidx) throws IOException {
		
		return ideaClient.getByUidx("myntra", uidx);
		
//		String url = idea_service_url + "uidx/myntra/" + uidx;
//		String response = Request.getRequest(url);
//
//		GsonBuilder gsonBuilder = new GsonBuilder();
//		Gson gson = gsonBuilder.create();
//
//		ProfileResponse profileResponse = (ProfileResponse) gson.fromJson(response.toString(), ProfileResponse.class);
//		return profileResponse;
	}
	
	public void blacklistUser(String uidx) throws Exception {
		
		UserEntry userEntry = new UserEntry();
		userEntry.setStatus("BLACKLISTED");
		userEntry.setAppName("myntra");
		userEntry.setUidx(uidx);
		
		changeUserStatus(userEntry);
		//profileResponse =  ideaClient.updateUser(userEntry);
		
		ProfileResponse profileResponse = ideaClient.getByUidx("myntra", uidx);
		
		
		if ((profileResponse == null) || (profileResponse.getStatus() == null))
			throw new Exception("Error while blacklisting user in IDEA Service. Uidx: " + uidx);
		String statusType = profileResponse.getStatus().getStatusType().toString();
		if (!"SUCCESS".equals(statusType)) {
			throw new Exception("Error while blacklisting user in IDEA Service. Uidx: " + uidx
					+ " Response returned is: " + profileResponse.getStatus().getStatusMessage());
		}
	}
	
	private void changeUserStatus(UserEntry userEntry) throws IOException {
		// TODO Auto-generated method stub
		StringBuilder url = new StringBuilder();
		url.append(idea_service_url);
		url.append("/idea/opt/profile/changeUserStatus");
		JSONObject jsonParam = new JSONObject();
		
		jsonParam.put("appName", "myntra");
		jsonParam.put("status", "BLACKLISTED");
		jsonParam.put("uidx", userEntry.getUidx());
		
		String response = Request.putRequest(url.toString(), jsonParam.toString());
		
	}



	public LinkedAccountFlags getLinkedAccountUser(String uid, List<String> linkedAccounts) throws IOException
	{
	    List<UserProfile> userProfileList = new ArrayList();
	    if(linkedAccounts ==  null){
	    	
	    	LinkedAccountFlags accountFlags = new LinkedAccountFlags();
	    	accountFlags.setGenuineAccount(true);
	    	return accountFlags;
	    	
	    }
	    
	    
	    for (String uidx: linkedAccounts) {
	    	
	      if (!StringUtils.equals(uid, uidx))
	      {
	
	        ProfileResponse profileResponse = getUserProfile(uidx);
	        UserProfile uP = new UserProfile();
	        uP.setUidx(uidx);
	        if (profileResponse.getEntry() != null) {
	          uP.setRegisteredOn(Utils.getTimestamp(profileResponse.getEntry().getRegistrationOn()));
	          uP.setStatus(profileResponse.getEntry().getStatus());
	          userProfileList.add(uP);
	        }
	        
	        
	      }
	    }
	    
	    
	    
	    Collections.sort(userProfileList, new Comparator<UserProfile>() {
	        public int compare(UserProfile o1, UserProfile o2) {
	            return o1.getRegisteredOn().compareTo(o2.getRegisteredOn());
	        }
	    });
	    
	
	    LinkedAccountFlags accountFlags = new LinkedAccountFlags();
	    
	    if (userProfileList.size() == 0) {
	      accountFlags.setLinked(Boolean.valueOf(false));
	      return accountFlags;
	    }
	    
	    accountFlags.setLinked(true);
	    for (UserProfile profile : userProfileList) {
	      if (StringUtils.equals(profile.getStatus(), UserType.BLACKLISTED.toString())) {
	        if (!accountFlags.getBlacklistedAccount().booleanValue())
	        {
	          accountFlags.setBlacklistedAccount(Boolean.valueOf(true));
	          accountFlags.setLinkedBlacklistedUser(profile.getUidx());
	        }
	      } else {
	        UserDetailsResponse userDetailsResponse = getUserDetails(profile.getUidx());
	        if (userDetailsResponse.getIsReturnAbuser().booleanValue()) {
	          if (!accountFlags.getReturnAbuser().booleanValue())
	          {
	            accountFlags.setReturnAbuser(Boolean.valueOf(true));
	            accountFlags.setLinkedReturnAbuser(profile.getUidx());
	          }
	        } 
//	        else if( userDetailsResponse.getMaxCOD().equals(Double.valueOf(CODLimit.LABEL_ZERO.getValue())) || userDetailsResponse.getMaxCOD().equals(Double.valueOf(CODLimit.LABEL_1500.getValue()))){
	        else if(userDetailsResponse.getMaxCOD().equals(Double.valueOf(CODLimit.LABEL_ZERO.getLabel())) || userDetailsResponse.getMaxCOD().equals(Double.valueOf(CODLimit.LABEL_1500.getLabel())) || userDetailsResponse.getMaxCOD().equals(Double.valueOf(CODLimit.LABEL_1500_8.getLabel()))){
	        	accountFlags.setRtoAbuser(Boolean.valueOf(true));
	          String codlabel = String.valueOf(userDetailsResponse.getMaxCOD().intValue());
	          Integer cod = Integer.valueOf(labelsTOValues.get(codlabel));
//	          Double cod = Double.valueOf((String)labelsTOValues.get(codlabel)).intValue();
	          if (cod.intValue() > accountFlags.getMaxCODlimit())
	          {
	            accountFlags.setMaxCODlimit(cod.intValue());
	            accountFlags.setLinkedRTOAbuser(profile.getUidx());
	          }
	        }
	        else {
	          accountFlags.setGenuineAccount(Boolean.valueOf(true));
	          break;
	        }
	      }
	    }
	    
	    return accountFlags;
	}



	public void markFreeUser(String uidx) throws ShieldException {
		// TODO Auto-generated method stub
		if ((featureGate.fetchFeatureGate("publish.bi.linked.accounts", false, Boolean.class))){
			publishBI.sendMessageFreeAccount(uidx);
		}
		
		else
			LOGGER.debug("Feature for pushing linked accounts data to BI is currently switched off.");
		
	}
	

}
