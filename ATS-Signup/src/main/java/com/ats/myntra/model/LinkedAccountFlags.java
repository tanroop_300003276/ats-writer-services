package com.ats.myntra.model;

public class LinkedAccountFlags {
	
	Boolean blacklistedAccount = false;
	Boolean returnAbuser = false;
	Boolean rtoAbuser = false;
	Boolean genuineAccount = false;
	Boolean linked = false;
	int maxCODlimit = -1;
	String linkedReturnAbuser = null;
	String linkedBlacklistedUser = null;
	String linkedRTOAbuser = null;
	
	public Boolean getBlacklistedAccount() {
		return blacklistedAccount;
	}

	public void setBlacklistedAccount(Boolean blacklistedAccount) {
		this.blacklistedAccount = blacklistedAccount;
	}

	public Boolean getReturnAbuser() {
		return returnAbuser;
	}

	public void setReturnAbuser(Boolean returnAbuser) {
		this.returnAbuser = returnAbuser;
	}

	public Boolean getRtoAbuser() {
		return rtoAbuser;
	}

	public void setRtoAbuser(Boolean rtoAbuser) {
		this.rtoAbuser = rtoAbuser;
	}

	public Boolean getGenuineAccount() {
		return genuineAccount;
	}

	public void setGenuineAccount(Boolean genuineAccount) {
		this.genuineAccount = genuineAccount;
	}

	public int getMaxCODlimit() {
		return maxCODlimit;
	}

	public void setMaxCODlimit(int maxCODlimit) {
		this.maxCODlimit = maxCODlimit;
	}

	public String getLinkedReturnAbuser() {
		return linkedReturnAbuser;
	}

	public void setLinkedReturnAbuser(String linkedReturnAbuser) {
		this.linkedReturnAbuser = linkedReturnAbuser;
	}

	public String getLinkedBlacklistedUser() {
		return linkedBlacklistedUser;
	}

	public void setLinkedBlacklistedUser(String linkedBlacklistedUser) {
		this.linkedBlacklistedUser = linkedBlacklistedUser;
	}

	public String getLinkedRTOAbuser() {
		return linkedRTOAbuser;
	}

	public void setLinkedRTOAbuser(String linkedRTOAbuser) {
		this.linkedRTOAbuser = linkedRTOAbuser;
	}

	public Boolean getLinked() {
		return linked;
	}

	public void setLinked(Boolean linked) {
		this.linked = linked;
	}
	

}
