package com.ats.myntra.model;

import com.ats.myntra.atsservice.enums.UserType;

public class LinkedAccountDetails {
	
	String uidx;
	
	String linkedTo;
	
	String linkedBy;
	
	String userType;
	
	String maxCod;

	public String getUidx() {
		return uidx;
	}

	public void setUidx(String uidx) {
		this.uidx = uidx;
	}

	public String getLinkedTo() {
		return linkedTo;
	}

	public void setLinkedTo(String linkedTo) {
		this.linkedTo = linkedTo;
	}

	public String getLinkedBy() {
		return linkedBy;
	}

	public void setLinkedBy(String linkedBy) {
		this.linkedBy = linkedBy;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getMaxCod() {
		return maxCod;
	}

	public void setMaxCod(String maxCod) {
		this.maxCod = maxCod;
	}
	
}
