package com.ats.myntra.requests;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Request {
	
	public static final Logger LOGGER = LoggerFactory.getLogger(Request.class);

	public static String getRequest(String url) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();
		LOGGER.debug("Sending 'GET' request to URL : " + url);
		LOGGER.debug("Response Code : " + responseCode);
		
		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		LOGGER.debug(response.toString());
		return response.toString();
	}

	public static String putRequest(String url, String urlParam) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setDoOutput( true );
		con.setInstanceFollowRedirects( false );
		con.setRequestMethod("PUT");

		//add request header
		con.setRequestProperty("Content-Type", "application/json");
		
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParam);
		wr.flush();
		wr.close();
		
		int responseCode = con.getResponseCode();
		if(responseCode!= 200)
			return null;
		LOGGER.debug("Sending 'PUT' request to URL : " + url +" with params : " + urlParam);
		LOGGER.debug("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		LOGGER.debug(response.toString());
		return response.toString();

	}
}
