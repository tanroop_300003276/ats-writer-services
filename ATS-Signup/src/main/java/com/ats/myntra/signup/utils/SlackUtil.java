package com.ats.myntra.signup.utils;

import java.io.IOException;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Component;

@Component
public class SlackUtil {
	
	public static void sendMessage(String message, String channel) {
		sendMessage(message, channel, null, null, null);
	}

	public static void sendMessage(String message, String channel, String userName) {
		sendMessage(message, channel, userName, null, null);
	}

	public static void sendMessage(String message, String channel, String userName, String userIcon) {
		sendMessage(message, channel, userName, userIcon, null);
	}

	public static void sendMessage(String message, String channel, String userName, String userIcon, String webhookUrl) {
		if(StringUtils.equals(channel, null) || StringUtils.equals(message, null))
			return;
		try {
			postMessage(message, channel, userName, userIcon, webhookUrl);
		} catch (Exception e) {
			// Some exception occurred.
		}
	}

	private static void postMessage(String message, String channel, String userName, String userIcon, String webhookUrl)
			throws ClientProtocolException, IOException {
		HttpClient httpClient = HttpClients.createDefault();
		if (StringUtils.equals(webhookUrl, null)) {
			webhookUrl = SlackConstants.getWebHookForChannel(channel);
		}
		HttpPost httpPost = new HttpPost(webhookUrl);

		JSONObject requestObject = new JSONObject();
		requestObject.put("text", message);
		requestObject.put("channel", channel);
//		if(StringUtils.isNotEmpty(userName)){
//			requestObject.put("username", userName);
//		}
//		if(StringUtils.isNotEmpty(userIcon)){
//			requestObject.put("icon_emoji", userIcon);
//		}
		StringEntity requestEntity = new StringEntity(requestObject.toString(), ContentType.APPLICATION_JSON);
		httpPost.setEntity(requestEntity);
		httpPost.setHeader("Content-type", "application/json");

		httpClient.execute(httpPost);
	}
}