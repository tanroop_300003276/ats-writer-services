package com.ats.myntra.signup.utils;


import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.myntra.Option;
import com.myntra.Switch;


@Component
public class FeatureGate {

    private static final Logger LOGGER = Logger.getLogger(FeatureGate.class);

    @Value("${switch.env}")
    private String switchEnv;

    @Value("${switch.namespace}")
    private String switchNameSpace;

    @PostConstruct
    public void init() throws Exception {
        Map<String, Object> options = new HashMap<String, Object>();
        options.put(Option.ENV, switchEnv);
        Switch.init(options);
    }

    public <T> T fetchFeatureGate(String key, T defaultValue, Class<T> type) {
        T featureGate = null;
        try {
            JsonObject configObj = Switch.newBuilder(switchNameSpace)
                    .keys(ImmutableList.of(key))
                    .get();            
            Gson gson = new Gson();
            featureGate = gson.fromJson(configObj.get(key), type);
        } catch (Throwable e) {
            LOGGER.error(String.format("Error while fetching feature gate value from switch for %s", key), e);
        }
        if (featureGate == null) {
            featureGate = defaultValue;
        }
        return featureGate;
    }

}