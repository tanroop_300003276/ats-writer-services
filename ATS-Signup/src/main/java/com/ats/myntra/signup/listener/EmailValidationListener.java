package com.ats.myntra.signup.listener;
import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.ats.myntra.helper.Helper;
import com.ats.myntra.quickemailvalidation.EmailValidationResponse;
import com.ats.myntra.quickemailvalidation.QuickEmailValidationService;
import com.ats.myntra.signup.event.Signup;
import com.ats.myntra.signup.utils.FeatureGate;
import com.ats.myntra.signup.utils.SlackUtil;
import com.ats.myntra.signup.utils.Utils;
import com.ats.myntra.statsd.StatsDClient;
@Component
public class EmailValidationListener implements MessageListener{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailValidationListener.class);
    
    @Autowired
    QuickEmailValidationService QuickEmailValidationService;
    
    @Autowired
    FeatureGate featureGate;
    
    @Autowired
    StatsDClient statsDClient;
    
    @Autowired
    SlackUtil slackUtil;
    
    @Autowired
    Helper helper;
    
    Boolean enabled = true;
    
    int retryCount = 0;
    
    @Value("${retryCountAllowed}")
	int retryCountAllowed;
    
    String olderException;
    
    @SuppressWarnings("static-access")
	@Override
    public void onMessage(Message message){
    	
        LOGGER.debug("Signup event received...");
        try {
        	if(!enabled)
        		throw new Exception("Older exception not yet fixed: " + olderException);
        	statsDClient.increment("signup.email.message");
            Signup signupEvent = Utils.getSignUpEvent(message,"EmailValidationListener");
            
            if(signupEvent.getUidx() == null){
            	LOGGER.error("Uidx Null Error: "+ signupEvent.getUidx() + ", " + signupEvent.getEmail() + ", " + signupEvent.getDeviceId() + ", " + signupEvent.getPhone()+", " + signupEvent.getChannel() );
            	return;
            }
            
            if(!emailToBeVerified(signupEvent)){
                LOGGER.debug("email not to be validated: " + signupEvent.getEmail());
                return;
            }
            LOGGER.debug("Validating email: " + signupEvent.getEmail());
            String email = signupEvent.getEmail();
            
            EmailValidationResponse emailValidationResponse = QuickEmailValidationService.validateEmail(email);
            
            String uidx = signupEvent.getUidx();
            emailValidationResponse.setUidx(uidx);
            
            helper.markEmailValidationResponse(emailValidationResponse);
            
            
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            statsDClient.gauge("email.validation", 1);
            retryCount++;
            if(retryCount > retryCountAllowed)
            {
            	olderException = e.getMessage();
            	enabled = false;
            }
            if(featureGate.fetchFeatureGate("ats.signup.message.slack", false, Boolean.class))
            	slackUtil.sendMessage("@channel Exception in email validation listener. " + e, "shield_notifs");
            throw new RuntimeException(e);
        }
        
    }
    private boolean emailToBeVerified(Signup signupEvent) {
        if (!(featureGate.fetchFeatureGate("ats.validate.signup.email", false, Boolean.class)))
            return false;
        
        if(signupEvent.getEmail() == null){
        	Utils.logMessage("Email is missing");
            statsDClient.increment("signup.email.missing");
            return false;
        }
        
        String channel = signupEvent.getChannel();
        if(StringUtils.equals(channel, "FACEBOOK") || StringUtils.equals(channel, "GPLUS")){
        	Utils.logMessage("Signup from channel: " + channel);
            statsDClient.increment("signup.email.fbgplus");
            return false;
        }
        
        return true;
    }
    
}