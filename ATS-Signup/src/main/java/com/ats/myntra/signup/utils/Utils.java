package com.ats.myntra.signup.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;

import com.ats.myntra.signup.event.Signup;
import com.ats.myntra.signup.event.SignupEvent;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Utils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Utils.class);

	public static Signup getSignUpEvent(Message message, String listener) throws IOException, ClassNotFoundException {
		
		String body = new String(message.getBody());
		LOGGER.debug(listener + " Signup event: " + body);
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		SignupEvent signupEvent = gson.fromJson(body, SignupEvent.class);
		
		
		String uidx = null;
		String email = null;
		String phone = null;
		String channel = null; 
		
		try{
			uidx = signupEvent.getUserEntry().getUidx();
		}catch(Exception e){
			LOGGER.debug("Fetching uidx: " +e.getMessage());
		}
		
		try{
			channel = signupEvent.getUserEntry().getChannel();
		}catch(Exception e){
			LOGGER.debug("Fetching channel: " +e.getMessage());
		}
		
		try{
			email = signupEvent.getUserEntry().getEmailDetails().get(0).getEmail();
		}catch(Exception e){
			LOGGER.debug("Fetching email: " + e.getMessage());
		}
		
		try{
			phone = signupEvent.getUserEntry().getPhoneDetails().get(0).getPhone();
		}catch(Exception e){
			LOGGER.debug("Fetching phone: " + e.getMessage());
		}
		LOGGER.debug(listener + " Relevant values: uidx: " + uidx + ", channel: " + channel + ", email: " + email+ ", phone: " + phone);
		
		Signup signup = new Signup();
		signup.setUidx(uidx);
		signup.setEmail(email);
		signup.setPhone(phone);
		signup.setChannel(channel);
		
		return signup;
	}

	public static void logMessage(String string) {
		LOGGER.debug(string);
		
	}

	public static String getTimestamp(Date registrationOn) {
		// TODO Auto-generated method stub
		String ts = new SimpleDateFormat("YYYY-MM-dd-HH:mm:ss").format(registrationOn);
		return ts;
	}

}
