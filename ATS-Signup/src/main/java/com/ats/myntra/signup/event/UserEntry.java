package com.ats.myntra.signup.event;

import java.util.List;

public class UserEntry {
	
	List<EmailDetails> emailDetails;
	
	List<PhoneDetails> phoneDetails;
	
	String uidx;
	
	String registrationOn;
	
	String status;
	
	String channel;

	public List<EmailDetails> getEmailDetails() {
		return emailDetails;
	}

	public void setEmailDetails(List<EmailDetails> emailDetails) {
		this.emailDetails = emailDetails;
	}

	public List<PhoneDetails> getPhoneDetails() {
		return phoneDetails;
	}

	public void setPhoneDetails(List<PhoneDetails> phoneDetails) {
		this.phoneDetails = phoneDetails;
	}

	public String getUidx() {
		return uidx;
	}

	public void setUidx(String uidx) {
		this.uidx = uidx;
	}

	public String getRegistrationOn() {
		return registrationOn;
	}

	public void setRegistrationOn(String registrationOn) {
		this.registrationOn = registrationOn;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	

}
