package com.ats.myntra.signup.event;

public class DeviceInfo {
	
	String deviceUUID;

	public String getDeviceUUID() {
		return deviceUUID;
	}

	public void setDeviceUUID(String deviceUUID) {
		this.deviceUUID = deviceUUID;
	}

}
