package com.ats.myntra.signup.listener;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ats.myntra.atsservice.enums.LinkedBy;
import com.ats.myntra.atsservice.enums.UserType;
import com.ats.myntra.helper.Helper;
import com.ats.myntra.model.LinkedAccountDetails;
import com.ats.myntra.model.LinkedAccountFlags;
import com.ats.myntra.signup.event.Signup;
import com.ats.myntra.signup.utils.FeatureGate;
import com.ats.myntra.signup.utils.SlackUtil;
import com.ats.myntra.signup.utils.Utils;
import com.ats.myntra.statsd.StatsDClient;
@Component
public class LinkedAccountsListener implements MessageListener{
    
    private static final Logger LOGGER = LoggerFactory.getLogger(LinkedAccountsListener.class);
    
    @Autowired
    Helper helper;
    
    @Autowired
    FeatureGate featureGate;
    
    @Autowired
    StatsDClient statsDClient;
    
    @Autowired
    SlackUtil slackUtil;
    
    @SuppressWarnings("static-access")
	@Override
    public void onMessage(Message message) {
        statsDClient.increment("linked.account.message");
        LOGGER.debug("Signup event received...");
        try {
            Signup signupEvent = Utils.getSignUpEvent(message, "LinkedAccountsListener");
            
            if(signupEvent.getUidx() == null){
            	LOGGER.error("Uidx Null Error: "+ signupEvent.getUidx() + ", " + signupEvent.getEmail() + ", " + signupEvent.getDeviceId() + ", " + signupEvent.getPhone()+", " + signupEvent.getChannel() );
            	return;
            }
            
            String uidx = signupEvent.getUidx();
            String phone = signupEvent.getPhone();
            
            Boolean abuserLinkedByDevice = false;
            Boolean abuserLinkedByPhone = false;
            
            List<String> linkedAccountsByDeviceId = helper.getLinkedAccountsByDeviceId(uidx);
            //List<String> linkedAccountsByDeviceId = atsService.getLinkedAccountsByDeviceId(uidx);
            LinkedAccountFlags linkedAccountUserByDeviceId = helper.getLinkedAccountUser(uidx, linkedAccountsByDeviceId);
            
            if(shouldApplyPenaltyByLinkedAccountDeviceId(linkedAccountUserByDeviceId)){
                LOGGER.debug("Applying penalties as per device id linked accounts");
                statsDClient.increment("linked.account.deviceId");
                abuserLinkedByDevice = true;
                applyPenaltiesAsPerLinkedAccount(uidx, linkedAccountUserByDeviceId, LinkedBy.DEVICE.toString());
                helper.setPhone(uidx, phone);
                return;
            }
            else if(linkedAccountUserByDeviceId.getLinked()){
            	helper.markFreeUser(uidx);
            	helper.setPhone(uidx, phone);
                return;
            }
            
            if(phone == null){
            	helper.markFreeUser(uidx);
                return;
            }
            
            List<String> linkedAccountsByPhone = helper.getLinkedAccountsByPhone(phone);
            //List<String> linkedAccountsByPhone = atsService.getLinkedAccountsByPhone(phone);
            LinkedAccountFlags linkedAccountUserByPhone = helper.getLinkedAccountUser(uidx, linkedAccountsByPhone);
            
            if(shouldApplyPenaltyByLinkedAccountPhone(linkedAccountUserByPhone)){
                LOGGER.debug("Applying penalties as per phone linked accounts");
                statsDClient.increment("linked.account.phone");
                abuserLinkedByPhone = true;
                applyPenaltiesAsPerLinkedAccount(uidx, linkedAccountUserByPhone, LinkedBy.PHONE.toString());
                
            }
            if(!abuserLinkedByDevice && !abuserLinkedByPhone)
            	helper.markFreeUser(uidx);
            
            helper.setPhone(uidx, phone);
            
            //atsService.setPhone(phone, uidx);
            
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error(e.getMessage());
            statsDClient.gauge("linked.accounts", 1);
            if(featureGate.fetchFeatureGate("ats.signup.message.slack", false, Boolean.class))
            	slackUtil.sendMessage("@channel Exception in linked accounts listener. " + e, "shield_notifs");
            throw new RuntimeException(e);
        }
        
    }
    
    
    private boolean shouldApplyPenaltyByLinkedAccountDeviceId(LinkedAccountFlags linkedAccountUserByDeviceId) {
        if (!(featureGate.fetchFeatureGate("ats.linked.deviceid", true, Boolean.class)))
            return false;
        
        return !linkedAccountUserByDeviceId.getGenuineAccount() && linkedAccountUserByDeviceId.getLinked();
        
    }
    
    
    private boolean shouldApplyPenaltyByLinkedAccountPhone(LinkedAccountFlags linkedAccountUserByPhone) {
        if (!(featureGate.fetchFeatureGate("ats.linked.phone", true, Boolean.class)))
            return false;
        
        return !linkedAccountUserByPhone.getGenuineAccount() && linkedAccountUserByPhone.getLinked();
    }
    
    private void applyPenaltiesAsPerLinkedAccount(String uidx, LinkedAccountFlags linkedAccountUser, String linkedBy) throws Exception {
    	
    	LinkedAccountDetails linkedAccountDetails = new LinkedAccountDetails();
    	linkedAccountDetails.setUidx(uidx);
    	linkedAccountDetails.setLinkedBy(linkedBy);
    	
    	if(linkedAccountUser.getBlacklistedAccount()){
    		linkedAccountDetails.setLinkedTo(linkedAccountUser.getLinkedBlacklistedUser());
    		linkedAccountDetails.setUserType(UserType.BLACKLISTED.toString());
    	}
    	else if(linkedAccountUser.getReturnAbuser()){
    		linkedAccountDetails.setLinkedTo(linkedAccountUser.getLinkedReturnAbuser());
    		linkedAccountDetails.setUserType(UserType.RETURN_ABUSER.toString());
    	}
    	else{
    		linkedAccountDetails.setLinkedTo(linkedAccountUser.getLinkedRTOAbuser());
    		linkedAccountDetails.setUserType(UserType.RTO_ABUSER.toString());
    		linkedAccountDetails.setMaxCod(String.valueOf(linkedAccountUser.getMaxCODlimit()));
    	}
    	
    	helper.markAsAbuser(linkedAccountDetails);
    	
//        if(linkedAccountUserByDeviceId.getBlacklistedAccount()){
//            LOGGER.debug("Blacklisting uidx: "+ uidx + ". Reason: Linked by: "+ linkedBy);
//            //atsService.blacklistUser(uidx);
////            atsService.linkUser(uidx, linkedAccountUserByDeviceId.getLinkedBlacklistedUser(), linkedBy, UserType.BLACKLISTED.toString());
//           // helper.linkUser(uidx, linkedAccountUserByDeviceId.getLinkedBlacklistedUser(), linkedBy, UserType.BLACKLISTED.toString());
//            helper.markAbuserByLinkedAccount(uidx, linkedAccountUserByDeviceId.getLinkedBlacklistedUser(), linkedBy, UserType.BLACKLISTED.toString())
//            
//            if(StringUtils.equals(linkedBy, LinkedBy.DEVICE.toString()))
//                statsDClient.increment("device.linked.account.blacklisted");
//            else
//                statsDClient.increment("phone.linked.account.blacklisted");
//        }
//        
//        else if(linkedAccountUserByDeviceId.getReturnAbuser()){
//            LOGGER.debug("Marking Return Abuser uidx: "+ uidx + ". Reason: Linked by: "+ linkedBy);
//            //atsService.markReturnAbuser(uidx, linkedBy);
//            
//            helper.markReturnAbuser(uidx, linkedBy);
//            helper.linkUser(uidx, linkedAccountUserByDeviceId.getLinkedReturnAbuser(), linkedBy, UserType.RETURN_ABUSER.toString());
//            
//            //atsService.linkUser(uidx, linkedAccountUserByDeviceId.getLinkedReturnAbuser(), linkedBy, UserType.RETURN_ABUSER.toString());
//            if(StringUtils.equals(linkedBy, LinkedBy.DEVICE.toString()))
//                statsDClient.increment("device.linked.account.returnabuser");
//            else
//                statsDClient.increment("phone.linked.account.returnabuser");
//            
//        }
//        
//        else{
//            LOGGER.debug("Marking RTO Abuser uidx: "+ uidx + ". Reason: Linked by: "+ linkedBy);
//            //atsService.setCODLimit(uidx, String.valueOf(linkedAccountUserByDeviceId.getMaxCODlimit()), linkedBy);
//            //atsService.linkUser(uidx, linkedAccountUserByDeviceId.getLinkedRTOAbuser(), linkedBy, UserType.RTO_ABUSER.toString());
//            helper.setCODLimit(uidx, String.valueOf(linkedAccountUserByDeviceId.getMaxCODlimit()), linkedBy);
//            helper.linkUser(uidx, linkedAccountUserByDeviceId.getLinkedBlacklistedUser(), linkedBy, UserType.RTO_ABUSER.toString());
//            if(StringUtils.equals(linkedBy, LinkedBy.DEVICE.toString()))
//                statsDClient.increment("device.linked.account.rtoabuser");
//            else
//                statsDClient.increment("phone.linked.account.rtoabuser");
//        }
    }
}