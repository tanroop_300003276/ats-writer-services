	package com.ats.myntra.atsservice.enums;

public enum CODLimit {
	
	LABEL_ZERO("1", 0), LABEL_1500("2", 1500), LABEL_1500_8("8", 1500);

	private String label;
    private int value;

    CODLimit(String label, int value) {
        this.setLabel(label);
        this.setValue(value);
    }

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}


}
