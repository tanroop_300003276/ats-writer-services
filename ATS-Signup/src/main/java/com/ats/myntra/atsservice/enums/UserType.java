package com.ats.myntra.atsservice.enums;

public enum UserType {
	
	NOT_LINKED("notlinked") , GENUINE("genuine"), BLACKLISTED("blacklisted"), RETURN_ABUSER("returnabuser"), RTO_ABUSER("rtoabuser");
	
	private final String userType;
    
	UserType(String userType) {
        this.userType = userType;
    }

	public String getUserType() {
		return userType;
	}
	
}
