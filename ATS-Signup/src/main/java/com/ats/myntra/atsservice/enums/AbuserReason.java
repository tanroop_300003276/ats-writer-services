package com.ats.myntra.atsservice.enums;

public enum AbuserReason {
	
	LINKED_BY_DEVICE,
	LINKED_BY_PHONE

}
