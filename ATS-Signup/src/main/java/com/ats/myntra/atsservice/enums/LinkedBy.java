package com.ats.myntra.atsservice.enums;

public enum LinkedBy {
	
	PHONE("phone"), DEVICE("device");
	
	private final String linkedby;
    
	LinkedBy(String linkedby) {
        this.linkedby = linkedby;
    }

	public String getLinkedby() {
		return linkedby;
	}
	
	

}
