package com.myntra.shield.messages;

import com.myntra.shield.enums.MessageType;

public class Message {
	
	MessageType messageType;
	
	MessageDetails messageDetails;

	public MessageType getMessageType() {
		return messageType;
	}

	public void setMessageType(MessageType messageType) {
		this.messageType = messageType;
	}

	public MessageDetails getMessageDetails() {
		return messageDetails;
	}

	public void setMessageDetails(MessageDetails messageDetails) {
		this.messageDetails = messageDetails;
	}
	
	

}
