package com.myntra.shield.messages;

import com.myntra.shield.enums.AbuserType;

public class MessageDetails {
	
	String uidx;
	
	Boolean isFakeEmail;
	
	String linkedTo;
	
	String linkedBy;
	
	AbuserType abuserType;
	
	Double netReturnPercentage;
	
	Boolean isReturnAbuser;
	
	Double minCod;
	
	Double maxCod;

	Double rtoPercentageByVolume;
	
	Double rtoPercentageByRevenue;
	
	Boolean isRTOAbuser;
	
	String codReason;
	
	@Override
	public String toString(){
		
		return "ATSOrderEntry [uidx=" + uidx + ", isFakeEmail="
				+ isFakeEmail + ", linkedTo=" + linkedTo
				+ ", linkedBy=" + linkedBy + ", abuserType=" + abuserType
				+ ", netReturnPercentage=" + netReturnPercentage
				+ ", isReturnAbuser=" + isReturnAbuser
				+ ", minCod=" + minCod
				+ ", maxCod=" + maxCod
				+ ", rtoPercentageByVolume=" + rtoPercentageByVolume
				+ ", rtoPercentageByRevenue=" + rtoPercentageByRevenue
				+ ", isRTOAbuser=" + isRTOAbuser
				+ ", codReason=" + codReason
				+ "]";
		
		
	}
	
	public String getUidx() {
		return uidx;
	}

	public void setUidx(String uidx) {
		this.uidx = uidx;
	}

	public Boolean getIsFakeEmail() {
		return isFakeEmail;
	}

	public void setIsFakeEmail(Boolean isFakeEmail) {
		this.isFakeEmail = isFakeEmail;
	}

	public String getLinkedTo() {
		return linkedTo;
	}

	public void setLinkedTo(String linkedTo) {
		this.linkedTo = linkedTo;
	}

	public String getLinkedBy() {
		return linkedBy;
	}

	public void setLinkedBy(String linkedBy) {
		this.linkedBy = linkedBy;
	}

	public AbuserType getAbuserType() {
		return abuserType;
	}

	public void setAbuserType(AbuserType abuserType) {
		this.abuserType = abuserType;
	}

	public Double getNetReturnPercentage() {
		return netReturnPercentage;
	}

	public void setNetReturnPercentage(Double netReturnPercentage) {
		this.netReturnPercentage = netReturnPercentage;
	}

	public Boolean getIsReturnAbuser() {
		return isReturnAbuser;
	}

	public void setIsReturnAbuser(Boolean isReturnAbuser) {
		this.isReturnAbuser = isReturnAbuser;
	}
	
	public Double getMinCod() {
		return minCod;
	}

	public void setMinCod(Double minCod) {
		this.minCod = minCod;
	}

	public Double getMaxCod() {
		return maxCod;
	}

	public void setMaxCod(Double maxCod) {
		this.maxCod = maxCod;
	}

	public Double getRtoPercentageByVolume() {
		return rtoPercentageByVolume;
	}

	public void setRtoPercentageByVolume(Double rtoPercentageByVolume) {
		this.rtoPercentageByVolume = rtoPercentageByVolume;
	}

	public Double getRtoPercentageByRevenue() {
		return rtoPercentageByRevenue;
	}

	public void setRtoPercentageByRevenue(Double rtoPercentageByRevenue) {
		this.rtoPercentageByRevenue = rtoPercentageByRevenue;
	}

	public Boolean getIsRTOAbuser() {
		return isRTOAbuser;
	}

	public void setIsRTOAbuser(Boolean isRTOAbuser) {
		this.isRTOAbuser = isRTOAbuser;
	}

	public String getCodReason() {
		return codReason;
	}

	public void setCodReason(String codReason) {
		this.codReason = codReason;
	}
	
}
