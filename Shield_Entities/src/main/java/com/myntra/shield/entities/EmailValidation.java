package com.myntra.shield.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;

@NamedNativeQuery(name="EmailValidation.getByTimestamp",  query="SELECT * from email_validation where timestamp > :lastRunTime and timestamp < :currentTime"
, resultClass=EmailValidation.class)

@Entity
public class EmailValidation {
	
	@Id
	String uidx;
	
	Boolean isFakeEmail;
	
	String timestamp;

	public String getUidx() {
		return uidx;
	}

	public void setUidx(String uidx) {
		this.uidx = uidx;
	}

	public Boolean getIsFakeEmail() {
		return isFakeEmail;
	}

	public void setIsFakeEmail(Boolean isFakeEmail) {
		this.isFakeEmail = isFakeEmail;
	}

	public String getTimestamp(){
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
}
