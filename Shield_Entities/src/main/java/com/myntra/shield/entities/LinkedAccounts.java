package com.myntra.shield.entities;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;

@NamedNativeQuery(name="LinkedAccounts.getLinkedAccounts",  query="SELECT * from linked_accounts where timestamp > :lastRunTimeStamp and timestamp < :currentTimeStamp"
, resultClass=LinkedAccounts.class)

@Entity
public class LinkedAccounts {
	
	@Id
	String uidx;
	
	String linkedTo;
	
	String linkedBy;
	
	String abuserType;
	
	String timestamp;
	
	Boolean isFreeUser;

	public String getUidx() {
		return uidx;
	}

	public void setUidx(String uidx) {
		this.uidx = uidx;
	}

	public String getLinkedTo() {
		return linkedTo;
	}

	public void setLinkedTo(String linkedTo) {
		this.linkedTo = linkedTo;
	}

	public String getLinkedBy() {
		return linkedBy;
	}

	public void setLinkedBy(String linkedBy) {
		this.linkedBy = linkedBy;
	}

	public String getAbuserType() {
		return abuserType;
	}

	public void setAbuserType(String abuserType) {
		this.abuserType = abuserType;
	}
	

	public Boolean getIsFreeUser() {
		return isFreeUser;
	}

	public void setIsFreeUser(Boolean isFreeUser) {
		this.isFreeUser = isFreeUser;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
}
