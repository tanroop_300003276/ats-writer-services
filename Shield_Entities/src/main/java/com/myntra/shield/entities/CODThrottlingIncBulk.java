package com.myntra.shield.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class CODThrottlingIncBulk {
	
	@Id
	String uidx;
	
	Double minCod;
	
	Double maxCod;

	Double rtoPercentageByVolume;
	
	Double rtoPercentageByRevenue;
	
	Boolean isRTOAbuser;
	
	String codReason;
	
	String timestamp;

	public String getUidx() {
		return uidx;
	}

	public void setUidx(String uidx) {
		this.uidx = uidx;
	}
	
	public Double getMinCod() {
		return minCod;
	}

	public void setMinCod(Double minCod) {
		this.minCod = minCod;
	}

	public Double getMaxCod() {
		return maxCod;
	}

	public void setMaxCod(Double maxCod) {
		this.maxCod = maxCod;
	}

	public Double getRtoPercentageByVolume() {
		return rtoPercentageByVolume;
	}

	public void setRtoPercentageByVolume(Double rtoPercentageByVolume) {
		this.rtoPercentageByVolume = rtoPercentageByVolume;
	}

	public Double getRtoPercentageByRevenue() {
		return rtoPercentageByRevenue;
	}

	public void setRtoPercentageByRevenue(Double rtoPercentageByRevenue) {
		this.rtoPercentageByRevenue = rtoPercentageByRevenue;
	}

	public Boolean getIsRTOAbuser() {
		return isRTOAbuser;
	}

	public void setIsRTOAbuser(Boolean isRTOAbuser) {
		this.isRTOAbuser = isRTOAbuser;
	}
	
	public String getCodReason() {
		return codReason;
	}

	public void setCodReason(String codReason) {
		this.codReason = codReason;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}	
	
}
