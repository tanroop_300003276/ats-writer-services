package com.myntra.shield.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class ReturnAbusers {
	
	@Id
	String uidx;
	
	Double netReturnPercentage;
	
	Boolean isReturnAbuser;
	
	String timestamp;

	public String getUidx() {
		return uidx;
	}

	public void setUidx(String uidx) {
		this.uidx = uidx;
	}

	public Double getNetReturnPercentage() {
		return netReturnPercentage;
	}

	public void setNetReturnPercentage(Double netReturnPercentage) {
		this.netReturnPercentage = netReturnPercentage;
	}

	public Boolean getIsReturnAbuser() {
		return isReturnAbuser;
	}

	public void setIsReturnAbuser(Boolean isReturnAbuser) {
		this.isReturnAbuser = isReturnAbuser;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
}
