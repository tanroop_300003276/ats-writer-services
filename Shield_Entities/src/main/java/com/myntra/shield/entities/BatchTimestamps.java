package com.myntra.shield.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class BatchTimestamps {
	
	@Id
	String jobName;
	
	String timestamp;

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
}
