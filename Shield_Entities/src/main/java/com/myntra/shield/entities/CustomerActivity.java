package com.myntra.shield.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

//@NamedNativeQuery(name="CustomerActivity.getCustomerActivity",  query="SELECT COALESCE(ev.uidx, cth.uidx, ra.uidx) AS uidx, ev.is_fake_email, cth.max_cod,  cth.rto_percentage_by_volume, cth.rto_percentage_by_revenue, cth.is_rto_abuser, ra.net_return_percentage, ra.is_return_abuser  from email_validation ev "
//		+ " outer join cod_throttling_inc_bulk cth on ev.uidx = cth.uidx "
//		+ " outer join return_abusers ra on cth.uidx = ra.uidx "
//		, resultClass=CustomerActivity.class)


@Entity
public class CustomerActivity {
	@Id
	String uidx;
	
	Boolean isFakeEmail;
	
	Double netReturnPercentage;

	Boolean isReturnAbuser;
	
	Double minCod;
	
	@Column(name = "maxCod")
	Double maxCod;

	Double rtoPercentageByVolume;
	
	Double rtoPercentageByRevenue;
	
	Boolean isRtoAbuser;
	
	String codReason;
	
//	@Column(name="timestamp")
//	String timestamp;

	public String getUidx() {
		return uidx;
	}

	public void setUidx(String uidx) {
		this.uidx = uidx;
	}

	public Boolean getIsFakeEmail() {
		return isFakeEmail;
	}

	public void setIsFakeEmail(Boolean isFakeEmail) {
		this.isFakeEmail = isFakeEmail;
	}

	public Double getNetReturnPercentage() {
		return netReturnPercentage;
	}

	public void setNetReturnPercentage(Double netReturnPercentage) {
		this.netReturnPercentage = netReturnPercentage;
	}

	public Boolean getIsReturnAbuser() {
		return isReturnAbuser;
	}

	public void setIsReturnAbuser(Boolean isReturnAbuser) {
		this.isReturnAbuser = isReturnAbuser;
	}
	
	public Double getMinCod() {
		return minCod;
	}

	public void setMinCod(Double minCod) {
		this.minCod = minCod;
	}

	public Double getMaxCod() {
		return maxCod;
	}

	public void setMaxCod(Double maxCod) {
		this.maxCod = maxCod;
	}

	public Double getRtoPercentageByVolume() {
		return rtoPercentageByVolume;
	}

	public void setRtoPercentageByVolume(Double rtoPercentageByVolume) {
		this.rtoPercentageByVolume = rtoPercentageByVolume;
	}

	public Double getRtoPercentageByRevenue() {
		return rtoPercentageByRevenue;
	}

	public void setRtoPercentageByRevenue(Double rtoPercentageByRevenue) {
		this.rtoPercentageByRevenue = rtoPercentageByRevenue;
	}

	public Boolean getIsRtoAbuser() {
		return isRtoAbuser;
	}

	public void setIsRtoAbuser(Boolean isRtoAbuser) {
		this.isRtoAbuser = isRtoAbuser;
	}

	public String getCodReason() {
		return codReason;
	}

	public void setCodReason(String codReason) {
		this.codReason = codReason;
	}
	
	

//	public String getTimestamp() {
//		return timestamp;
//	}
//
//	public void setTimestamp(String timestamp) {
//		this.timestamp = timestamp;
//	}
	
}
