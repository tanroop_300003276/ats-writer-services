package com.myntra.shield.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.myntra.shield.entities.CODThrottling;
import com.myntra.shield.entities.CODThrottlingIncBulk;

public interface CODThrottlingIncBulkRepository extends CrudRepository<CODThrottlingIncBulk, String>{

//	List<CODThrottling> getCODThrottlingResults(@Param("lastRunTimeStamp")String lastRunTimeStamp, @Param("currentTimeStamp")String currentTimeStamp);

}