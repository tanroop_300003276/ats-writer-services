package com.myntra.shield.repositories;

import org.springframework.data.repository.CrudRepository;

import com.myntra.shield.entities.BatchTimestamps;


public interface BatchTimestampsRepository extends CrudRepository<BatchTimestamps, String>{

}
