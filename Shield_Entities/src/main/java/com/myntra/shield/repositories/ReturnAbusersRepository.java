package com.myntra.shield.repositories;

import org.springframework.data.repository.CrudRepository;

import com.myntra.shield.entities.ReturnAbusers;

public interface ReturnAbusersRepository extends CrudRepository<ReturnAbusers, String>{

}
