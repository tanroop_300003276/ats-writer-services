package com.myntra.shield.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.myntra.shield.entities.CODThrottling;

public interface CODThrottlingRepository extends CrudRepository<CODThrottling, String>{

	List<CODThrottling> getCODThrottlingResults(@Param("lastRunTimeStamp")String lastRunTimeStamp, @Param("currentTimeStamp")String currentTimeStamp);

//	List<CODThrottling> getCODThrottlingResults();
}
