package com.myntra.shield.repositories;

import org.springframework.data.repository.CrudRepository;

import com.myntra.shield.entities.CustomerActivity;

public interface CustomerActivityRepository extends CrudRepository<CustomerActivity, String>{

}
