package com.myntra.shield.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.myntra.shield.entities.EmailValidation;

public interface EmailValidationRepository extends CrudRepository<EmailValidation, String>{
	
	List<EmailValidation> getByTimestamp(@Param("lastRunTime")String lastRunTime, @Param("currentTime")String currentTime);

}
