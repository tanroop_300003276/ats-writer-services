package com.myntra.shield.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.myntra.shield.entities.LinkedAccounts;

public interface LinkedAccountsRepository extends CrudRepository<LinkedAccounts, String>{

	List<LinkedAccounts> getLinkedAccounts(@Param("lastRunTimeStamp")String lastRunTimeStamp, @Param("currentTimeStamp")String currentTimeStamp);

//	List<LinkedAccounts> getLinkedAccounts();
}
