package com.myntra.shield.enums;

public enum ShieldExceptionMessage {
	
	
	DATA_MISSING_EMAIL_VALIDATION("Please ensure that all the fields, namely, 'messageType, uidx, isFakeEmail', are present."),
	DATA_MISSING_LINKED_ACCOUNTS("Please ensure that all the fields, namely, 'messageType, uidx, linkedTo, linkedBy, abuserType', are present."),
	DATA_MISSING_COD_THROTTLING("Please ensure that all the fields, namely, 'messageType, uidx, minCOD, maxCOD, rtoPercentageByRevenue, rtoPercentageByVolume, isRtoAbuser, codReason', are present."),
	DATA_MISSING_RETURN_ABUSER("Please ensure that all the fields, namely, 'messageType, uidx, netReturnPercentage, isReturnAbuser', are present."),
	DATA_MISSING_FREE_ACCOUNT("Please ensure that all the fields, name, 'messageType, uid', are present"),
	
	QUEUE_PUBLISH_ERROR("Error occured while publishing message to queue.");
	
	private final String message;
    
	ShieldExceptionMessage(String message) {
        this.message = message;
    }
    public String getMessage() {
        return message;
    }

}
