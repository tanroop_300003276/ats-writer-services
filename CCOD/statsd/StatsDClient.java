package com.ats.myntra.statsd;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.metrics.Metric;
import org.springframework.boot.actuate.metrics.writer.MetricWriter;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

@Component
public class StatsDClient {
	
	@Autowired
    private MetricWriter metricWriter;

    public void timing(String key, Number value) {
        Metric<Number> metric = new Metric<Number>(String.format("timer.%s", key), value);
        metricWriter.set(metric);
    }

    public void increment(String key) {
        Metric<Number> metric = new Metric<Number>(String.format("counter.%s", key), 1);
        metricWriter.set(metric);
    }

    public void gauge(String key, Number value) {
        Metric<Number> metric = new Metric<Number>(String.format("gauge.%s", key), value);
        metricWriter.set(metric);
    }

    public StopWatch startTiming(String id) {
        StopWatch stopWatch = new StopWatch(id);
        stopWatch.start();
        return stopWatch;
    }

    public void endTiming(StopWatch stopWatch) {
        stopWatch.stop();
        timing(stopWatch.getId(), stopWatch.getLastTaskTimeMillis());
    }

}
