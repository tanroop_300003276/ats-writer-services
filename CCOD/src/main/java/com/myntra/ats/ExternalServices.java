package com.myntra.ats;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.myntra.ats.blocked.client.ATSClient;
import com.myntra.oms.client.OrderClient;
import com.myntra.paymentplan.client.PaymentPlanClient;

@Configuration
public class ExternalServices {
	 @SuppressWarnings("static-access")
	    @Bean
	    public PaymentPlanClient paymentPlanClient(){
	    	PaymentPlanClient paymentPlanClient = new PaymentPlanClient();
	        paymentPlanClient.setTimeout(60000);
	    	return paymentPlanClient;
	        
	    }
	 
	 @SuppressWarnings("static-access")
	    @Bean
	    public ATSClient atsClient(){
	        ATSClient atsClient = new ATSClient();
	        return atsClient;
	    }
	 
	   @SuppressWarnings("static-access")
	    @Bean
	    public OrderClient orderClient(){
	    	OrderClient orderLineClient = new OrderClient();
	    	return orderLineClient;
	        
	    }
	 
}
