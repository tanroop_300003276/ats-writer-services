package com.myntra.ats;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.ExportMetricWriter;
import org.springframework.boot.actuate.endpoint.MetricsEndpoint;
import org.springframework.boot.actuate.endpoint.MetricsEndpointMetricReader;
import org.springframework.boot.actuate.metrics.statsd.StatsdMetricWriter;
import org.springframework.boot.actuate.metrics.writer.MetricWriter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StatsDConfig {
	
	@Value("${statsd.prefix}")
    private String prefix;

    @Value("${statsd.server}")
    private String statsDHost;

    @Value("${statsd.port}")
    private Integer statsDPort;
    
    @Bean
    public MetricsEndpointMetricReader metricsEndpointMetricReader(final MetricsEndpoint metricsEndpoint) {
        return new MetricsEndpointMetricReader(metricsEndpoint);
    }
	
	@Bean
    @ExportMetricWriter
    public MetricWriter metricWriter() {
        return new StatsdMetricWriter(prefix, statsDHost, statsDPort);
    }
	
}
