package com.myntra.ats.messageConsumerImpl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;

import com.myntra.oms.phonix.event.OrderUpdateEventEntry;

@Component
public class OrderUpdateEventEntryConverterImpl{

	private static final Logger logger = LoggerFactory.getLogger(OrderUpdateEventEntryConverterImpl.class);

	@Autowired
	private Jaxb2Marshaller  marshaller;
	
	public OrderUpdateEventEntry convertMessage(Message message) {
		try{
			InputStream is = new ByteArrayInputStream(message.getBody());
			OrderUpdateEventEntry orderUpdateEventEntry = unmarshallXml(is);
			return orderUpdateEventEntry;
		}catch(Exception e){
			logger.error("Exception Inside OrderUpdateEventEntryConverterImpl:convertMessage::",e);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T unmarshallXml(final InputStream xml){
	    try{  
	    	return (T) marshaller.unmarshal(new StreamSource(xml));
	    }catch(Exception e){
	    	logger.info("Failed to convert LineEvent {}",xml,e);
	    	return null;
	    }
	    }

}
