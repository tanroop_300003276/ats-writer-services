package com.myntra.ats.messageConsumerImpl;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.xml.transform.stream.StreamSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;

import com.myntra.oms.phonix.event.OrderLineUpdateEventEntry;

@Component
public class OrderLineEntryConsumerImpl {

	private static final Logger logger = LoggerFactory.getLogger(OrderLineEntryConsumerImpl.class);
	
	@Autowired
	private Jaxb2Marshaller  marshaller;
	
	public OrderLineUpdateEventEntry convertMessage(Message message) {
		try{
			InputStream is = new ByteArrayInputStream(message.getBody());
			OrderLineUpdateEventEntry orderLineUpdateEventEntry = unmarshallXml(is);
			return orderLineUpdateEventEntry;
		}catch(Exception e){
			logger.error("Exception Inside:OrderLineEntryConsumerImpl:convertMessage::"+e.toString());
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	   public <T> T unmarshallXml(final InputStream xml)  {
		try{
			return (T) marshaller.unmarshal(new StreamSource(xml));
		}catch(Exception e){
			logger.error("Failed to convert OrderEvent {}",xml,e);
			return null;
		}
	   }
	

}