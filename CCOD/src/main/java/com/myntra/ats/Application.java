package com.myntra.ats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Import;

import com.myntra.ats.reconciliation.scheduler.SchedulerConfig;

@SpringBootApplication(exclude={HibernateJpaAutoConfiguration.class, FreeMarkerAutoConfiguration.class, DataSourceAutoConfiguration.class, CassandraDataAutoConfiguration.class})
@Import({ SchedulerConfig.class })
@EnableConfigurationProperties
public class Application {
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(Application.class, args);
    }
    
}
