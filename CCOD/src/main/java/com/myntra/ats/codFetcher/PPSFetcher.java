package com.myntra.ats.codFetcher;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.myntra.oms.phonix.event.OrderUpdateEventEntry;
import com.myntra.paymentplan.client.PaymentPlanClient;
import com.myntra.paymentplan.domain.request.PaymentPlanRequest;
import com.myntra.paymentplan.domain.response.PaymentPlanResponse;

@Component
public class PPSFetcher {

	private static final Logger logger = LoggerFactory.getLogger(PPSFetcher.class);

	@Autowired
	PaymentPlanClient paymentPlanClient;
	
	public PaymentPlanResponse getPaymentPlanResponse(OrderUpdateEventEntry orderUpdateEventEntry){
		try{
			PaymentPlanRequest paymentPlanRequest = new PaymentPlanRequest();
			paymentPlanRequest.setPpsId(orderUpdateEventEntry.getUpdatedEntry().getPaymentPpsId());
			int count = 0;
			int maxTries = 3;
			while(true) {
				try{
					PaymentPlanResponse paymentPlanResponse = paymentPlanClient.getPaymentPlan(paymentPlanRequest);
					return paymentPlanResponse;
				}catch(Exception e){
					 if (++count == maxTries){
						 logger.error("Reached max retries PPSFetcher:getPaymentPlanResponse:paymentPlanClient",e);
						 break;
					 }
				}
			}
		}catch(Exception e){
			logger.error("Failed to get PaymentPlanResponse for ppsId {}",orderUpdateEventEntry.getUpdatedEntry().getPaymentPpsId(),e);
		}
		return null;
		
	}
	
}
