package com.myntra.ats.manager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.myntra.ats.ATSRedisInteractor.ATSRedis;
import com.myntra.ats.blocked.client.CODChange;
import com.myntra.ats.listener.RedisInteractor.RedisClusterInteractor;
import com.myntra.ats.messageFilterer.OrderLineEntryFilterer;
import com.myntra.ats.statsD.StatsDClient;
import com.myntra.oms.client.entry.OrderLineEntry;

@Component
public class OrderLineEntryManager {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderEventManager.class);	
	
	@Autowired
	OrderLineEntryFilterer orderLineEntryFilterer;
	
	@Autowired
	RedisClusterInteractor redisClusterInteractor;
	
	@Autowired
	StatsDClient statsDClient;
	
	@Autowired
	ATSRedis atsRedis;
	
	public void updateATS(OrderLineEntry orderLineEntry){
		try{
			StopWatch stopWatchFilter = statsDClient.startTiming("order-line-filter");
			if(!orderLineEntryFilterer.orderLineEntryFilterer(orderLineEntry)){
				statsDClient.endTiming(stopWatchFilter);
				return;
			}
			statsDClient.endTiming(stopWatchFilter);
			logger.info("Passed filter class orderId {}",orderLineEntry.getOrderId());
			statsDClient.increment("order-line-filter-passed");
			
			CODChange codChange = redisClusterInteractor.getATSRedisObject(orderLineEntry);
			logger.info("orderLine: codChange object: {} for orderId: {}",codChange.toString(),orderLineEntry.getOrderId());
			statsDClient.increment("ats-api-call");
			StopWatch stopWatchATS = statsDClient.startTiming("ats-api-call");
			codChange = atsRedis.setAmountToRedis(codChange);
			statsDClient.endTiming(stopWatchATS);
		}catch(Exception e){
			logger.info("Exception Inside:OrderLineEntryManager:updateATS::",e);
		}
	}
}
