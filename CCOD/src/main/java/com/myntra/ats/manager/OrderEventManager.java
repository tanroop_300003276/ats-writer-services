package com.myntra.ats.manager;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.myntra.ats.ATSRedisInteractor.ATSRedis;
import com.myntra.ats.RedisDataFormatter.RedisObject;
import com.myntra.ats.blocked.client.CODChange;
import com.myntra.ats.codFetcher.PPSFetcher;
import com.myntra.ats.listener.RedisInteractor.RedisClusterInteractor;
import com.myntra.ats.messageFilterer.OrderEventFilterer;
import com.myntra.ats.redisEntities.CumulativeCODRedisObject;
import com.myntra.ats.statsD.StatsDClient;
import com.myntra.oms.phonix.event.OrderUpdateEventEntry;
import com.myntra.paymentplan.domain.response.PaymentPlanResponse;

@Component
public class OrderEventManager {

	private static final Logger logger = LoggerFactory.getLogger(OrderEventManager.class);

	@Autowired
	PPSFetcher ppsFetcher;
	
	@Autowired
	ATSRedis atsRedis;
	
	@Autowired
	RedisObject redisObject;
	
	@Autowired
	RedisClusterInteractor redisClusterInteractor;
	
	@Autowired
	OrderEventFilterer orderEventFilterer;
	
	@Autowired
	StatsDClient statsDClient;
	
	public void updateATS(OrderUpdateEventEntry orderUpdateEventEntry) {
		
		try{
			StopWatch stopWatchFilter = statsDClient.startTiming("order-event-filter");
			if(!orderEventFilterer.orderEventFilter(orderUpdateEventEntry)){
				statsDClient.endTiming(stopWatchFilter);
				return;
			}
			statsDClient.endTiming(stopWatchFilter);
			logger.info("Passed filter class orderId {}",orderUpdateEventEntry.getUpdatedEntry().getOrderReleases().get(0).getOrderId());
			statsDClient.increment("order-event-filter-paased");
			
			StopWatch stopWatchPaymentClient = statsDClient.startTiming("payment-call");
			statsDClient.increment("payment-api-call");
			PaymentPlanResponse paymentPlanResponse = ppsFetcher.getPaymentPlanResponse(orderUpdateEventEntry);
			statsDClient.endTiming(stopWatchPaymentClient);
			logger.info("Got pps response for orderId {}",orderUpdateEventEntry.getUpdatedEntry().getOrderReleases().get(0).getOrderId());
			
			if(paymentPlanResponse == null)
				return;
			
			CumulativeCODRedisObject cumulativeCODRedisObject = redisObject.getRedisObjectFromPPSResponse(orderUpdateEventEntry, paymentPlanResponse);
			logger.info("Created ccod Redis Object orderId {}",orderUpdateEventEntry.getUpdatedEntry().getOrderReleases().get(0).getOrderId());
			
			if(cumulativeCODRedisObject.getMap() != null && redisClusterInteractor.update(cumulativeCODRedisObject)){
				statsDClient.increment("order.event.entry.stored.Redis");
				
				CODChange codChange = atsRedis.getCODChange(cumulativeCODRedisObject); 
				logger.info("codChange object: {} for orderId: {}",codChange.toString(),orderUpdateEventEntry.getUpdatedEntry().getOrderReleases().get(0).getOrderId());
				StopWatch stopWatchATS = statsDClient.startTiming("ats-api-call");
				statsDClient.increment("ats-api-call");
				codChange = atsRedis.setAmountToRedis(codChange);
				statsDClient.endTiming(stopWatchATS);
			}
		}catch(Exception e){
			logger.error("Exception Inside:OrderEventManager:updateATS::",e);
		}
	}
	
}