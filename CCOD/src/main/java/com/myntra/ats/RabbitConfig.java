package com.myntra.ats;


import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.myntra.ats.listener.OrderEventListener;
import com.myntra.ats.listener.OrderLineEntryListener;

@Configuration
public class RabbitConfig {
	
	@Value("${queue.orderEventQueue}")
	private String orderEventQueue;
	
	
	@Value("${queue.orderLine}")
	private String orderLineQueue;
	
	@Bean
    Queue orderEventQueue() {
        return new Queue(orderEventQueue, true);
    }
	
	
	@Bean
    Queue orderLineQueue() {
        return new Queue(orderLineQueue, true);
    }
	
	@Autowired
	OrderEventListener orderEventListener;
	
	@Autowired
	OrderLineEntryListener orderLineQueueListener;
	
	
    @Bean
    SimpleMessageListenerContainer orderEventContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(orderEventQueue);
        container.setMessageListener(orderEventListenerAdapter());
        container.setConcurrentConsumers(5);
        return container;
    }
    
    @Bean
    SimpleMessageListenerContainer orderLineQueueContainer(ConnectionFactory connectionFactory) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(orderLineQueue);
        container.setMessageListener(orderLineQueueListenerAdapter());
        container.setConcurrentConsumers(5);
        return container;
    }
    

    @Bean
    MessageListenerAdapter orderEventListenerAdapter() {
        return new MessageListenerAdapter(orderEventListener, "onMessage");
    }
    
    
    @Bean
    MessageListenerAdapter orderLineQueueListenerAdapter() {
        return new MessageListenerAdapter(orderLineQueueListener, "onMessage");
    }
    

}