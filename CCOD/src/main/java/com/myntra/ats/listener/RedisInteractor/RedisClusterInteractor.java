package com.myntra.ats.listener.RedisInteractor;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;
import org.springframework.util.StopWatch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.ats.blocked.client.CODChange;
import com.myntra.ats.redisEntities.CumulativeCODRedisObject;
import com.myntra.ats.statsD.StatsDClient;
import com.myntra.oms.client.entry.OrderLineEntry;

@Repository
public class RedisClusterInteractor {
	
	private static final Logger logger = LoggerFactory.getLogger(RedisClusterInteractor.class);
	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	private ValueOperations<String, Object> opsForValue;
	
	@Autowired
	StatsDClient statsDClient;
		
	@PostConstruct
    private void init() {
		opsForValue = redisTemplate.opsForValue();
    }
	
	public Boolean update(CumulativeCODRedisObject cumulativeCODRedisObject){
		try{
			if(redisTemplate.hasKey("orderId:"+String.valueOf(cumulativeCODRedisObject.getOrderId()))){
				logger.info("RedisClusterInteractor:update Key is present in Redis Cluster {}",cumulativeCODRedisObject.getOrderId());
				return false;
			}
			StopWatch stopWatchRedis = statsDClient.startTiming("order-event-redis-set");
			statsDClient.increment("order-event-redis-set");
			opsForValue.set("orderId:"+String.valueOf(cumulativeCODRedisObject.getOrderId()), cumulativeCODRedisObject);
			statsDClient.endTiming(stopWatchRedis);
			logger.info("RedisClusterInteractor:update orderId stored in Redis Cluster {}",cumulativeCODRedisObject.getOrderId());
			return true;
		}catch(Exception e){
			logger.error("Exception Inside:RedisClusterInteractor:update::",e);
		}
		return false;
	}
	
	public Boolean updateForLine(CumulativeCODRedisObject cumulativeCODRedisObject){
		try{
			if(!redisTemplate.hasKey("orderId:"+String.valueOf(cumulativeCODRedisObject.getOrderId()))){
				logger.info("RedisClusterInteractor:update Key is not present in Redis Cluster {}",cumulativeCODRedisObject.getOrderId());
				return false;
			}
			statsDClient.increment("redis-orderId-updated");
			StopWatch stopWatchRedis = statsDClient.startTiming("order-line-redis-set");
			opsForValue.set("orderId:"+String.valueOf(cumulativeCODRedisObject.getOrderId()), cumulativeCODRedisObject);
			statsDClient.endTiming(stopWatchRedis);
			logger.info("RedisClusterInteractor:update orderId stored in Redis Cluster {}",cumulativeCODRedisObject.getOrderId());
			return true;
		}catch(Exception e){
			logger.error("Exception Inside:RedisClusterInteractor:update::",e);
		}
		return false;
	}
	
	public Boolean isKeyPresent(OrderLineEntry orderLineEntry){
		try{
			if(redisTemplate.hasKey("orderId:"+String.valueOf(orderLineEntry.getOrderId())))
				return true;
		}catch(Exception e){
			logger.error("Exception Inside:RedisClusterInteractor:isKeyPresent::",e);
		}
		return false;
	}
	
	public CODChange getATSRedisObject(OrderLineEntry orderLineEntry){
		CODChange codChange = new CODChange();
		try{
			CumulativeCODRedisObject cumulativeCODRedisObject = fetchObjectFromRedis(orderLineEntry);
			codChange = getCODChange(orderLineEntry, cumulativeCODRedisObject);
		}catch(Exception e){
			logger.error("Exception Inside:RedisClusterInteractor:getATSRedisObject::",e);
		}
		return codChange;	
	}
	
	
	public CumulativeCODRedisObject fetchObjectFromRedis(OrderLineEntry orderLineEntry){
		try{
			StopWatch stopWatchRedis = statsDClient.startTiming("fetch-redis-order-object");
			statsDClient.increment("redis-fetch");
			Object object = opsForValue.get("orderId:"+String.valueOf(orderLineEntry.getOrderId()));
			statsDClient.endTiming(stopWatchRedis);
			byte[] json = mapper.writeValueAsBytes(object);
			CumulativeCODRedisObject cumulativeCODRedisObject = mapper.readValue(json,CumulativeCODRedisObject.class);
			logger.info("RedisClusterInteractor:fetchObjectFromRedis cumulativeCODRedisObject received from Redis {}",cumulativeCODRedisObject.toString());
			return cumulativeCODRedisObject;
		}catch(Exception e){
			logger.error("Exception Inside:RedisClusterInteractor:fetchObjectFromRedis::",e);
		}
		return null;
	}
	
	public CODChange getCODChange(OrderLineEntry orderLineEntry, CumulativeCODRedisObject cumulativeCODRedisObject){
		CODChange codChange = new CODChange();
		try{
			codChange.setLogin(cumulativeCODRedisObject.getLogin());
			Double amount = new Double(fetchCODofSKU(orderLineEntry,cumulativeCODRedisObject));
			codChange.setAmount(amount);
		}catch(Exception e){
			logger.error("Exception Inside:RedisClusterInteractor:getCODChange::",e);
		}
		
		return codChange;
	}
	
	public double fetchCODofSKU(OrderLineEntry orderLineEntry, CumulativeCODRedisObject cumulativeCODRedisObject){
		long additionalAmount, CODofSkuLine = 0;
		double amount = 0;
		try{
			Integer quantity = orderLineEntry.getQuantity();
			long codTotal = cumulativeCODRedisObject.getMap().get(orderLineEntry.getSkuId().toString()).getCodValue();
			Integer quantityInRedis = cumulativeCODRedisObject.getMap().get(orderLineEntry.getSkuId().toString()).getQuantity();
			long codTotalPerQuantity = codTotal/quantityInRedis;
			cumulativeCODRedisObject.getMap().get(orderLineEntry.getSkuId().toString()).setQuantity(quantityInRedis - quantity);
			cumulativeCODRedisObject.getMap().get(orderLineEntry.getSkuId().toString()).setCodValue(codTotal - (codTotalPerQuantity*quantity));
			additionalAmount = deleteOrderLineEntryFromMap(orderLineEntry,cumulativeCODRedisObject);
			CODofSkuLine = codTotalPerQuantity*quantity + additionalAmount;	
			amount = paiseToRupee(CODofSkuLine);
			logger.info("RedisClusterInteractor:fetchCODofSKU COD value of sku: {} is {}",orderLineEntry.getSkuId(),amount);
			}catch(Exception e){
				logger.error("Exception Inside:RedisClusterInteractor:fetchCODofSKU::",e);
			}
		return amount;
	}
	
	public double paiseToRupee(long amount){
		double rupee = 0;
		try{
			rupee = (double)(amount)/100;
			rupee =  Math.floor(rupee);
			logger.info("RedisClusterInteractor:paiseToRupee Received paise {} coverted rupee {}",amount,rupee);
		}catch(Exception e){
			logger.error("Exception Inside:RedisClusterInteractor:paiseToRupee::",e);
		}
		return rupee;
	}
	
	public long deleteOrderLineEntryFromMap(OrderLineEntry orderLineEntry,CumulativeCODRedisObject cumulativeCODRedisObject){
		long additionalAmount = 0;
		try{
			Integer quantityLeft = cumulativeCODRedisObject.getMap().get(orderLineEntry.getSkuId().toString()).getQuantity();
			if(quantityLeft.equals(0)){
				logger.info("RedisClusterInteractor:deleteOrderLineEntryFromMap Removing skuId {} from Map",orderLineEntry.getSkuId());
				cumulativeCODRedisObject.getMap().remove(orderLineEntry.getSkuId().toString());
			}
			if(cumulativeCODRedisObject.getMap().isEmpty()){
				additionalAmount = cumulativeCODRedisObject.getAdditionalAmount();
				logger.info("RedisClusterInteractor:deleteOrderLineEntryFromMap Deleting key {} from Redis",orderLineEntry.getOrderId());
				statsDClient.increment("orderId-deleted-from-redis");
				redisTemplate.delete("orderId:"+orderLineEntry.getOrderId().toString());
			}
			else{
				updateForLine(cumulativeCODRedisObject);
			}
		}catch(Exception e){
			logger.error("Exception Inside:RedisClusterInteractor:deleteOrderLineEntryFromMap::",e);
		}
		return additionalAmount;
	}
	
}
