package com.myntra.ats.listener;

import java.util.Date;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.myntra.ats.manager.OrderEventManager;
import com.myntra.ats.messageConsumerImpl.OrderUpdateEventEntryConverterImpl;
import com.myntra.ats.statsD.StatsDClient;
import com.myntra.ats.utils.FeatureGate;
import com.myntra.oms.phonix.event.OrderUpdateEventEntry;

@Component
public class OrderEventListener implements MessageListener {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderEventListener.class);
	
	@Autowired
	OrderUpdateEventEntryConverterImpl messageConverter;
	@Autowired
	OrderEventManager orderEventManager;
	@Autowired
	StatsDClient statsDClient;
	@Autowired
	private FeatureGate featureGate;
	
	@Autowired
	private Jaxb2Marshaller marshaller;

	@Override
	public void onMessage(Message message) {
		
		OrderUpdateEventEntry orderUpdateEventEntry;
		try {
			if(featureGate.fetchFeatureGate("ats.ccod.order.disable", false, Boolean.class)){
				return;
			}
			statsDClient.increment("order-event-count");
			StopWatch stopWatch = statsDClient.startTiming("message-convert-order-event");
			StopWatch stopWatchOrderEvent = statsDClient.startTiming("order-event-timer");
			orderUpdateEventEntry = messageConverter.convertMessage(message);
			statsDClient.endTiming(stopWatch);
			logger.info("Received on:{} OrderUpdateEventEntry:{}",new Date(),orderUpdateEventEntry);
			orderEventManager.updateATS(orderUpdateEventEntry);
			statsDClient.endTiming(stopWatchOrderEvent);
		}catch (Exception e){ 
			logger.error("Exception Inside:OrderEventListener:onMessage::",e);
		}
	}
	
}
