package com.myntra.ats.listener;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.myntra.ats.manager.OrderLineEntryManager;
import com.myntra.ats.messageConsumerImpl.OrderLineEntryConsumerImpl;
import com.myntra.ats.statsD.StatsDClient;
import com.myntra.ats.utils.FeatureGate;
import com.myntra.oms.phonix.event.OrderLineUpdateEventEntry;

@Component
public class OrderLineEntryListener implements MessageListener{

	private static final Logger logger = LoggerFactory.getLogger(OrderLineEntryListener.class);
	
	@Autowired
	OrderLineEntryConsumerImpl messageConverter;
	
	@Autowired
	OrderLineEntryManager orderLineEntryManager;
	
	@Autowired
	StatsDClient statsDClient;
	
	@Autowired
	private FeatureGate featureGate;
	
	@Override
	public void onMessage(Message message) {
		OrderLineUpdateEventEntry orderLineUpdateEventEntry;
		try {
			if(featureGate.fetchFeatureGate("ats.ccod.line.disable", false, Boolean.class)){
				return;
			}
			StopWatch stopWatchOrderEvent = statsDClient.startTiming("order-line-timer");
			statsDClient.increment("order-line-event-count");
			StopWatch stopWatch = statsDClient.startTiming("message-convert-order-line");
			orderLineUpdateEventEntry = messageConverter.convertMessage(message);
			statsDClient.endTiming(stopWatch);
			logger.info("Received on: {} OrderLineEntry object: {}",new Date(),orderLineUpdateEventEntry);
			orderLineEntryManager.updateATS(orderLineUpdateEventEntry.getUpdatedEntry());
			statsDClient.endTiming(stopWatchOrderEvent);
		}catch (Exception e){ 
			logger.error("Exception Inside:OrderLineEntryListener:onMessage::",e);
		}
	}

}
