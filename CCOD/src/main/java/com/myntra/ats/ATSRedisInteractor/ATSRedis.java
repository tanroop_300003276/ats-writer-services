package com.myntra.ats.ATSRedisInteractor;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.myntra.ats.blocked.client.ATSClient;
import com.myntra.ats.blocked.client.CODChange;
import com.myntra.ats.redisEntities.CumulativeCODRedisObject;
import com.myntra.ats.redisEntities.SkuObject;

@Component
public class ATSRedis {
	
	private static final Logger logger = LoggerFactory.getLogger(ATSRedis.class);
	
	@Autowired
	ATSClient atsClient;
	
	public CODChange setAmountToRedis(CODChange codChange){
		try {
			return atsClient.setDiffCOD(codChange);
		}catch (IOException e){
			logger.error("Exception Inside:ATSRedis:setAmountToRedis:: Error while calling ATS Client",e);
		}
		return null;
		
	}
	
	public CODChange getCODChange(CumulativeCODRedisObject cumulativeCODRedisObject){
		CODChange codChange = new CODChange();
		Double codValue = 0.0;
		try{
			codChange.setLogin(cumulativeCODRedisObject.getLogin());
			long amount = cumulativeCODRedisObject.getAdditionalAmount();
			HashMap<String, SkuObject> map = cumulativeCODRedisObject.getMap();
			
			for (Entry<String, SkuObject> entry : map.entrySet()){
			    amount += entry.getValue().getCodValue();
			}
			codValue = (double) (amount)/100;
			codValue = Math.floor(codValue);
			//codValue = BigDecimal.valueOf(codValue).setScale(2,RoundingMode.HALF_DOWN).doubleValue();
			codChange.setAmount(new Double(-codValue));
		}catch(Exception e){
			logger.error("Exception Inside:ATSRedis:getCODChange::",e);
		}
		return codChange;
	}
	
}
