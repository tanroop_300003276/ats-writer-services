package com.myntra.ats.RedisDataFormatter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.myntra.ats.codFetcher.PPSFetcher;
import com.myntra.ats.redisEntities.CumulativeCODRedisObject;
import com.myntra.ats.redisEntities.SkuObject;
import com.myntra.oms.phonix.event.OrderUpdateEventEntry;
import com.myntra.paymentplan.domain.instrument.InstrumentResponseStatus;
import com.myntra.paymentplan.domain.paymentplan.PaymentPlanInstrumentDetailEntry;
import com.myntra.paymentplan.domain.paymentplan.PaymentPlanItemEntry;
import com.myntra.paymentplan.domain.paymentplan.PaymentPlanItemInstrumentDetailEntry;
import com.myntra.paymentplan.domain.response.PaymentPlanResponse;
import com.myntra.paymentplan.enums.PPSItemType;
import com.myntra.paymentplan.enums.PaymentMethod;

@Component
public class RedisObject {
	private static final Logger logger = LogManager.getLogger(RedisObject.class);	

	@Autowired
	PPSFetcher ppsFetcher;
	
	
	public CumulativeCODRedisObject getRedisObjectFromPPSResponse(OrderUpdateEventEntry orderUpdateEventEntry,PaymentPlanResponse paymentPlanResponse) throws Exception{
		try{
			CumulativeCODRedisObject cumulativeCODRedisObject = new CumulativeCODRedisObject();
			cumulativeCODRedisObject.setLogin(orderUpdateEventEntry.getUpdatedEntry().getLogin());
			cumulativeCODRedisObject.setOrderId(orderUpdateEventEntry.getUpdatedEntry().getOrderReleases().get(0).getOrderId());
			cumulativeCODRedisObject.setAdditionalAmount(0);
			cumulativeCODRedisObject.setMap(getSkuOjectHashMap(orderUpdateEventEntry,cumulativeCODRedisObject,paymentPlanResponse));
			cumulativeCODRedisObject.setTimestamp(new SimpleDateFormat("YYYYMMdd HH:mm:ss").format(new Date()));
			return cumulativeCODRedisObject;
		}catch(Exception e){
			logger.info("Exception for orderId {} Inside:RedisObject:getRedisObjectFromPPSResponse::",orderUpdateEventEntry.getUpdatedEntry().getOrderReleases().get(0).getOrderId(),e);
		}
		return null;
	}
	
	public HashMap<String, SkuObject> getSkuOjectHashMap(OrderUpdateEventEntry orderUpdateEventEntry,CumulativeCODRedisObject cumulativeCODRedisObject,PaymentPlanResponse paymentPlanResponse) throws Exception{
		try{
			boolean checkPPSId = false;
			for(PaymentPlanInstrumentDetailEntry paymentPlanInstrumentDetailEntry : paymentPlanResponse.getPaymentPlanEntry().getPaymentPlanInstrumentDetails()){
				if(paymentPlanInstrumentDetailEntry.getInstrumentType().equals(PaymentMethod.COD) && paymentPlanInstrumentDetailEntry.getInstrumentResponseStatus().equals(InstrumentResponseStatus.TX_SUCCESS)){
					checkPPSId = true;
					break;
				}
			}
			if(checkPPSId){
				HashMap<String, SkuObject> hashMap = new HashMap<>();
				Boolean isAdditionalCharge = false;
				for(PaymentPlanItemEntry paymentPlanItemEntry : paymentPlanResponse.getPaymentPlanEntry().getPaymentPlanItems()){
					String key = null;
					SkuObject skuObject = new SkuObject();
					isAdditionalCharge = isPPSItemType(paymentPlanItemEntry.getSkuId());
					for(PaymentPlanItemInstrumentDetailEntry paymentPlanItemInstrumentDetailEntry : paymentPlanItemEntry.getPaymentPlanItemInstrumentDetails()){
						if(paymentPlanItemInstrumentDetailEntry.getInstrumentType().equals(PaymentMethod.COD)){
							if(isAdditionalCharge){
								cumulativeCODRedisObject.setAdditionalAmount(cumulativeCODRedisObject.getAdditionalAmount() + paymentPlanItemInstrumentDetailEntry.getAmount());
								break;
							}
							else{
								key = paymentPlanItemEntry.getSkuId().toString();
								skuObject.setQuantity(paymentPlanItemEntry.getQuantity());
								skuObject.setCodValue(paymentPlanItemInstrumentDetailEntry.getAmount());
							}
						}
					}
					if(key != null)
						hashMap.put(key, skuObject);
				}	
				return hashMap;
			}
			
		}catch(Exception e){
			logger.info("Exception for orderId {} Inside:RedisObject:getSkuOjectHashMap::",orderUpdateEventEntry.getUpdatedEntry().getOrderReleases().get(0).getOrderId(),e);
		}
		return null;
	}
		
	public Boolean isPPSItemType(String skuId) {
	    try{
			for (PPSItemType c : PPSItemType.values()) {
		        if (c.getItemIdentifier().equals(skuId)) {
		            return true;
		        }
		    }
	    }catch(Exception e){
	    	logger.info("Exception Inside:RedisObject:isPPSItemType::",e);
	    }
	    return false;
	}
}
