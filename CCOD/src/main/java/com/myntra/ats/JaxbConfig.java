package com.myntra.ats;

import java.util.HashMap;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.myntra.oms.phonix.event.OrderLineUpdateEventEntry;
import com.myntra.oms.phonix.event.OrderUpdateEventEntry;

@Configuration
public class JaxbConfig {
	 @Bean
	    public Jaxb2Marshaller jaxb2Marshaller() {
	        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
	        marshaller.setClassesToBeBound(new Class[]{
	           //all the classes the context needs to know about
	        		OrderUpdateEventEntry.class,
	        		OrderLineUpdateEventEntry.class
	        }); //"alternatively" setContextPath(<jaxb.context>), 

	        marshaller.setMarshallerProperties(new HashMap<String, Object>() {{
	          put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
	        }});

	        return marshaller;
	    }
}
