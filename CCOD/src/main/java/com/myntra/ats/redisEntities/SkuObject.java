package com.myntra.ats.redisEntities;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SkuObject implements Serializable{
	@JsonProperty
	private Integer quantity;
	@JsonProperty
	private long codValue;
	
	public Integer getQuantity() {
		return quantity;
	}
	public long getCodValue() {
		return codValue;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public void setCodValue(long codValue) {
		this.codValue = codValue;
	}
	
}
