package com.myntra.ats.redisEntities;


import java.util.HashMap;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CumulativeCODRedisObject {
	@JsonProperty
	private Long orderId;
	@JsonProperty
	private String login;
	@JsonProperty
	private long additionalAmount;
	@JsonProperty
	private String timestamp;
	@JsonProperty
	HashMap<String, SkuObject> map = new HashMap<>();
	public Long getOrderId() {
		return orderId;
	}
	public String getLogin() {
		return login;
	}
	public long getAdditionalAmount() {
		return additionalAmount;
	}
	public HashMap<String, SkuObject> getMap() {
		return map;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public void setAdditionalAmount(long additionalAmount) {
		this.additionalAmount = additionalAmount;
	}
	public void setMap(HashMap<String, SkuObject> map) {
		this.map = map;
	}
	@Override
	public String toString() {
		return "CumulativeCODRedisObject [orderId=" + orderId + ", login=" + login + ", additionalAmount="
				+ additionalAmount + ", timestamp=" + timestamp + ", map=" + map + "]";
	}
	
}
