//package com.myntra.ats.messageConsumer;
//
//import java.io.InputStream;
//import java.io.StringReader;
//
//import javax.xml.bind.JAXBContext;
//import javax.xml.bind.JAXBException;
//import javax.xml.bind.Unmarshaller;
//import javax.xml.transform.stream.StreamSource;
//
//import org.springframework.amqp.core.Message;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.oxm.jaxb.Jaxb2Marshaller;
//
//
//
//public interface MessageConverter<T> {
//	
//	public T convertMessage(Message msg);
//	
//	
//	public default T convert(Class<?> T, Message message){
//		
//		try{
//			JAXBContext jaxbContext = JAXBContext.newInstance(T);
//	    	Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
//	    	StringReader reader = new StringReader(new String(message.getBody()));
//	    	T object =  (T) unmarshaller.unmarshal(reader);
//	    	return object;
//			}catch(Exception e){
//				e.printStackTrace();
//			}	
//		return null;
//	}
//	
//	@SuppressWarnings("unchecked")
//	public <T> T unmarshallXml(final InputStream xml) throws JAXBException {
//	      return (T) marshaller.unmarshal(new StreamSource(xml));
//	   }
//}
