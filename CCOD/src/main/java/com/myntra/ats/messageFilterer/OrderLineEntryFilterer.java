package com.myntra.ats.messageFilterer;

import java.util.Arrays;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.myntra.ats.listener.RedisInteractor.RedisClusterInteractor;
import com.myntra.oms.client.entry.OrderLineEntry;

@Component
public class OrderLineEntryFilterer {
	
	private static final Logger logger = LogManager.getLogger(OrderLineEntryFilterer.class);	
	
	@Autowired
	RedisClusterInteractor redisClusterInteractor;
	
	@Value("${valid.orderLine.status}")
	private String[] validOrderLineStatuses;
	
	public Boolean orderLineEntryFilterer(OrderLineEntry orderLineEntry){
		try{
			if(checkExchangeLineId(orderLineEntry) && checkOrderLineStatus(orderLineEntry) && redisClusterInteractor.isKeyPresent(orderLineEntry)){
				logger.info("OrderLineEntryFilterer:orderLineEntryFilterer passed the filter for orderId {}",orderLineEntry.getOrderId());
				return true;
			}
			logger.info("Did not pass the status filter for orderId {}",orderLineEntry.getOrderId());
		}catch(Exception e){
			logger.error("Exception Inside:OrderLineEntryFilterer:orderLineEntryFilterer::",e);
		}
		return false;
	}
	
	public Boolean checkExchangeLineId(OrderLineEntry orderLineEntry){
		try{
			if(orderLineEntry.getExchangeLineId() == null)
				return true;
			logger.info("Did not pass the exchangeID status filter for orderId {}",orderLineEntry.getOrderId());
		}catch(Exception e){
			logger.error("Exception Inside:OrderLineEntryFilterer:checkExchangeLineId::",e);
		}
		return false;
	}
	
	public Boolean checkOrderLineStatus(OrderLineEntry orderLineEntry){
		try{
			if(Arrays.asList(validOrderLineStatuses).contains(orderLineEntry.getStatus()))
				return true;
			logger.info("OrderLineEntryFilterer:checkOrderLineStatus doesn't have a valid status: {}",orderLineEntry.getStatus());
		}catch(Exception e){
			logger.error("Exception Inside:OrderLineEntryFilterer:checkOrderLineStatus::",e);
		}
		return false;
	}
	
		
}
