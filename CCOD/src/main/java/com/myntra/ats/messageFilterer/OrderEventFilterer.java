package com.myntra.ats.messageFilterer;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.myntra.oms.phonix.event.OrderUpdateEventEntry;



@Component
public class OrderEventFilterer {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderEventFilterer.class);
	
	@Value("${valid.orderEntryOld.status}")
	private String[] OrderEntryOldStatus;
	
	@Value("${valid.orderEnrtyUpdated.status}")
	private String OrderEntryUpdatedStatus;
	
	@Value("${paymentMethodCOD}")
	private String paymentMethodCOD;
	
	public Boolean orderEventFilter(OrderUpdateEventEntry orderUpdateEventEntry){
		try{
			if(statusFilter(orderUpdateEventEntry) && checkPaymentMethod(orderUpdateEventEntry) && checkExchangeReleaseId(orderUpdateEventEntry) && checkOnHold(orderUpdateEventEntry)){
				return true;
			}
			
		}catch(Exception e){
			logger.error("Exception Inside:OrderEventFilterer:orderEventFilter",e);
		}
		return false;
	}
	
	public Boolean statusFilter(OrderUpdateEventEntry orderUpdateEventEntry){
		try{
			if(Arrays.asList(OrderEntryOldStatus).contains(orderUpdateEventEntry.getOldEntry().getOrderReleases().get(0).getStatus()) && orderUpdateEventEntry.getUpdatedEntry().getOrderReleases().get(0).getStatus().equals(OrderEntryUpdatedStatus)){
				return true;
			}
		}catch(Exception e){
			logger.error("Exception Inside:OrderEventFilterer:statusFilter",e);
		}
		return false;
	}
	
	public Boolean checkPaymentMethod(OrderUpdateEventEntry orderUpdateEventEntry){
		try{
			if(orderUpdateEventEntry.getUpdatedEntry().getPaymentMethod().equals(paymentMethodCOD))
				return true;
		}catch(Exception e){
			logger.error("Exception Inside:OrderEventFilterer:checkPaymentMthod",e);
		}
		return false;
	}
	
	public Boolean checkExchangeReleaseId(OrderUpdateEventEntry orderUpdateEventEntry){
		try{
			if(orderUpdateEventEntry.getUpdatedEntry().getOrderReleases().get(0).getExchangeReleaseId() == null)
				return true;
		}catch(Exception e){
			logger.error("Exception Inside:OrderEventFilterer:checkExchangeReleaseId",e);
		}
		return false;
	}
	
	public Boolean checkOnHold(OrderUpdateEventEntry orderUpdateEventEntry){
		try{
			if(orderUpdateEventEntry.getUpdatedEntry().getOnHold())
				return false;
		}catch(Exception e){
			logger.error("Exception Inside:OrderEventFilterer:checkOnHold",e);
		}
		return true;
	}
	
}
