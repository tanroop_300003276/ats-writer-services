package com.myntra.ats.reconciliation.omsDataExtractor;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.myntra.ats.reconciliation.utils.OMSAPIResponse;
import com.myntra.ats.redisEntities.CumulativeCODRedisObject;
import com.myntra.oms.client.OrderClient;
import com.myntra.oms.client.response.OrderResponse;

@Component
public class FetchDataFromOMS {
	
	@Autowired
	OrderClient orderClient;
	
	@Autowired
	OMSSlaveSearch omsSlaveSearch;
	
	private static final Logger logger = LoggerFactory.getLogger(FetchDataFromOMS.class);
	
	public ArrayList<CumulativeCODRedisObject> fetchCCODObjectFromOMS(String orderIDList, OMSAPIResponse omsAPIResponse){
		
		try {
			OrderResponse orderResponse = omsSlaveSearch.slaveSearch(orderIDList,omsAPIResponse);
			return omsSlaveSearch.getCumulativeCODRedisObject(orderResponse);
		} catch (Exception e) {
			logger.error("Found error in oms slave search",e);
		}
		return null;
		
	}
	
}
