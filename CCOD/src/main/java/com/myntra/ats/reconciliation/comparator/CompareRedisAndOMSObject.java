package com.myntra.ats.reconciliation.comparator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.myntra.ats.redisEntities.CumulativeCODRedisObject;
import com.myntra.ats.redisEntities.SkuObject;
import com.myntra.oms.client.entry.OrderLineEntry;

@Component
public class CompareRedisAndOMSObject {
	
	private static final Logger logger = LoggerFactory.getLogger(CompareRedisAndOMSObject.class);
	
	public ArrayList<OrderLineEntry> compare(CumulativeCODRedisObject cumulativeCODOMSObject, CumulativeCODRedisObject cumulativeCODRedisObject){
		ArrayList<OrderLineEntry> orderLineEntryList = new ArrayList<OrderLineEntry>();
		try{
			Iterator it = cumulativeCODRedisObject.getMap().entrySet().iterator();
			while (it.hasNext()) {
				OrderLineEntry orderLineEntry = new OrderLineEntry();
				Map.Entry<String,SkuObject> pair = (Map.Entry)it.next();
				logger.info("cumulativeCODOMSObject: {}",cumulativeCODOMSObject.toString());
				logger.info("cumulativeCODRedisObject: {}",cumulativeCODRedisObject.toString());
							
				if(checkSkuExists(pair,cumulativeCODOMSObject)){
					if(cumulativeCODOMSObject.getMap().get(pair.getKey()).getQuantity().compareTo(pair.getValue().getQuantity()) < 0){
						orderLineEntry.setSkuId(Long.valueOf(pair.getKey()));
						orderLineEntry.setOrderId(cumulativeCODOMSObject.getOrderId());
						orderLineEntry.setQuantity(quantityLeft(pair,cumulativeCODOMSObject));
						orderLineEntry.setStatus("D");
					}
					else
						continue;
				}
				else{
					orderLineEntry.setSkuId(Long.valueOf(pair.getKey()));
					orderLineEntry.setOrderId(cumulativeCODRedisObject.getOrderId());
					orderLineEntry.setQuantity(pair.getValue().getQuantity());
					orderLineEntry.setStatus("D");
				}
				orderLineEntryList.add(orderLineEntry);
			}
			return orderLineEntryList;
		}catch(Exception e){
			logger.error("Exception in CompareRedisAndOMSObject:comapre",e);
		}
		return orderLineEntryList;
	}
	
	
	public Boolean checkSkuExists(Map.Entry<String,SkuObject> pair,CumulativeCODRedisObject cumulativeCODOMSObject){
		try{
			return cumulativeCODOMSObject.getMap().containsKey(pair.getKey());
		} catch(Exception e){
			logger.error("Exception in checkSkuExists",e);
			return false;
		}
	}
	
	public Integer quantityLeft(Map.Entry<String,SkuObject> pair,CumulativeCODRedisObject cumulativeCODOMSObject){
		try{
			return (pair.getValue().getQuantity() - cumulativeCODOMSObject.getMap().get(pair.getKey()).getQuantity());
		} catch(Exception e){
			logger.error("Exception in checkSkuExists",e);
			return 0;
		}
	}
	
}
