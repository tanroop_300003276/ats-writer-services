package com.myntra.ats.reconciliation.utils;

import java.util.ArrayList;
import java.util.Date;

import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

import com.myntra.ats.redisEntities.CumulativeCODRedisObject;

@Component
public class ReconciliationUtils {
	
	public CumulativeCODRedisObject findByOrderId(ArrayList<CumulativeCODRedisObject> CumulativeCODOMSObjectList, Long orderId){
		
		for(CumulativeCODRedisObject CumulativeCODOMSObject : CumulativeCODOMSObjectList){
			if(CumulativeCODOMSObject.getOrderId().equals(orderId)){
				return CumulativeCODOMSObject;
			}
		}
		return null;	
	}
	
	public Boolean stopSchedulerAfterThreeHours(JobExecutionContext context){
		Date currentDateTime = new Date();
		int currentHour = currentDateTime.getHours();
		int runtimeHour = context.getFireTime().getHours();
		if((currentHour - runtimeHour) > 3)
			return true;
		return false;
		
	}
	
}
