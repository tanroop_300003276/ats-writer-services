package com.myntra.ats.reconciliation.manager;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import com.myntra.ats.manager.OrderLineEntryManager;
import com.myntra.ats.reconciliation.comparator.CompareRedisAndOMSObject;
import com.myntra.ats.reconciliation.omsDataExtractor.FetchDataFromOMS;
import com.myntra.ats.reconciliation.redisDataExtractor.FetchKeyDataFromRedis;
import com.myntra.ats.reconciliation.redisDataExtractor.FetchValueFromKeys;
import com.myntra.ats.reconciliation.utils.OMSAPIResponse;
import com.myntra.ats.reconciliation.utils.ReconciliationUtils;
import com.myntra.ats.redisEntities.CumulativeCODRedisObject;
import com.myntra.ats.statsD.StatsDClient;
import com.myntra.oms.client.entry.OrderLineEntry;

@Component
public class RecompiliationManager {

	@Autowired
	FetchKeyDataFromRedis fetchKeyDataFromRedis;
	
	@Autowired
	FetchValueFromKeys fetchValueFromKeys;
	
	@Autowired
	FetchDataFromOMS fetchDataFromOMS;
	
	@Autowired
	CompareRedisAndOMSObject compareRedisAndOMSObject;
	
	@Autowired
	OrderLineEntryManager orderLineEntryManager;
	
	@Autowired
	ReconciliationUtils reconciliationUtils;
	
	@Value("${keys.prefix}")
	private String pattern;
	
	@Value("${oms.batch.size}")
	private int batchSize;
	
	@Autowired
	StatsDClient statsDClient;
	
	private static final Logger logger = LoggerFactory.getLogger(RecompiliationManager.class);
	
	public void reconcile(JobExecutionContext context){
		try{
			OMSAPIResponse omsAPIResponse = new OMSAPIResponse();
			StopWatch stopWatchGetKeys = statsDClient.startTiming("gets-keys-redis");
			Iterable<List<String>> batchKeysForRedis = fetchKeyDataFromRedis.makeBatchForKeysRedis(pattern, batchSize);
			statsDClient.endTiming(stopWatchGetKeys);
			logger.info("Fetched batch keys from Redis");
			
			
			ArrayList<String> batchKeysForOMS = fetchKeyDataFromRedis.makeBatchForKeysOMS(batchKeysForRedis);
			logger.info("Got batch keys for oms");
			
			ArrayList<OrderLineEntry> orderLineEntryList = new ArrayList<OrderLineEntry>();
			int i =0;
			int omsApiFail = 0;
			
			for(List<String> batch: batchKeysForRedis){
				try{
					StopWatch stopWatchBatchProcessing = statsDClient.startTiming("batch-processing");
					logger.info("Iterating over batch keys from redis and batch keys for oms");
					if(i >= batchKeysForOMS.size()){
						statsDClient.endTiming(stopWatchBatchProcessing);
						break;
					}
					StopWatch stopWatchGetBatchValues = statsDClient.startTiming("gets-value-for-batch-redis");
					ArrayList<CumulativeCODRedisObject>  CumulativeCODRedisObjectList = fetchValueFromKeys.fetchValueFromKeys(batch);
					statsDClient.endTiming(stopWatchGetBatchValues);
					StopWatch stopWatchGetBatchOMS = statsDClient.startTiming("oms-api-call");
					statsDClient.increment("oms-api-call");
					ArrayList<CumulativeCODRedisObject> CumulativeCODOMSObjectList = fetchDataFromOMS.fetchCCODObjectFromOMS(batchKeysForOMS.get(i),omsAPIResponse);
					statsDClient.endTiming(stopWatchGetBatchOMS);
					i++;
					if(!omsAPIResponse.isResult()){
						omsApiFail++;
					}
					for(CumulativeCODRedisObject cumulativeCODRedisObject :CumulativeCODRedisObjectList){
						if(!omsAPIResponse.isResult()){
							statsDClient.increment("oms-api-call-timeout");
							break;
						}
						CumulativeCODRedisObject cumulativeCODOMSObject = reconciliationUtils.findByOrderId(CumulativeCODOMSObjectList, cumulativeCODRedisObject.getOrderId());
						if(cumulativeCODOMSObject == null){
							logger.info("Found null cumulative cod object");
							continue;
						}
						logger.info("Found not null cumulative cod object",cumulativeCODOMSObject.toString());
						orderLineEntryList = compareRedisAndOMSObject.compare(cumulativeCODOMSObject, cumulativeCODRedisObject);
						if(orderLineEntryList.size() > 0){
							statsDClient.increment("discrepancy-count");
						}
						CumulativeCODOMSObjectList.remove(cumulativeCODOMSObject);
						for(OrderLineEntry orderLineEntry : orderLineEntryList){
							if(orderLineEntryList.size() > 0){
								statsDClient.increment("order-line-created-reconciliation");
								orderLineEntryManager.updateATS(orderLineEntry);
								logger.info("Discrepancy Found for orderLineEntry {}",orderLineEntry);
							}
						}
					}
					statsDClient.gauge("percent-completed", ((float)(i-omsApiFail)/batchKeysForOMS.size())*100);
					logger.info("Percentage Completed: {}", ((float)(i-omsApiFail)/batchKeysForOMS.size())*100);
					
					statsDClient.endTiming(stopWatchBatchProcessing);
					if(reconciliationUtils.stopSchedulerAfterThreeHours(context)){
						statsDClient.increment("exceeded-alotted-hours");
						break;
					}
				}catch(Exception e){
					logger.error("Exception in main loop of reconciliation",e);
				}
			}
		}catch(Exception e){
			logger.error("Exception inside:RecompiliationManager:reconcile",e);
		}
	}

}
