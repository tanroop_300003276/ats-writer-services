package com.myntra.ats.reconciliation.omsDataExtractor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.myntra.ats.reconciliation.utils.OMSAPIResponse;
import com.myntra.ats.redisEntities.CumulativeCODRedisObject;
import com.myntra.ats.redisEntities.SkuObject;
import com.myntra.commons.auth.ContextInfo;
import com.myntra.oms.client.OrderClient;
import com.myntra.oms.client.entry.OrderEntry;
import com.myntra.oms.client.entry.OrderLineEntry;
import com.myntra.oms.client.entry.OrderReleaseEntry;
import com.myntra.oms.client.response.OrderResponse;


@Component
public class OMSSlaveSearch {
	
	@Value("${valid.orderLine.status}")
	private String[] validOrderLineStatuses;
	
	@Autowired
	OrderClient orderClient;
	
	private static final Logger logger = LoggerFactory.getLogger(OMSSlaveSearch.class);
	
	public OrderResponse slaveSearch(String orderIds, OMSAPIResponse omsAPIResponse){
		ContextInfo contextInfo = new ContextInfo();
		contextInfo.setName("Shield");
		contextInfo.setLoginId("Shield");
		contextInfo.setEmail("Shield");
		contextInfo.setPassword("Shield");
		StringBuilder sb = new StringBuilder("id.in:");
		sb.append(orderIds);
		OrderResponse orderResponse = new OrderResponse();
		int count = 0;
		int maxTries = 3;
		while(true) {
			try{
				orderResponse = orderClient.slaveSearch(0, 10, null, null, sb.toString(), null,null, null, null, contextInfo);
				omsAPIResponse.setResult(true); 
				return orderResponse;
			}catch(Exception e){
				 if (++count == maxTries){
					 logger.info("Reached max retries OMSSlaveSearch:slaveSearch1");
					 omsAPIResponse.setResult(false);
					 break;
				 }
			}
		}
		return orderResponse;
					
	}
	
	public ArrayList<CumulativeCODRedisObject> getCumulativeCODRedisObject(OrderResponse orderResponse){
		ArrayList<CumulativeCODRedisObject> cumulativeCODRedisObjectList = new ArrayList<CumulativeCODRedisObject>();
		try{
			for(OrderEntry orderEntry : orderResponse.getData()){
				CumulativeCODRedisObject cumulativeCODRedisObject = new CumulativeCODRedisObject();
				cumulativeCODRedisObject.setLogin(orderEntry.getLogin());
				cumulativeCODRedisObject.setOrderId(orderEntry.getOrderReleases().get(0).getOrderId());
				HashMap<String, SkuObject> hashMap = new HashMap<>();
				String key = null;
				for(OrderReleaseEntry orderReleaseEntry : orderEntry.getOrderReleases()){
					for(OrderLineEntry orderLineEntry : orderReleaseEntry.getOrderLines()){
						if(!Arrays.asList(validOrderLineStatuses).contains(orderLineEntry.getStatus())){
							SkuObject skuObject = new SkuObject();
							key = String.valueOf(orderLineEntry.getSkuId());
							if(hashMap.containsKey(key)){
								skuObject.setQuantity(hashMap.get(key).getQuantity() + orderLineEntry.getQuantity());
							}
							else
								skuObject.setQuantity(orderLineEntry.getQuantity());
							hashMap.put(key, skuObject);
						}
					}
				}
				cumulativeCODRedisObject.setMap(hashMap);
				cumulativeCODRedisObjectList.add(cumulativeCODRedisObject);
			}
		}catch(Exception e){
			logger.info("Exception in OMSSlaveSearch:getCumulativeCODRedisObject",e);
		}
		return cumulativeCODRedisObjectList;		
	}

}
