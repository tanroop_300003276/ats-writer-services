package com.myntra.ats.reconciliation.redisDataExtractor;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myntra.ats.reconciliation.omsDataExtractor.OMSSlaveSearch;
import com.myntra.ats.redisEntities.CumulativeCODRedisObject;

@Component
public class FetchValueFromKeys {
	
	private static final Logger logger = LoggerFactory.getLogger(FetchValueFromKeys.class);
	private ObjectMapper mapper = new ObjectMapper();
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	@Autowired
	OMSSlaveSearch omsSlaveSearch;
	
	private ValueOperations<String, Object> opsForValue;
	
	@PostConstruct
    private void init() {
		opsForValue = redisTemplate.opsForValue();
    }
	
	public ArrayList<CumulativeCODRedisObject> fetchValueFromKeys(List<String> keys) {
		ArrayList<CumulativeCODRedisObject> cumulativeCODRedisObjectList = new ArrayList<CumulativeCODRedisObject>();
		try{
			for(Object object : opsForValue.multiGet(keys)){
				byte[] json;
				json = mapper.writeValueAsBytes(object);
				cumulativeCODRedisObjectList.add(mapper.readValue(json,CumulativeCODRedisObject.class));
			}
			logger.info("Collected values from redis successfully");
		}catch(Exception e){
			logger.error("Exception in fetching values from redis",e);
		}
		return cumulativeCODRedisObjectList;
	}
	
}
