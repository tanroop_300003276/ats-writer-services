package com.myntra.ats.reconciliation.utils;

import org.springframework.stereotype.Component;

@Component
public class OMSAPIResponse {
	private boolean result;

	public boolean isResult() {
		return result;
	}

	public void setResult(boolean result) {
		this.result = result;
	}
	
}
