package com.myntra.ats.reconciliation.job;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;

import com.myntra.ats.reconciliation.manager.RecompiliationManager;
import com.myntra.ats.statsD.StatsDClient;
import com.myntra.ats.utils.FeatureGate;

public class ReconciliationJob implements Job{
	
//	@Autowired
//	private ReconciliationService reconciliationService;
	
	@Autowired
	private RecompiliationManager recompiliationManager;
	
	@Autowired
	private FeatureGate featureGate;
	
	@Autowired
	StatsDClient statsDClient;
	
	private static final Logger logger = LoggerFactory.getLogger(ReconciliationJob.class);
	
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		if(featureGate.fetchFeatureGate("ats.ccod.reconciliation.disable", false, Boolean.class)){
			return;
		}
		StopWatch stopWatchReconciliation = statsDClient.startTiming("reconciliation");
		logger.info("Starting job at {}",new Date());
		recompiliationManager.reconcile(context);
		statsDClient.endTiming(stopWatchReconciliation);
		logger.info("Ending job at {}",new Date());
	}
}