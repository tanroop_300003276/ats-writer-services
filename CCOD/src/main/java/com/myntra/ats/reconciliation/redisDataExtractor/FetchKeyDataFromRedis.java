package com.myntra.ats.reconciliation.redisDataExtractor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import com.google.common.collect.Iterables;

@Component
public class FetchKeyDataFromRedis {
	
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	
	@Autowired
	FetchValueFromKeys fetchValueFromKeys;
	
	private ValueOperations<String, Object> opsForValue;
	
	@PostConstruct
    private void init() {
		opsForValue = redisTemplate.opsForValue();
    }
	
	public Set<String> fetchAllKeys(String pattern){
		return redisTemplate.keys(pattern);
	}
	
	public Iterable<List<String>> makeBatchForKeysRedis(String pattern, int batchSize){
		Set<String>  keys =  redisTemplate.keys(pattern);
		Iterable<List<String>> list = Iterables.partition(keys, batchSize);
		return list;	
	}
	
	public ArrayList<String> makeBatchForKeysOMS(Iterable<List<String>> list){
		String str;
		ArrayList<String> orderIdLists = new ArrayList<String>();
		for(List<String> l1: list){
			str = createOrderIdList(l1);
			if(!str.equals(""))
				orderIdLists.add(str);
		}
		return orderIdLists;
	}
	
	public String createOrderIdList(List<String> list){
		StringBuilder sb = new StringBuilder("");
		String str[];
		for(String orderId: list){
			str = orderId.split("orderId:");
			if(str.length > 1){
				sb.append(str[1]);
				sb.append(',');
			}
		}
		if(!sb.toString().equals(""))
			sb.deleteCharAt(sb.length()-1);
		
		return sb.toString();
	}
		
}
