from rediscluster import StrictRedisCluster
from datetime import datetime
import codecs,json,datetime,os

#Connect to Redis Cluster
startup_nodes = [{"host": "10.84.244.66", "port": "7000"}]
rc = StrictRedisCluster(startup_nodes=startup_nodes, decode_responses=False)

#Fetch Current Date - 30days
expiryTimeStamp = datetime.datetime.now() - datetime.timedelta(days=30)



def processData():
	keys = rc.keys("orderId:*")
	for orderId in keys:
		response = json.loads(rc.get(orderId))
		print response
		if(checkTimeStamp(response)):
			amount = fecthCOD(response)
			updateValueInRedis(response['login'],amount)
			deleteRedisEntry(orderId)
			


def fecthCOD(response):
	if(response['map'] is None):
		return 0
	amount = response['additionalAmount']
	for key in response['map']:
		amount += response['map'][key]['codValue']
		return amount


def checkTimeStamp(response):
	responseTimeStamp = response['timestamp']
	responseTimeStamp = datetime.datetime.strptime(str(responseTimeStamp), "%Y%m%d %H:%M:%S")
	if(responseTimeStamp < expiryTimeStamp):
		return True



def deleteRedisEntry(orderId):
	rc.delete(orderId)



def updateValueInRedis(login,amount):
	url = 'http://ats.myntra.com/myntra-ats-service/customer/cod/difference/set'
	count = 0
	maxTries = 3
	amount = amount/100
	while count < maxTries:
		try:
			out="curl -X PUT -H \"Accept: application/json\" -H \"Content-Type: application/json\" -H \"Cache-Control: no-cache\" \
			 -d '{\"login\":\""+str(login)+"\",\"amount\":\""+str(amount)+"\"}' "+ url
			os.system(out)
			count = maxTries
			break
		except Exception as e:
			count += 1
			if (count == maxTries):
				raise e;


def main():
	processData()


if __name__== "__main__":
	main()