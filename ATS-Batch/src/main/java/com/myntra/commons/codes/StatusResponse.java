/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.myntra.commons.codes;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.myntra.commons.code.StatusCodes;

@XmlRootElement(name = "status")
public class StatusResponse implements Serializable {
	private static final long serialVersionUID = 5521850192852967435L;
	private int statusCode;
	private String statusMessage = "";
	private Type statusType = Type.SUCCESS;
	private long totalCount;

	public StatusResponse() {
	}

	public StatusResponse(int statusCode, String statusMessage, Type type) {
		this.statusCode = statusCode;
		this.statusMessage = statusMessage;
		this.statusType = type;
	}

	public StatusResponse(StatusCodes err, Type type) {
		this.statusCode = err.getStatusCode();
		this.statusMessage = err.getMessage();
		this.statusType = type;
	}

	public StatusResponse(StatusCodes err, Type type, long totalCount) {
		this.statusCode = err.getStatusCode();
		this.statusMessage = err.getMessage();
		this.statusType = type;
		this.totalCount = totalCount;
	}

	public int getStatusCode() {
		return this.statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return this.statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public Type getStatusType() {
		return this.statusType;
	}

	public void setStatusType(Type statusType) {
		this.statusType = statusType;
	}

	public long getTotalCount() {
		return this.totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	@com.fasterxml.jackson.annotation.JsonIgnore
	@org.codehaus.jackson.annotate.JsonIgnore
	@XmlTransient
	public boolean isSuccess() {
		return (this.statusType == Type.SUCCESS);
	}

	public String toString() {
		return "StatusResponse [statusCode=" + this.statusCode + ", statusMessage=" + this.statusMessage
				+ ", statusType=" + this.statusType + ", totalCount=" + this.totalCount + "]";
	}

	public static enum Type {
		ERROR, SUCCESS, WARNING;
	}
}