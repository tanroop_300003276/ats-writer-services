package com.myntra.commons.codes;

public class StatusCodes {
	private int statusCode;
	private String message;

	public int getStatusCode() {
		return this.statusCode;
	}

	protected void setAll(int statusCode, String message) {
		this.statusCode = statusCode;
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}
}
