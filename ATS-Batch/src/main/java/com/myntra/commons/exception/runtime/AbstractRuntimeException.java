/*** Eclipse Class Decompiler plugin, copyright (c) 2016 Chen Chao (cnfree2000@hotmail.com) ***/
package com.myntra.commons.exception.runtime;

import com.myntra.commons.codes.StatusCodes;
import com.myntra.commons.codes.StatusResponse;

public abstract class AbstractRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 147359627523034553L;
	private int code;
	private StatusResponse.Type type;
	private StatusCodes statusCode;

	public AbstractRuntimeException(String message, int code) {
		super(message);

		this.type = StatusResponse.Type.ERROR;

		this.code = code;
	}

	public AbstractRuntimeException(StatusCodes err) {
		this(err.getMessage(), err.getStatusCode());
		this.statusCode = err;
	}

	public AbstractRuntimeException(String message, int code, Throwable cause) {
		this(message, code, cause, null);
		this.code = code;
	}

	public AbstractRuntimeException(String message, int code, Throwable cause, StatusResponse.Type type) {
		super(message, cause);

		this.type = StatusResponse.Type.ERROR;

		this.code = code;
		if (type != null)
			this.type = type;
	}

	public AbstractRuntimeException(StatusCodes err, Throwable cause) {
		this(err.getMessage(), err.getStatusCode(), cause);
	}

	public int getCode() {
		return this.code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public StatusResponse.Type getType() {
		return this.type;
	}

	public void setType(StatusResponse.Type type) {
		this.type = type;
	}

	public StatusResponse getStatusResponse() {
		return new StatusResponse(this.code, getMessage(), this.type);
	}

	public StatusCodes getStatusCode() {
		return this.statusCode;
	}
}