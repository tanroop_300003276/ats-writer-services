package com.myntra.shield.tasklets.queryandpublish;

import java.io.IOException;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Component;

import com.myntra.shield.tasks.BatchTasks;
import com.myntra.shield.tasks.QueryBITasks;

@Component
public interface QueryBIAndPublishOperations {
	
	public BatchTasks<T> getTaskImpl();
	
	public QueryBITasks getQueryBITaskImpl();
	
	default void queryandpublishOperations() throws IOException, InterruptedException{
		
		BatchTasks<T> batchTasks = getTaskImpl();
		batchTasks.setTimestamps();
		
		QueryBITasks queryBITasks = getQueryBITaskImpl();
		queryBITasks.startQuery();
		queryBITasks.downloadResultsFromDDP();
		queryBITasks.processResults();
		
		batchTasks.insertLastRunTimestamp();
		
	}

}
