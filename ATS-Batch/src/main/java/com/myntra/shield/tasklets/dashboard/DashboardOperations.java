package com.myntra.shield.tasklets.dashboard;

import java.io.IOException;

import org.apache.poi.ss.formula.functions.T;

import com.myntra.shield.tasks.BatchTasks;
import com.myntra.shield.tasks.S3Tasks;
import com.myntra.shield.tasks.WriteFileTasks;

public interface DashboardOperations {
	
	public BatchTasks<T> getTaskImpl();
	
	public WriteFileTasks getWriteFileTasks();
	
	public S3Tasks<T> getS3Tasks();
	
	@SuppressWarnings("rawtypes")
	public default void dashboardOperations() throws IOException, Exception{
		BatchTasks batchTasks = getTaskImpl();
		batchTasks.setTimestamps();
		
		WriteFileTasks fileTasks = getWriteFileTasks();
		fileTasks.getResults();
		fileTasks.writeToFile();
		
		S3Tasks<T> s3Tasks = getS3Tasks();
		s3Tasks.uploadToS3();
		
		batchTasks.insertLastRunTimestamp();
	}

}
