package com.myntra.shield.tasklets.queryandpublish;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.myntra.shield.tasks.BatchTasks;
import com.myntra.shield.tasks.QueryBITasks;
import com.myntra.shield.tasks.impl.TasksIncrementalRTO;

@Component
public class IncrementalRTOTasklet implements Tasklet, QueryBIAndPublishOperations{

	@Autowired
	TasksIncrementalRTO<T> tasksIncrementalRTO;
	
	@Override
	public BatchTasks<T> getTaskImpl() {
		return tasksIncrementalRTO;
	}

	@Override
	public QueryBITasks getQueryBITaskImpl() {
		return tasksIncrementalRTO;
	}
	
	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		queryandpublishOperations();
		return RepeatStatus.FINISHED;
	}

}
