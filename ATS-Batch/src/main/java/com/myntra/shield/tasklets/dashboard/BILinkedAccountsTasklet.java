package com.myntra.shield.tasklets.dashboard;

import org.apache.poi.ss.formula.functions.T;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.myntra.shield.tasks.BatchTasks;
import com.myntra.shield.tasks.S3Tasks;
import com.myntra.shield.tasks.WriteFileTasks;
import com.myntra.shield.tasks.impl.TasksLinkedAccounts;

@Component
public class BILinkedAccountsTasklet implements Tasklet, DashboardOperations{
	
	@Autowired
	TasksLinkedAccounts tasksLinkedAccounts;
	
	@Override
	public BatchTasks<T> getTaskImpl() {
		return this.tasksLinkedAccounts;
	}
	
	@Override
	public WriteFileTasks getWriteFileTasks() {
		return this.tasksLinkedAccounts;
	}
	
	@Override
	public S3Tasks<T> getS3Tasks() {
		return this.tasksLinkedAccounts;
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		dashboardOperations();
		return RepeatStatus.FINISHED;
	}

}
