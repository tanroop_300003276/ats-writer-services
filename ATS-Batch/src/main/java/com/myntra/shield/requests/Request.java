package com.myntra.shield.requests;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Request {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Request.class);

	public static String getRequest(String url, String... user) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");
		if(user != null)
			con.setRequestProperty("submitted-by", "tanroop.dhillon3@myntra.com");

		LOGGER.debug("Sending GET request to URL : " + url);
		int responseCode = con.getResponseCode();
		LOGGER.debug("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		LOGGER.debug("Response: " + response.toString());
		return response.toString();
	}
	
	
	public static String putRequest(String url, String urlParam) throws IOException {
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setDoOutput( true );
		con.setInstanceFollowRedirects( false );
		con.setRequestMethod("PUT");

		//add request header
		con.setRequestProperty("Content-Type", "application/json");
		
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParam);
		wr.flush();
		wr.close();
		
		int responseCode = con.getResponseCode();
		if(responseCode!= 200)
			return null;
		
		LOGGER.debug("Sending PUT request to URL : " + url+" with params : " + urlParam);
		LOGGER.debug("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		LOGGER.debug("Response: " + response.toString());
		return response.toString();

	}


	public static String postRequest(String url, String urlParam, String... user ) throws IOException {
		// TODO Auto-generated method stub
		LOGGER.debug("Sending POST request to URL : " + url+" with params : " + urlParam);
		System.out.println("Sending POST request to URL : " + url+" with params : " + urlParam);
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setDoOutput( true );
		con.setInstanceFollowRedirects( false );
		con.setRequestMethod("POST");

		//add request header
		con.setRequestProperty("Content-Type", "application/json");
		if(user != null){
			con.setRequestProperty("submitted-by", "tanroop.dhillon3@myntra.com");
			con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		}
		
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		if(urlParam != null)
			wr.writeBytes(urlParam);
		wr.flush();
		wr.close();
		
		int responseCode = con.getResponseCode();
		LOGGER.debug("Response Code : " + responseCode);
		if(responseCode!= 200)
			return null;
		//System.out.println("\nSending 'POST' request to URL : " + url +" with params : " + urlParam);
		//System.out.println("Response Code : " + responseCode);
		

		BufferedReader in = new BufferedReader(
		        new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());
		return response.toString();

	}


}
