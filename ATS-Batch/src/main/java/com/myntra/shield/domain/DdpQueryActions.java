package com.myntra.shield.domain;

public class DdpQueryActions {
	
	String downloadResult;

	public String getDownloadResult() {
		return downloadResult;
	}

	public void setDownloadResult(String downloadResult) {
		this.downloadResult = downloadResult;
	}

}
