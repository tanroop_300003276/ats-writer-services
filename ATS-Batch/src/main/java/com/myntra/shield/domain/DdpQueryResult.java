package com.myntra.shield.domain;

public class DdpQueryResult {

	String queryId;
	
	String queryStatus;
	
	String success;
	
	DdpQueryActions actions;
	
	public String getQueryId() {
		return queryId;
	}

	public void setQueryId(String queryId) {
		this.queryId = queryId;
	}
	
	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}
	
	public String getQueryStatus() {
		return queryStatus;
	}

	public void setQueryStatus(String queryStatus) {
		this.queryStatus = queryStatus;
	}
	
	public DdpQueryActions getActions() {
		return actions;
	}

	public void setActions(DdpQueryActions actions) {
		this.actions = actions;
	}

}
