package com.myntra.shield.domain;

public enum CODReason {

	COD101("DEFAULT"),
    COD102("HIGH_RTO"),
    COD103("INVALID_EMAIL"),
	COD104("HIGH_RETURNS"),
	COD105("LOW_RTO_PREMIUM_CUSTOMER"),
	COD106("TRANSACTIONS_UNDER_REVIEW"),
	COD107("NEW_CUSTOMER"),
	COD108("LINKED_BY_PHONE"),
	COD109("LINKED_BY_DEVICE");
    
    private final String reason;
    
    CODReason(String reason) {
        this.reason = reason;
    }
    public String getReason() {
        return reason;
    }
}