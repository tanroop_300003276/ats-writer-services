package com.myntra.shield.domain;

public class CODLimitResponse {
	
	private Double maxCODLimit;

	public Double getMaxCODLimit() {
		return maxCODLimit;
	}

	public void setMaxCODLimit(Double maxCODLimit) {
		this.maxCODLimit = maxCODLimit;
	}
	
	

}
