package com.myntra.shield.domain;

public class BooleanResponse {
	
	Boolean value;

	public Boolean getValue() {
		return value;
	}

	public void setValue(Boolean value) {
		this.value = value;
	}
	
	

}
