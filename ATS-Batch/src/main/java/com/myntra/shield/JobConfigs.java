package com.myntra.shield;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class JobConfigs {
	
	@Value("${cron.CODThrottling}")
	private String cron_cod_throttling;
    
    @Value("${cron.dashboard.CODThrottling}")
    private String cron_dashboard_cod_throttling;
    
    @Value("${cron.dashboard.CustomerActivity}")
    private String cron_dashboard_customer_activity;
    
    
    @Value("${cron.dashboard.CustomerCOD}")
    private String cron_dashboard_customer_cod;
    
    public String getCron_dashboard_customer_cod() {
		return cron_dashboard_customer_cod;
	}

	public void setCron_dashboard_customer_cod(String cron_dashboard_customer_cod) {
		this.cron_dashboard_customer_cod = cron_dashboard_customer_cod;
	}

	@Value("${cron.dashboard.LinkedAccounts}")
    private String cron_dashboard_linked_accounts;
    
    @Value("${cron.IncrementalRTO}")
	private String cron_incremental_rto;
    
    @Value("${cron.BulkRTO}")
	private String cron_bulk_rto;
    
    public String getCron_bulk_rto() {
		return cron_bulk_rto;
	}

	@Value("${cron.CustomerActivityValidation}")
	private String cron_customer_activity_validation;
    
    @Value("${cron.LinkedAccountsValidation}")
	private String cron_linked_accounts_validation;
    
    @Value("${s3.directory.dashboard.CODThrottling}")
    private String s3_dir_dashboard_cod_throttling;
    
    @Value("${s3.directory.dashboard.CustomerActivity}")
    private String s3_dir_dashboard_customer_activity;
    
    @Value("${s3.directory.dashboard.CustomerCOD}")
    private String s3_dir_dashboard_customer_cod;
    
    public String getS3_dir_dashboard_customer_cod() {
		return s3_dir_dashboard_customer_cod;
	}

	@Value("${s3.directory.dashboard.LinkedAccounts}")
    private String s3_dir_dashboard_linked_accounts;
    
    @Value("${job.name.CODThrottling}")
    private String job_name_cod_throttling;
    
    @Value("${job.name.dashboard.CODThrottling}")
    private String job_name_dashboard_cod_throttling;
    
    @Value("${job.name.dashboard.LinkedAccounts}")
    private String job_name_dashboard_linked_accounts;
    
    @Value("${job.name.dashboard.CustomerActivity}")
    private String job_name_dashboard_customer_activity;
    
    @Value("${job.name.dashboard.CustomerCOD}")
    private String job_name_dashboard_customer_cod;
    
    public String getJob_name_dashboard_customer_cod() {
		return job_name_dashboard_customer_cod;
	}

	@Value("${job.name.IncrementalRTO}")
    private String job_name_incremental_rto;
    
    @Value("${job.name.BulkRTO}")
    private String job_name_bulk_rto;
    
    public String getJob_name_bulk_rto() {
		return job_name_bulk_rto;
	}

	@Value("${job.name.CustomerActivityValidation}")
    private String job_name_customer_activity_validation;
    
    @Value("${job.name.LinkedAccountsValidation}")
    private String job_name_linked_accounts_validation;
    
    @Value("${job.enabled.THROTTLECOD}")
    private Boolean job_enabled_throttleCod;
    
    @Value("${job.enabled.DASHBOARDCODTHROTTLING}")
    private Boolean job_enabled_dashboard_cod_throttling;
    
    @Value("${job.enabled.DASHBOARDLINKEDACCOUNTS}")
    private Boolean job_enabled_dashboard_linked_accounts;
    
    @Value("${job.enabled.DASHBOARDCUSTOMERACTIVITY}")
    private Boolean job_enabled_dashboard_customer_activity;
    
    @Value("${job.enabled.DASHBOARDCUSTOMERCOD}")
    private Boolean job_enabled_dashboard_customer_cod;
    
    @Value("${job.enabled.INCREMENTALRTO}")
    private Boolean job_enabled_incremental_rto;
    
    @Value("${job.enabled.BULKRTO}")
    private Boolean job_enabled_bulk_rto;
    
    public Boolean getJob_enabled_bulk_rto() {
		return job_enabled_bulk_rto;
	}

	@Value("${job.enabled.CUSTOMERACTIVITYVALIDATION}")
    private Boolean job_enabled_customer_activity_validation;
    
    @Value("${job.enabled.LINKEDACCOUNTSVALIDATION}")
    private Boolean job_enabled_linked_accounts_validation;
    
    @Value("${IncrementalRTO.query}")
    private String queryIncrementalRto;
    
    @Value("${IncrementalRTO.queryName}")
    private String queryNameIncrementalRto;
    
    @Value("${IncrementalRTO.queryDescription}")
    private String queryDescIncrementalRto;
    
    @Value("${IncrementalRTO.queryResultsDIR}")
    private String queryResultsDIRIncrementalRTO;
    
    @Value("${IncrementalRTO.queryResultsFilename}")
    private String queryResultsFilenameIncrementalRTO;
    
    @Value("${BulkRTO.query}")
    private String queryBulkRto;
    
    @Value("${BulkRTO.queryName}")
    private String queryNameBulkRto;
    
    @Value("${BulkRTO.queryDescription}")
    private String queryDescBulkRto;
    
    @Value("${BulkRTO.queryResultsDIR}")
    private String queryResultsDIRBulkRTO;
    
    @Value("${BulkRTO.queryResultsFilename}")
    private String queryResultsFilenameBulkRTO;
    
    @Value("${dashboard.query.CustomerActivity}")
    private String queryCustomerActivity;
    
    @Value("${s3.directory.dashboard.CustomerActivity}")
    private String s3DirectoryCustomerActivity;
    
    @Value("${filename.dashboard.CustomerActivity}")
    private String s3FilenameCustomerActivity;
    
    @Value("${dashboard.query.CustomerCOD}")
    private String queryCustomerCOD;
    
    @Value("${s3.directory.dashboard.CustomerCOD}")
    private String s3DirectoryCustomerCOD;
    
    @Value("${filename.dashboard.CustomerCOD}")
    private String s3FilenameCustomerCOD;
    
    public String getQueryCustomerCOD() {
		return queryCustomerCOD;
	}

	public String getS3DirectoryCustomerCOD() {
		return s3DirectoryCustomerCOD;
	}

	public String getS3FilenameCustomerCOD() {
		return s3FilenameCustomerCOD;
	}

	public String getS3DirectoryCustomerActivity() {
		return s3DirectoryCustomerActivity;
	}

	public String getS3FilenameCustomerActivity() {
		return s3FilenameCustomerActivity;
	}

	public String getS3DirectoryLinkedAccounts() {
		return s3DirectoryLinkedAccounts;
	}

	public String getS3FilenameLinkedAccounts() {
		return s3FilenameLinkedAccounts;
	}

	@Value("${s3.directory.dashboard.LinkedAccounts}")
    private String s3DirectoryLinkedAccounts;
    
    @Value("${filename.dashboard.LinkedAccounts}")
    private String s3FilenameLinkedAccounts;
    
    @Value("${CustomerActivityValidation.query}")
    private String queryCustomerActivityValidation;
    
    public String getQueryCustomerActivityValidation() {
		return queryCustomerActivityValidation;
	}

	public String getQueryNameCustomerActivityValidation() {
		return queryNameCustomerActivityValidation;
	}

	public String getQueryDescCustomerActivityValidation() {
		return queryDescCustomerActivityValidation;
	}

	public String getQueryResultsDIRCustomerActivityValidation() {
		return queryResultsDIRCustomerActivityValidation;
	}

	public String getQueryResultsFilenameCustomerActivityValidation() {
		return queryResultsFilenameCustomerActivityValidation;
	}

	public String getQueryLinkedAccountsValidation() {
		return queryLinkedAccountsValidation;
	}

	public String getQueryNameLinkedAccountsValidation() {
		return queryNameLinkedAccountsValidation;
	}

	public String getQueryDescLinkedAccountsValidation() {
		return queryDescLinkedAccountsValidation;
	}

	public String getQueryResultsDIRLinkedAccountsValidation() {
		return queryResultsDIRLinkedAccountsValidation;
	}

	public String getQueryResultsFilenameLinkedAccountsValidation() {
		return queryResultsFilenameLinkedAccountsValidation;
	}

	@Value("${CustomerActivityValidation.queryName}")
    private String queryNameCustomerActivityValidation;
    
    @Value("${CustomerActivityValidation.queryDescription}")
    private String queryDescCustomerActivityValidation;
    
    @Value("${CustomerActivityValidation.queryResultsDIR}")
    private String queryResultsDIRCustomerActivityValidation;
    
    @Value("${CustomerActivityValidation.queryResultsFilename}")
    private String queryResultsFilenameCustomerActivityValidation;
    
    @Value("${LinkedAccountsValidation.query}")
    private String queryLinkedAccountsValidation;
    
    @Value("${LinkedAccountsValidation.queryName}")
    private String queryNameLinkedAccountsValidation;
    
    @Value("${LinkedAccountsValidation.queryDescription}")
    private String queryDescLinkedAccountsValidation;
    
    @Value("${LinkedAccountsValidation.queryResultsDIR}")
    private String queryResultsDIRLinkedAccountsValidation;
    
    @Value("${LinkedAccountsValidation.queryResultsFilename}")
    private String queryResultsFilenameLinkedAccountsValidation;
    
	public String getQueryCustomerActivity() {
		return queryCustomerActivity;
	}

	public String getQueryIncrementalRto() {
		return queryIncrementalRto;
	}

	public String getQueryNameIncrementalRto() {
		return queryNameIncrementalRto;
	}

	public String getQueryDescIncrementalRto() {
		return queryDescIncrementalRto;
	}

	public String getQueryResultsDIRIncrementalRTO() {
		return queryResultsDIRIncrementalRTO;
	}

	public String getQueryResultsFilenameIncrementalRTO() {
		return queryResultsFilenameIncrementalRTO;
	}

	public String getQueryBulkRto() {
		return queryBulkRto;
	}

	public String getQueryNameBulkRto() {
		return queryNameBulkRto;
	}

	public String getQueryDescBulkRto() {
		return queryDescBulkRto;
	}

	public String getQueryResultsDIRBulkRTO() {
		return queryResultsDIRBulkRTO;
	}

	public String getQueryResultsFilenameBulkRTO() {
		return queryResultsFilenameBulkRTO;
	}

	public String getCron_cod_throttling() {
		return cron_cod_throttling;
	}

	public String getCron_dashboard_cod_throttling() {
		return cron_dashboard_cod_throttling;
	}

	public String getCron_dashboard_customer_activity() {
		return cron_dashboard_customer_activity;
	}

	public String getCron_dashboard_linked_accounts() {
		return cron_dashboard_linked_accounts;
	}

	public String getCron_incremental_rto() {
		return cron_incremental_rto;
	}

	public String getCron_customer_activity_validation() {
		return cron_customer_activity_validation;
	}

	public String getCron_linked_accounts_validation() {
		return cron_linked_accounts_validation;
	}

	public String getS3_dir_dashboard_cod_throttling() {
		return s3_dir_dashboard_cod_throttling;
	}

	public String getS3_dir_dashboard_customer_activity() {
		return s3_dir_dashboard_customer_activity;
	}

	public String getS3_dir_dashboard_linked_accounts() {
		return s3_dir_dashboard_linked_accounts;
	}

	public String getJob_name_cod_throttling() {
		return job_name_cod_throttling;
	}

	public String getJob_name_dashboard_cod_throttling() {
		return job_name_dashboard_cod_throttling;
	}

	public String getJob_name_dashboard_linked_accounts() {
		return job_name_dashboard_linked_accounts;
	}

	public String getJob_name_dashboard_customer_activity() {
		return job_name_dashboard_customer_activity;
	}

	public String getJob_name_incremental_rto() {
		return job_name_incremental_rto;
	}

	public String getJob_name_customer_activity_validation() {
		return job_name_customer_activity_validation;
	}

	public String getJob_name_linked_accounts_validation() {
		return job_name_linked_accounts_validation;
	}

	public Boolean getJob_enabled_throttleCod() {
		return job_enabled_throttleCod;
	}

	public Boolean getJob_enabled_dashboard_cod_throttling() {
		return job_enabled_dashboard_cod_throttling;
	}

	public Boolean getJob_enabled_dashboard_linked_accounts() {
		return job_enabled_dashboard_linked_accounts;
	}

	public Boolean getJob_enabled_dashboard_customer_activity() {
		return job_enabled_dashboard_customer_activity;
	}

	public Boolean getJob_enabled_dashboard_customer_cod() {
		return job_enabled_dashboard_customer_cod;
	}

	public Boolean getJob_enabled_incremental_rto() {
		return job_enabled_incremental_rto;
	}

	public Boolean getJob_enabled_customer_activity_validation() {
		return job_enabled_customer_activity_validation;
	}

	public Boolean getJob_enabled_linked_accounts_validation() {
		return job_enabled_linked_accounts_validation;
	}

}
