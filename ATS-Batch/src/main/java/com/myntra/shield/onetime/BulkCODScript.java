package com.myntra.shield.onetime;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.myntra.idea.client.IdeaClient;
import com.myntra.shield.cod.MarkAndPublishCOD;
import com.myntra.shield.enums.MessageType;
import com.myntra.shield.queue.QueueClient;
import com.myntra.shield.repositories.BatchTimestampsRepository;

public class BulkCODScript {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BulkCODScript.class);
	
	@Value("${filename.BulkCODScript}")
	String filename;
	
	@Autowired
	IdeaClient ideaClient;
	
	@Autowired
	QueueClient queueClient;
	
	@Autowired
	BatchTimestampsRepository batchTimestampsRepository;
	
	@Autowired
	MarkAndPublishCOD markAndPublishCOD;
	
	@PostConstruct
	public void init(){
		
		try {
			InputStream in = new GZIPInputStream(new FileInputStream(filename));
			InputStreamReader reader = new InputStreamReader(in);
			BufferedReader br = new BufferedReader(reader);
			String sCurrentLine;
			ExecutorService executorService = Executors.newFixedThreadPool(20);
			while ((sCurrentLine = br.readLine()) != null) {
				String[] userDetails = sCurrentLine.replaceAll("\"", "").replaceAll("%", "").split(",");
				
//				try{
//					markCODDetails(userDetails);
//				}catch(Exception e){
//						LOGGER.error("Exception while forming cod data from {}", userDetails, e);
//				}
				
				executorService.execute(new Runnable() {
				    public void run() {
				    	try{
							markCODDetails(userDetails);
						}catch(Exception e){
								LOGGER.error("Exception while forming cod data from {}", userDetails, e);
						}
				    }
				});
				
			}
			
			executorService.shutdown();
			while(true){
				LOGGER.debug("Polling for all threads in executor service to finish...");
				boolean finshed = executorService.awaitTermination(1, TimeUnit.MINUTES);
				LOGGER.debug("ExecutorService status: " + finshed);
				if(finshed)
					break;
			}
			
			br.close();
			
		}catch(Exception e){
			LOGGER.error("Exception encountered while setting return details. {}", e);
		}
		
	}
		
	private void markCODDetails(String[] userDetails) {
			String uidx = userDetails[1];
			String cod = userDetails[6];
//			if(cod.equals("9")){
//				LOGGER.debug("Not updating cod of user with 1 transaction. uidx: {}", uidx);
//				return;
//			}
			String rto_vol_perc = userDetails[4];
			String rto_rev_perc = userDetails[5];
			String transactions = userDetails[2];
//			if(!markAndPublishCOD.validateCODDetailsFromLabel(uidx,  cod, rto_vol_perc, rto_rev_perc, transactions))
//				markAndPublishCOD.markandpublishCOD(uidx, cod, rto_vol_perc, rto_rev_perc, transactions, MessageType.COD_THROTTLING_BULK);
			
//			if(markAndPublishCOD.validateCODDetailsFromLabel(uidx,  cod, rto_vol_perc, rto_rev_perc, transactions))
			markAndPublishCOD.markandpublishCOD(uidx, cod, rto_vol_perc, rto_rev_perc, transactions, MessageType.COD_THROTTLING_BULK);
		}
	
}
