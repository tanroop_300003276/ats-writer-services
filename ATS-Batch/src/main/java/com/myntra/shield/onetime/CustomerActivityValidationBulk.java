package com.myntra.shield.onetime;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.myntra.shield.cod.MarkAndPublishCOD;

public class CustomerActivityValidationBulk {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerActivityValidationBulk.class);
	
	@Value("${filename.CustomerActivityValidationBulk}")
	String filename;
	
	@Autowired
	MarkAndPublishCOD markAndPublishCOD;
	
	@PostConstruct
	public void init() throws IOException, InterruptedException{
		
		processResults();
		
	}
	
	public void processResults() throws IOException, InterruptedException {
		InputStream in = new GZIPInputStream(new FileInputStream(filename));
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(reader);
		String line = br.readLine(); 
		ExecutorService executorService = Executors.newFixedThreadPool(20);
		while (line != null) { 
			final String[] userdetails = line.replace("\"", "").split(",");
//			try{
//				validateDetails(userdetails);
//			}catch(Exception e){
//				LOGGER.error("Exception while forming cod data from " + line, e);
//			}
			executorService.execute(new Runnable() {
			    public void run() {
			    	try{
						validateDetails(userdetails);
					}catch(Exception e){
							LOGGER.error("Exception while forming cod data from {}", userdetails, e);
					}
			    }
			});
			
			 line = br.readLine(); 
		} 
		executorService.shutdown();
		while(true){
			LOGGER.debug("Polling for all threads in executor service to finish...");
			boolean finshed = executorService.awaitTermination(1, TimeUnit.MINUTES);
			LOGGER.debug("ExecutorService status: " + finshed);
			if(finshed)
				break;
		}
		 
		 br.close();
		 return;
	}

	protected void validateDetails(String[] userdetails) {
		if(userdetails == null)
			return;
		
		String uidx = userdetails[0];
		String maxCod = userdetails[5];
		String rto_perc_vol = userdetails[8];
		String rto_perc_rev = userdetails[9];
		LOGGER.debug("BI data. uidx: {}, maxCod: {}, rto_perc_vol: {}, rto_perc_rev: {}", uidx, maxCod, rto_perc_vol, rto_perc_rev);
		if(maxCod!= null)
			markAndPublishCOD.validateCODDetails(uidx, maxCod, rto_perc_vol, rto_perc_rev);
		
		String isFakeEmail = userdetails[12];
		if(isFakeEmail != null){
			Boolean fakeEmail = Boolean.valueOf(isFakeEmail);
			markAndPublishCOD.validateFakeEmail(uidx, fakeEmail);
		}
	}

}
