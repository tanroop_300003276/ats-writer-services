package com.myntra.shield.onetime;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.myntra.ats.blocked.client.ATSClient;
import com.myntra.ats.blocked.client.response.UserDetailsResponse;
import com.myntra.ats.user.client.request.UserDetailsRequest;
import com.myntra.idea.client.IdeaClient;
import com.myntra.idea.response.ProfileResponse;
import com.myntra.shield.entities.BatchTimestamps;
import com.myntra.shield.enums.MessageType;
import com.myntra.shield.exception.ShieldException;
import com.myntra.shield.messages.Message;
import com.myntra.shield.messages.MessageDetails;
import com.myntra.shield.queue.QueueClient;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.utils.Utils;

public class ReturnDetails {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReturnDetails.class);
	
	@Value("${filename.ReturnDetails}")
	String filename;
	
	@Value("${filenameOldRA}")
	String filenameOldRA;
	
	@Autowired
	IdeaClient ideaClient;
	
	@Autowired
	QueueClient queueClient;
	
	@Value("${publishCODToQueueEnabled}")
	Boolean publishCODToQueueEnabled;
	
	@Value("${updateShieldEnabled}")
	Boolean updateShieldEnabled;
	
	@Autowired
	BatchTimestampsRepository batchTimestampsRepository;
	
	@Autowired
	ATSClient atsClient;
	
	@PostConstruct
	public void init(){
		
		
		
		try {
			FileReader fr = new FileReader(filenameOldRA);
			BufferedReader br = new BufferedReader(fr);
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				if(sCurrentLine.contains("idcustomer"))
					continue;
				String[] userDetails = sCurrentLine.replaceAll("\"", "").replaceAll("%", "").split(",");
				
				String email = userDetails[1];
				String ret_perc = userDetails[2];
				String uidx = null;
				try{
					ProfileResponse profileResponse = ideaClient.getByEmail("myntra", email);
					if(profileResponse != null && profileResponse.getEntry()!= null)
						uidx = profileResponse.getEntry().getUidx();
				}catch(Exception e){
					LOGGER.error("exception while fetching user profile from idea by email: {}", email);
				}
				if(uidx == null){
					LOGGER.debug("Uidx fetched is null for email: {}", email);
					continue;
				}
				
				UserDetailsRequest request = new UserDetailsRequest();
				request.setLogin(uidx);
				request.setIsReturnAbuser(false);
				request.setReasonReturnAbuser("DEFAULT");
				if(updateShieldEnabled)
					atsClient.setReturnAbuser(request);
				
				
				if(ret_perc != null)
					publishToQueue(uidx, ret_perc, false);
				else 
					publishToQueue(uidx, "0", false);
				
				
			}
			
			fr = new FileReader(filename);
			br = new BufferedReader(fr);
			while ((sCurrentLine = br.readLine()) != null) {
				if(sCurrentLine.contains("idcustomer"))
					continue;
				String[] userDetails = sCurrentLine.replaceAll("\"", "").replaceAll("%", "").split(",");
				
				String email = userDetails[1];
				String ret_perc = userDetails[2];
				String uidx = null;
				try{
					ProfileResponse profileResponse = ideaClient.getByEmail("myntra", email);
					if(profileResponse != null && profileResponse.getEntry()!= null)
						uidx = profileResponse.getEntry().getUidx();
				}catch(Exception e){
					LOGGER.error("exception while fetching user profile from idea by email: {}", email);
				}
				if(uidx == null){
					LOGGER.debug("Uidx fetched is null for email: {}", email);
					continue;
				}
				UserDetailsRequest request = new UserDetailsRequest();
				request.setLogin(uidx);
				request.setIsReturnAbuser(true);
				request.setReasonReturnAbuser("HIGH_RETURNS");
				
				if(updateShieldEnabled)
					atsClient.setReturnAbuser(request);
				
				
				if(ret_perc != null)
					publishToQueue(uidx, ret_perc, true);
				else 
					publishToQueue(uidx, "0", true);
				
				
			}
			
			BatchTimestamps batchTimestamps = new BatchTimestamps();
			batchTimestamps.setJobName("RETURN_DETAILS");
			batchTimestamps.setTimestamp(Utils.getTimestamp());
//			
			batchTimestampsRepository.save(batchTimestamps);
			
		}catch(Exception e){
			LOGGER.error("Exception encountered while setting return details. {}", e);
		}
		
	}
	
	private void publishToQueue(String uidx, String ret_perc, Boolean is_ra) {
		Message msg = new Message();
		msg.setMessageType(MessageType.RETURN_ABUSER);
		MessageDetails msgDetails = new MessageDetails();
		msgDetails.setUidx(uidx);
		msgDetails.setNetReturnPercentage(Double.valueOf(ret_perc));
		msgDetails.setIsReturnAbuser(is_ra);
		
		msg.setMessageDetails(msgDetails);
		
		LOGGER.debug("msgDetails: {}", msgDetails);
		
		
		if(publishCODToQueueEnabled){
			try {
				queueClient.sendReturnAbuserMessage(msg);;
			} catch (ShieldException e) {
				LOGGER.error("Exception while publishing to queue. Message: " + msg.getMessageDetails().toString() +". Exception: " + e.getMessage());
			}
		}
	}

}
