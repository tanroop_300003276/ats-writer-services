package com.myntra.shield.onetime;

import java.io.BufferedReader;
import java.io.FileReader;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.interceptor.security.AbstractUsernameTokenInInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.myntra.idea.client.IdeaClient;
import com.myntra.idea.response.ProfileResponse;
import com.myntra.shield.entities.BatchTimestamps;
import com.myntra.shield.enums.AbuserType;
import com.myntra.shield.enums.MessageType;
import com.myntra.shield.exception.ShieldException;
import com.myntra.shield.messages.Message;
import com.myntra.shield.messages.MessageDetails;
import com.myntra.shield.queue.QueueClient;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.utils.Utils;

public class LinkedAccoutsDetails {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LinkedAccoutsDetails.class);
	
	@Value("${filename.LinkedAccountsDetails}")
	String filename;
	
	@Autowired
	QueueClient queueClient;
	
	@Value("${publishCODToQueueEnabled}")
	Boolean publishCODToQueueEnabled;
	
	@Autowired
	BatchTimestampsRepository batchTimestampsRepository;
	
	@PostConstruct
	public void init(){
		
		try {
			FileReader fr = new FileReader(filename);
			BufferedReader br = new BufferedReader(fr);
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				try{
					String[] userDetails = sCurrentLine.replaceAll("\"", "").replaceAll("%", "").split(",");
					String uidx = userDetails[0];
					String uidxP = userDetails[1];
					String linkedBy = userDetails[2];
					String abuserType = userDetails[3];
					
					publishToQueue(uidx, uidxP, linkedBy, abuserType);
				}catch(Exception e){
					LOGGER.error("Error in linked accounts. Current record: {}. Exception: {}",sCurrentLine, e);
				}
			}
			
			BatchTimestamps batchTimestamps = new BatchTimestamps();
			batchTimestamps.setJobName("LINKED_ACCOUNTS_BULK");
			batchTimestamps.setTimestamp(Utils.getTimestamp());
			
			batchTimestampsRepository.save(batchTimestamps);
			
		}catch(Exception e){
			LOGGER.error("Exception encountered while setting return details. {}", e);
		}
		
	}
	
	private void publishToQueue(String uidx, String uidxP, String linkedBy, String abuserType) {
		Message msg = new Message();
		msg.setMessageType(MessageType.LINKED_ACCOUNTS);
		
		MessageDetails msgDetails = new MessageDetails();
		msgDetails.setUidx(uidx);
		msgDetails.setLinkedTo(uidxP);
		msgDetails.setLinkedBy(linkedBy);
		AbuserType abType = null;
		if(StringUtils.equals(abuserType, AbuserType.BLACKLISTED.toString()))
			abType = AbuserType.BLACKLISTED;
		else if(StringUtils.equals(abuserType, AbuserType.RETURN_ABUSER.toString()))
			abType = AbuserType.RETURN_ABUSER;
		else if(StringUtils.equals(abuserType, AbuserType.RTO_ABUSER.toString()))
			abType = AbuserType.RTO_ABUSER;
		
		msgDetails.setAbuserType(abType);
		
		msg.setMessageDetails(msgDetails);
		
		LOGGER.debug("msgDetails: {}", msgDetails);
		
		if(publishCODToQueueEnabled){
			try {
				queueClient.sendLinkedAccountsMessage(msg);
			} catch (ShieldException e) {
				LOGGER.error("Exception while publishing to queue. Message: " + msg.getMessageDetails().toString() +". Exception: " + e.getMessage());
			}
		}
		
	}

}
