package com.myntra.shield.utils;

import java.util.HashMap;
import java.util.Map;
public class SlackConstants {
    public static final String DEFAULT_SLACK_CHANNEL = "#testing_ats";
    public static final String CRON_SLACK_CHANNEL = "#search_cron";
    private static final String DEFAULT_WEBHOOK_URL = "https://hooks.slack.com/services/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";
    private static final String CRON_WEBHOOK_URL = "https://hooks.slack.com/services/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";
    private static Map<String, String> channelWebHookMap = new HashMap<String, String>();
    static {
        channelWebHookMap.put(CRON_SLACK_CHANNEL, CRON_WEBHOOK_URL);
    }
    public static String getWebHookForChannel(String channel) {
//      if (channelWebHookMap.containsKey(channel)) {
//          return channelWebHookMap.get(channel);
//      }
        return DEFAULT_WEBHOOK_URL;
    }
}