package com.myntra.shield.utils;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

public class Utils {
	
	
	public static String getTimestamp() {
		return new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date());
	}

	public static String getTimestamp(Date time) {
		return new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(time);
	}

	public static String getTimestamp(LocalDate yesterday) {
		return new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(yesterday);
	}

	public static String getFilename(String filename, String currentTimeStamp) {
		StringBuilder fName = new StringBuilder(filename);
		fName.append(currentTimeStamp.replace(" ", "_"));
		fName.append(".csv");
		return fName.toString();
	}

}
