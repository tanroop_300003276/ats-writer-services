package com.myntra.shield;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.quartz.JobDataMap;
import org.quartz.Trigger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.JobRegistry;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.support.JobRegistryBeanPostProcessor;
import org.springframework.batch.core.configuration.support.MapJobRegistry;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.jdbc.datasource.init.ScriptStatementFailedException;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.myntra.shield.onetime.BulkCODScript;
import com.myntra.shield.onetime.CustomerActivityValidationBulk;
import com.myntra.shield.onetime.LinkedAccoutsDetails;
import com.myntra.shield.onetime.ReturnDetails;
import com.myntra.shield.schedulers.*;
import com.myntra.shield.tasklets.dashboard.*;
import com.myntra.shield.tasklets.queryandpublish.BulkRTOTasklet;
import com.myntra.shield.tasklets.queryandpublish.CustomerActivityValidationTasklet;
import com.myntra.shield.tasklets.queryandpublish.DailyRTONewTasklet;
import com.myntra.shield.tasklets.queryandpublish.IncrementalRTOTasklet;
import com.myntra.shield.tasklets.queryandpublish.LinkedAccountsValidationTasklet;

@Configuration
public class JobConfigurationDetails {
	
	@Autowired
	JobConfigs jobConfigs;
	
	@Value("${datasource.dbUrl}")
    String dbUrl;
    @Value("${datasource.dbDriver}")
    String dbDriver;
    @Value("${datasource.dbUsername}")
    String dbUsername;
    @Value("${datasource.dbPassword}")
    String dbPassword;
    
    
    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;
    
    
    @Autowired
    BICODThrottlingTasklet bICODThrottlingTasklet;
    
    @Autowired
    BICustomerActivityTasklet bICustomerActivityTasklet;
    
    @Autowired
    BICustomerCODTasklet bICustomerCODTasklet;
    
    
    @Autowired
    BILinkedAccountsTasklet bILinkedAccountsTasklet;
    
    @Autowired
    IncrementalRTOTasklet incrementalRTOTasklet;
    
    @Autowired
    BulkRTOTasklet bulkRTOTasklet;
    
    @Autowired
    DailyRTONewTasklet dailyRTONewTasklet;
    
    @Autowired
	CustomerActivityValidationTasklet customerActivityValidationTasklet;
	
	@Autowired
	LinkedAccountsValidationTasklet linkedAccountsValidationTasklet;
    
    @Bean
    MapJobRegistry jobRegistry(){
    	return new MapJobRegistry();
    }
    
    @Bean
    SimpleJobLauncher jobLauncher() throws Exception{
    	SimpleJobLauncher sjl =  new SimpleJobLauncher();
    	sjl.setJobRepository(jobRepository().getObject());
    	return sjl;
    }
    
    @Bean
    @ConfigurationProperties("spring.datasource")
    public DataSource dataSource(){
    	return DataSourceBuilder.create().build();
    }
    
    
    @Bean
    JobBuilderFactory jobBuilderFactory() throws Exception{
    	JobBuilderFactory jbf = new JobBuilderFactory(jobRepository().getObject());
    			return jbf;
    }
    
    @Bean
    StepBuilderFactory stepBuilderFactory() throws Exception{
    	StepBuilderFactory builderFactory = new StepBuilderFactory(jobRepository().getObject(), transactionManager());
    	return builderFactory;
    }
    
    @Bean 
    public JobRepositoryFactoryBean jobRepository(){
    	JobRepositoryFactoryBean jobRepositoryFactoryBean = new JobRepositoryFactoryBean();
    	jobRepositoryFactoryBean.setTransactionManager(transactionManager());
    	jobRepositoryFactoryBean.setDataSource(dataSource());
    	jobRepositoryFactoryBean.setDatabaseType(Database.MYSQL.toString());
    	
    	return jobRepositoryFactoryBean;
    }
    
    
    @Bean
    public JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor(JobRegistry jobRegistry) {
        JobRegistryBeanPostProcessor jobRegistryBeanPostProcessor = new JobRegistryBeanPostProcessor();
        jobRegistryBeanPostProcessor.setJobRegistry(jobRegistry);
        return jobRegistryBeanPostProcessor;
    }
    
    
	@Bean
	@ConditionalOnProperty(name="job.enabled.THROTTLECOD")
	public JobDetailFactoryBean jobDetailFactoryBeanCODThrottling() throws Exception{
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(ThrottleCOD.class);
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("jobName", jobConfigs.getJob_name_cod_throttling());
		jobDataMap.put("jobLauncher", jobLauncher());
		jobDataMap.put("jobLocator", jobRegistry());
		factory.setJobDataAsMap(jobDataMap);
		factory.setName(jobConfigs.getJob_name_cod_throttling());
		return factory;
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.INCREMENTALRTO")
	public JobDetailFactoryBean jobDetailFactoryBeanIncrementalRTO() throws Exception{
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(IncrementalRTO.class);
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("jobName", jobConfigs.getJob_name_incremental_rto());
		jobDataMap.put("jobLauncher", jobLauncher());
		jobDataMap.put("jobLocator", jobRegistry());
		factory.setJobDataAsMap(jobDataMap);
		factory.setName(jobConfigs.getJob_name_incremental_rto());
		return factory;
	}
	
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.BULKRTO")
	public JobDetailFactoryBean jobDetailFactoryBeanBulkRTO() throws Exception{
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(BulkRTO.class);
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("jobName", jobConfigs.getJob_name_bulk_rto());
		jobDataMap.put("jobLauncher", jobLauncher());
		jobDataMap.put("jobLocator", jobRegistry());
		factory.setJobDataAsMap(jobDataMap);
		factory.setName(jobConfigs.getJob_name_bulk_rto());
		return factory;
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.CUSTOMERACTIVITYVALIDATION")
	public JobDetailFactoryBean jobDetailFactoryBeanCustomerActivityValidation() throws Exception{
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(CustomerActivityValidation.class);
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("jobName", jobConfigs.getJob_name_customer_activity_validation());
		jobDataMap.put("jobLauncher", jobLauncher());
		jobDataMap.put("jobLocator", jobRegistry());
		factory.setJobDataAsMap(jobDataMap);
		factory.setName(jobConfigs.getJob_name_customer_activity_validation());
		return factory;
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.LINKEDACCOUNTSVALIDATION")
	public JobDetailFactoryBean jobDetailFactoryBeanLinkedAccountsValidation() throws Exception{
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(LinkedAccountsValidation.class);
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("jobName", jobConfigs.getJob_name_linked_accounts_validation());
		jobDataMap.put("jobLauncher", jobLauncher());
		jobDataMap.put("jobLocator", jobRegistry());
		factory.setJobDataAsMap(jobDataMap);
		factory.setName(jobConfigs.getJob_name_linked_accounts_validation());
		return factory;
	}
	
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDCUSTOMERACTIVITY")
	public JobDetailFactoryBean jobDetailFactoryBeanDashboardCustomerActivity() throws Exception{
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(CustomerActivityDashboard.class);
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("jobName", jobConfigs.getJob_name_dashboard_customer_activity());
		jobDataMap.put("jobLauncher", jobLauncher());
		jobDataMap.put("jobLocator", jobRegistry());
		factory.setJobDataAsMap(jobDataMap);
		factory.setName(jobConfigs.getJob_name_dashboard_customer_activity());
		return factory;
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDCUSTOMERCOD")
	public JobDetailFactoryBean jobDetailFactoryBeanDashboardCustomerCOD() throws Exception{
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(CustomerCOD.class);
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("jobName", jobConfigs.getJob_name_dashboard_customer_cod());
		jobDataMap.put("jobLauncher", jobLauncher());
		jobDataMap.put("jobLocator", jobRegistry());
		factory.setJobDataAsMap(jobDataMap);
		factory.setName(jobConfigs.getJob_name_dashboard_customer_cod());
		return factory;
	}
	
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDLINKEDACCOUNTS")
	public JobDetailFactoryBean jobDetailFactoryBeanDashboardLinkedAccounts() throws Exception{
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(LinkedAccountsDashboard.class);
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("jobName", jobConfigs.getJob_name_dashboard_linked_accounts());
		jobDataMap.put("jobLauncher", jobLauncher());
		jobDataMap.put("jobLocator", jobRegistry());
		factory.setJobDataAsMap(jobDataMap);
		factory.setName(jobConfigs.getJob_name_dashboard_linked_accounts());
		return factory;
	}
	
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDCODTHROTTLING")
	public JobDetailFactoryBean jobDetailFactoryBeanDashboardCODThrottling() throws Exception{
		JobDetailFactoryBean factory = new JobDetailFactoryBean();
		factory.setJobClass(CODThrottlingDashboard.class);
		JobDataMap jobDataMap = new JobDataMap();
		jobDataMap.put("jobName", jobConfigs.getJob_name_dashboard_cod_throttling());
		jobDataMap.put("jobLauncher", jobLauncher());
		jobDataMap.put("jobLocator", jobRegistry());
		factory.setJobDataAsMap(jobDataMap);
		factory.setName(jobConfigs.getJob_name_dashboard_cod_throttling());
		return factory;
	}
	
	@ConditionalOnProperty(name="job.enabled.THROTTLECOD")
	@Bean
	public CronTriggerFactoryBean cronTriggerFactoryBeanCODThrottling() throws Exception{
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBeanCODThrottling().getObject());
		stFactory.setName(jobConfigs.getJob_name_cod_throttling()+"TRIGGER");
		stFactory.setCronExpression(jobConfigs.getCron_cod_throttling());
		stFactory.setStartDelay(0);
		return stFactory;
	}
	
	@ConditionalOnProperty(name="job.enabled.INCREMENTALRTO")
	@Bean
	public CronTriggerFactoryBean cronTriggerFactoryBeanIncrementalRTO() throws Exception{
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBeanIncrementalRTO().getObject());
		stFactory.setName(jobConfigs.getJob_name_incremental_rto()+"TRIGGER");
		stFactory.setCronExpression(jobConfigs.getCron_incremental_rto());
		stFactory.setStartDelay(0);
		return stFactory;
	}
	
	@ConditionalOnProperty(name="job.enabled.BULKRTO")
	@Bean
	public CronTriggerFactoryBean cronTriggerFactoryBeanBulkRTO() throws Exception{
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBeanBulkRTO().getObject());
		stFactory.setName(jobConfigs.getJob_name_bulk_rto()+"TRIGGER");
		stFactory.setCronExpression(jobConfigs.getCron_bulk_rto());
		stFactory.setStartDelay(0);
		return stFactory;
	}
	
	@ConditionalOnProperty(name="job.enabled.CUSTOMERACTIVITYVALIDATION")
	@Bean
	public CronTriggerFactoryBean cronTriggerFactoryBeanCustomerActivityValidation() throws Exception{
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBeanCustomerActivityValidation().getObject());
		stFactory.setName(jobConfigs.getJob_name_customer_activity_validation()+"TRIGGER");
		stFactory.setCronExpression(jobConfigs.getCron_customer_activity_validation());
		stFactory.setStartDelay(0);
		return stFactory;
	}
	
	@ConditionalOnProperty(name="job.enabled.LINKEDACCOUNTSVALIDATION")
	@Bean
	public CronTriggerFactoryBean cronTriggerFactoryBeanLinkedAccountsValidation() throws Exception{
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBeanLinkedAccountsValidation().getObject());
		stFactory.setName(jobConfigs.getJob_name_linked_accounts_validation()+"TRIGGER");
		stFactory.setCronExpression(jobConfigs.getCron_linked_accounts_validation());
		stFactory.setStartDelay(0);
		return stFactory;
	}
	
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDCUSTOMERACTIVITY")
	public CronTriggerFactoryBean cronTriggerFactoryBeanDashboardCustomerActivity() throws Exception{
		
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBeanDashboardCustomerActivity().getObject());
		stFactory.setName(jobConfigs.getJob_name_dashboard_customer_activity()+"TRIGGER");
		stFactory.setCronExpression(jobConfigs.getCron_dashboard_customer_activity());
		return stFactory;
		
	}
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDCUSTOMERCOD")
	public CronTriggerFactoryBean cronTriggerFactoryBeanDashboardCustomerCOD() throws Exception{
		
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBeanDashboardCustomerCOD().getObject());
		stFactory.setName(jobConfigs.getJob_name_dashboard_customer_cod()+"TRIGGER");
		stFactory.setCronExpression(jobConfigs.getCron_dashboard_customer_cod());
		return stFactory;
		
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDLINKEDACCOUNTS")
	public CronTriggerFactoryBean cronTriggerFactoryBeanDashboardLinkedAccounts() throws Exception{
		
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBeanDashboardLinkedAccounts().getObject());
		stFactory.setName(jobConfigs.getJob_name_dashboard_linked_accounts()+"TRIGGER");
		stFactory.setCronExpression(jobConfigs.getCron_dashboard_linked_accounts());
		return stFactory;
		
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDCODTHROTTLING")
	public CronTriggerFactoryBean cronTriggerFactoryBeanDashboardCODThrottling() throws Exception{
		
		CronTriggerFactoryBean stFactory = new CronTriggerFactoryBean();
		stFactory.setJobDetail(jobDetailFactoryBeanDashboardCODThrottling().getObject());
		stFactory.setName(jobConfigs.getJob_name_dashboard_cod_throttling()+"TRIGGER");
		stFactory.setCronExpression(jobConfigs.getCron_dashboard_cod_throttling());
		return stFactory;
		
	}
	
	
	@Bean
	public SchedulerFactoryBean schedulerFactoryBean() throws Exception {
		SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
		
		List<Trigger> triggers = new ArrayList<>();
		
//		if(jobConfigs.getJob_enabled_throttleCod())
//			triggers.add(cronTriggerFactoryBeanCODThrottling().getObject());
//		if(jobConfigs.getJob_enabled_dashboard_cod_throttling())
//			triggers.add(cronTriggerFactoryBeanDashboardCODThrottling().getObject());
		if(jobConfigs.getJob_enabled_dashboard_customer_activity())
			triggers.add(cronTriggerFactoryBeanDashboardCustomerActivity().getObject());
		if(jobConfigs.getJob_enabled_dashboard_linked_accounts())
			triggers.add(cronTriggerFactoryBeanDashboardLinkedAccounts().getObject());
		if(jobConfigs.getJob_enabled_incremental_rto())
			triggers.add(cronTriggerFactoryBeanIncrementalRTO().getObject());
		if(jobConfigs.getJob_enabled_bulk_rto())
			triggers.add(cronTriggerFactoryBeanBulkRTO().getObject());
		if(jobConfigs.getJob_enabled_customer_activity_validation())
			triggers.add(cronTriggerFactoryBeanCustomerActivityValidation().getObject());
		if(jobConfigs.getJob_enabled_linked_accounts_validation())
			triggers.add(cronTriggerFactoryBeanLinkedAccountsValidation().getObject());
		if(jobConfigs.getJob_enabled_dashboard_customer_cod())
			triggers.add(cronTriggerFactoryBeanDashboardCustomerCOD().getObject());
		
		scheduler.setTriggers(triggers.toArray(new Trigger[triggers.size()]));
		return scheduler;
	}
	
//	@ConditionalOnProperty({"job.enabled.THROTTLECOD","do_not_skip_ddp_query"})
//	@Bean
//	public Job ThrottleCOD(){
//		return jobBuilderFactory.get(jobConfigs.getJob_name_cod_throttling())
//		.incrementer(new RunIdIncrementer())
//		.start(queryRTODetails())
//		.next(markCOD())
//		.next(updateCODThrottleTimestamp())
//		.build();
//		
//	}
//	
//	
//	@ConditionalOnProperty({"job.enabled.THROTTLECOD","skip_ddp_query"})
//	@Bean
//	public Job ThrottleCODDev(){
//		return jobBuilderFactory.get(jobConfigs.getJob_name_cod_throttling())
//		.incrementer(new RunIdIncrementer())
//		.start(markCOD())
//		.next(updateCODThrottleTimestamp())
//		.build();
//		
//	}
	
	@ConditionalOnProperty({"job.enabled.THROTTLECOD"})
	@Bean
	public Job DailyRTONew(){
		return jobBuilderFactory.get(jobConfigs.getJob_name_cod_throttling())
		.incrementer(new RunIdIncrementer())
		.start(dailyRTONew())
		.build();
		
	}
	
	@Bean Step dailyRTONew(){
		
		return stepBuilderFactory.get("dailyRTONew")
				.tasklet(dailyRTONewTasklet)
				.allowStartIfComplete(true)
				.build();
		
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.INCREMENTALRTO")
	public Job IncrementalRTO(){
		return jobBuilderFactory.get(jobConfigs.getJob_name_incremental_rto())
		.incrementer(new RunIdIncrementer())
		.start(incrementalRTO())
		.build();
		
	}
	
	@Bean Step incrementalRTO(){
		return stepBuilderFactory.get("incrementalRTO")
				.tasklet(incrementalRTOTasklet)
				.allowStartIfComplete(true)
				.build();
		
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.BULKRTO")
	public Job BulkRTO(){
		return jobBuilderFactory.get(jobConfigs.getJob_name_bulk_rto())
		.incrementer(new RunIdIncrementer())
		.start(bulkRTO())
		.build();
		
	}
	
	@Bean Step bulkRTO(){
		return stepBuilderFactory.get("bulkRTO")
				.tasklet(bulkRTOTasklet)
				.allowStartIfComplete(true)
				.build();
		
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDCUSTOMERACTIVITY")
	public Job DashboardCustomerActivity(){
		return jobBuilderFactory.get(jobConfigs.getJob_name_dashboard_customer_activity())
		.incrementer(new RunIdIncrementer())
		.start(bICustomerActivity())
		.build();
		
	}
	
	@Bean
	Step bICustomerActivity(){
		return stepBuilderFactory.get("bICustomerActivity")
				.tasklet(bICustomerActivityTasklet)
				.allowStartIfComplete(true)
				.build();
	}
	
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDCUSTOMERCOD")
	public Job DashboardCustomerCOD(){
		return jobBuilderFactory.get(jobConfigs.getJob_name_dashboard_customer_cod())
		.incrementer(new RunIdIncrementer())
		.start(bICustomerCOD())
		.build();
		
	}
	
	@Bean
	Step bICustomerCOD(){
		return stepBuilderFactory.get("bICustomerCOD")
				.tasklet(bICustomerCODTasklet)
				.allowStartIfComplete(true)
				.build();
	}
	
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDLINKEDACCOUNTS")
	public Job DashboardLinkedAccounts(){
		return jobBuilderFactory.get(jobConfigs.getJob_name_dashboard_linked_accounts())
		.incrementer(new RunIdIncrementer())
		.start(bILinkedAccounts())
		.build();
		
	}
	
	@Bean
	Step bILinkedAccounts(){
		return stepBuilderFactory.get("bILinkedAccounts")
				.tasklet(bILinkedAccountsTasklet)
				.allowStartIfComplete(true)
				.build();
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.DASHBOARDCODTHROTTLING")
	public Job DashboardCODThrottling(){
		return jobBuilderFactory.get(jobConfigs.getJob_name_dashboard_cod_throttling())
		.incrementer(new RunIdIncrementer())
		.start(bICODThrottling())
		.build();
		
	}
	
	@Bean
	Step bICODThrottling(){
		return stepBuilderFactory.get("bICODThrottling")
				.tasklet(bICODThrottlingTasklet)
				.allowStartIfComplete(true)
				.build();
	}
	
	
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.CUSTOMERACTIVITYVALIDATION")
	public Job CustomerActivityValidation(){
		return jobBuilderFactory.get(jobConfigs.getJob_name_customer_activity_validation())
		.incrementer(new RunIdIncrementer())
		.start(customerActivityValidation())
		.build();
		
	}
	
	@Bean
	Step customerActivityValidation(){
		return stepBuilderFactory.get("customerActivityValidation")
				.tasklet(customerActivityValidationTasklet)
				.allowStartIfComplete(true)
				.build();
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.LINKEDACCOUNTSVALIDATION")
	public Job LinkedAccountsValidation(){
		return jobBuilderFactory.get(jobConfigs.getJob_name_linked_accounts_validation())
		.incrementer(new RunIdIncrementer())
		.start(linkedAccountsValidation())
		.build();
		
	}
	
	@Bean
	Step linkedAccountsValidation(){
		return stepBuilderFactory.get("linkedAccountsValidation")
				.tasklet(linkedAccountsValidationTasklet)
				.allowStartIfComplete(true)
				.build();
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.RETURNDETAILS")
	ReturnDetails returnDetails(){
		return new ReturnDetails();
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.LINKEDACCOUNTSBULK")
	LinkedAccoutsDetails linkedAccountsDetails(){
		return new LinkedAccoutsDetails();
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.BULKCODSCRIPT")
	BulkCODScript BulkCODScript(){
		return new BulkCODScript();
	}
	
	@Bean
	@ConditionalOnProperty(name="job.enabled.CUSTOMERACTIVITYVALIDATIONBULK")
	CustomerActivityValidationBulk customerActivityValidationBulk(){
		return new CustomerActivityValidationBulk();
	}
	
	@PostConstruct
	public void init(){
		Resource initSchema1 = new ClassPathResource("delete-batch.sql");
		//Resource initSchema2 = new ClassPathResource("org/springframework/batch/core/schema-mysqldb.sql");
		DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema1);
		
		try{
		DatabasePopulatorUtils.execute(databasePopulator, dataSource());
		}
		catch(ScriptStatementFailedException e){
			System.out.println("Exception ecountered while deleting tables");
		}
		
//		databasePopulator = new ResourceDatabasePopulator(initSchema2);
//		
//		DatabasePopulatorUtils.execute(databasePopulator, dataSource());
		
//		insertDummyRecords();
	}
//		@Autowired
//		CODThrottlingRepository codThrottlingRepository;
//		
//		@Autowired
//		LinkedAccountsRepository linkedAccountsRepository;
//		
//		@Autowired
//		BatchTimestampsRepository batchTimestampsRepository;
	
	

	@Bean
	JpaTransactionManager transactionManager(){
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		return transactionManager;
	}
	
	@Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setHibernateProperties(hibernateProperties());
        return sessionFactory;
     }
	
	
	 private Properties hibernateProperties() {
	        Properties properties = new Properties();
	        properties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
	        properties.put("hibernate.show_sql", true);
	        properties.put("hibernate.format_sql", true);
	        return properties;        
	    }

}
