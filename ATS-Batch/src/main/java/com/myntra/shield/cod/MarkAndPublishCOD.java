package com.myntra.shield.cod;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.myntra.ats.blocked.client.ATSClient;
import com.myntra.ats.blocked.client.response.OutstandingCODResponse;
import com.myntra.ats.blocked.client.response.UserDetailsResponse;
import com.myntra.ats.user.client.request.UserDetailsRequest;
import com.myntra.idea.client.IdeaClient;
import com.myntra.idea.response.ProfileResponse;
import com.myntra.shield.domain.CODReason;
import com.myntra.shield.entities.CODThrottlingIncBulk;
import com.myntra.shield.enums.MessageType;
import com.myntra.shield.exception.ShieldException;
import com.myntra.shield.messages.Message;
import com.myntra.shield.messages.MessageDetails;
import com.myntra.shield.queue.QueueClient;
import com.myntra.shield.repositories.CODThrottlingIncBulkRepository;
import com.myntra.shield.utils.Utils;

@Component
public class MarkAndPublishCOD {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MarkAndPublishCOD.class);
	
	@Value("${labels.label1}")
	String label1;
	
	@Value("${labels.label2}")
	String label2;
	
	@Value("${labels.label6}")
	String label6;
	
	@Value("${labels.label7}")
	String label7;
	
	@Value("${labels.label9}")
	String label9;
	
	
	@Value("${labels.value1}")
	String value1;
	@Value("${labels.value2}")
	String value2;
	@Value("${labels.value6}")
	String value6;
	@Value("${labels.value7}")
	String value7;
	@Value("${labels.value9}")
	String value9;
	
	Map<String, String> labelsTOValues;
	Map<String, String> valuesTOLabels;
	
	@Value("${publishCODToQueueEnabled}")
	Boolean publishCODToQueueEnabled;
	
	@Value("${updateShieldEnabled}")
	Boolean updateShieldEnabled;
	
	@Value("${validationEnabled}")
	Boolean validationEnabled;
	
	@Value("${updateNewCustomerCOD}")
	Boolean updateNewCustomerCOD;
	
	@Value("${writeToMysqlEnabled}")
	Boolean writeToMysqlEnabled;
	
	@Autowired
	CODThrottlingIncBulkRepository repository;
	
	@Autowired
	ATSClient atsClient;
	
	@Autowired
	QueueClient queueClient;
	
	@Autowired
	IdeaClient ideaClient;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@PostConstruct
	public void init(){
		
		labelsTOValues = new HashMap();
		labelsTOValues.put(label1, value1);
		labelsTOValues.put(label2, value2);
		labelsTOValues.put(label6, value6);
		labelsTOValues.put(label7, value7);
		labelsTOValues.put(label9, value9);

		valuesTOLabels = new HashMap();
		valuesTOLabels.put(value1, label1);
		valuesTOLabels.put(value2, label2);
		valuesTOLabels.put(value6, label6);
		valuesTOLabels.put(value7, label7);
		valuesTOLabels.put(value9, label9);
	}
	
	
	
	@SuppressWarnings("static-access")
	public void markandpublishCOD(String email, Integer ttl_transactions, Double rto_shipped, Double ttl_shipped, Double rto_rev, Double ttl_rev, MessageType msgType){
		String uidx = null;
		try{
			ProfileResponse profile = ideaClient.getByEmail("myntra", email);
			if(profile != null && profile.getEntry() != null);
				uidx = profile.getEntry().getUidx();
				
		}catch(Exception e){
			LOGGER.error("Exception encountered while fetching uidx from idea by email: {}", email);
		}
		if(uidx == null)
			return;
		
		Double rto_perc_by_rev;
		Double rto_perc_by_vol;
		String cod = null;
		if(ttl_rev.equals(Double.valueOf(0.0)))
			rto_perc_by_rev = Double.valueOf(0.0);
		else 
			rto_perc_by_rev = rto_rev/ttl_rev;
		
		if(ttl_shipped.equals(Double.valueOf(0.0)))
			rto_perc_by_vol = Double.valueOf(0.0);
		else 
			rto_perc_by_vol = rto_shipped/ttl_shipped;
		
		if(ttl_transactions >= 5){
			
			if(rto_perc_by_rev > 0.4d)
				cod = label1;
			else if(rto_perc_by_rev > 0.3d)
				cod = label2;
			else if(rto_perc_by_rev > 0.05d)
				cod = label7;
			else cod = label6;
			
		}
		else if(ttl_transactions == 4){
			
			if(rto_perc_by_vol > 0.75)
				cod = label1;
			else if(rto_perc_by_vol > 0.5)
				cod = label2;
			else 
				cod = label7;
			
		}
		else if(ttl_transactions >=2){
			if(rto_perc_by_vol > 0.66)
				cod = label1;
			else 
				cod = label7;
		
		}
		else if(ttl_transactions ==1 )
			cod = label7;
		
		rto_perc_by_vol = rto_perc_by_vol *100;
		
		rto_perc_by_rev = rto_perc_by_rev * 100;
		double rto_per_vol_round = Math.round(rto_perc_by_vol * 100.0) / 100.0;
		double rto_per_rev_round = Math.round(rto_perc_by_rev * 100.0) / 100.0;
		
//		if(validationEnabled)
//			validateCODDetailsFromLabel(uidx, cod, String.valueOf(rto_per_vol_round), String.valueOf(rto_per_rev_round));
		
		markandpublishCODDetails(uidx, cod, rto_per_vol_round, rto_per_rev_round, ttl_transactions, msgType);
	}


	


	private void markandpublishCODDetails(String uidx, String cod, double rto_per_vol_round, double rto_per_rev_round,
			Integer ttl_transactions, MessageType msgType) {
		if(ttl_transactions == 1){
			UserDetailsResponse atsResp = atsClient.getAbuserDetails(uidx);
			if(atsResp.getMaxCOD().equals(Double.valueOf("1")) || atsResp.getMaxCOD().equals(Double.valueOf("2"))  ||  atsResp.getMaxCOD().equals(Double.valueOf("8"))){
				LOGGER.debug("Ignoring the user and not setting cod since it is marked because of linked accounts. Uidx: {}", uidx);
				return;
			}
		}
		
		markandpublishCODDetails(uidx, cod, rto_per_vol_round, rto_per_rev_round, msgType);
		
	}



	@SuppressWarnings("static-access")
	public void markandpublishCOD(String uidx, String cod, String rto_vol_perc, String rto_rev_perc, String transactions, MessageType msgType) {
		Double rto_perc_by_vol = null;
		Double rto_perc_by_rev = null;
		
		if(rto_vol_perc != null)
			rto_perc_by_vol = Double.valueOf(rto_vol_perc) *100;
		if(rto_rev_perc != null)
			rto_perc_by_rev = Double.valueOf(rto_rev_perc) *100;
		
		Double rto_per_vol_round = null;
		if(rto_perc_by_vol != null)
			rto_per_vol_round = Math.round(rto_perc_by_vol * 100.0) / 100.0;
		
		Double rto_per_rev_round = null;
		if(rto_perc_by_rev != null)
			rto_per_rev_round = Math.round(rto_perc_by_rev * 100.0) / 100.0;
		
		markandpublishCODDetails(uidx, cod, rto_per_vol_round, rto_per_rev_round, Integer.valueOf(transactions), msgType);
		
//		markandpublishCODDetails(uidx, cod, rto_per_vol_round, rto_per_rev_round, msgType);
		
	}
	
	@SuppressWarnings("unchecked")
	public void markandpublishCODDetails(String uidx, String cod, double rto_per_vol_round, double rto_per_rev_round, MessageType msgType) {
		
		UserDetailsRequest request = new UserDetailsRequest();
		request.setLogin(uidx);
		request.setMaxCOD(cod);
			
		if(cod.equals("1") || cod.equals("2")){
			request.setReasonRTOAbuser(CODReason.COD102.toString());
//			jsonParam.put("reasonRTOAbuser", CODReason.COD102.toString());
		}
		else if(cod.equals("6")){
			request.setReasonRTOAbuser(CODReason.COD105.toString());
//			jsonParam.put("reasonRTOAbuser", CODReason.COD105.toString());
		}
		else if(cod.equals("7")){
			request.setReasonRTOAbuser(CODReason.COD106.toString());
//			jsonParam.put("reasonRTOAbuser", CODReason.COD106.toString());
		}
		else if(cod.equals("9")){
			request.setReasonRTOAbuser(CODReason.COD107.toString());
//			jsonParam.put("reasonRTOAbuser", CODReason.COD101.toString());
		}
		request.setIsCODThrottled(true);
//		jsonParam.put("isCODThrottled", Boolean.valueOf(true));
		
		LOGGER.debug("Data to be updated in Shield. COD details: {}, {}, {}, {}", request.getLogin(), request.getMaxCOD(), request.getReasonRTOAbuser(), request.getIsCODThrottled());
		if(updateShieldEnabled){
			try {
				if(!validateCODDetailsFromLabel(uidx,  cod, String.valueOf(rto_per_vol_round), String.valueOf(rto_per_rev_round)))
					atsClient.setCODLimit(request);
			} catch (UnsupportedEncodingException e) {
				LOGGER.error("Exception while marking cod. Details : {}, {}" + uidx, cod );
				e.printStackTrace();
			}
		}
		
		if(writeToMysqlEnabled){
			try{
			writeToMysql(uidx, cod, rto_per_vol_round, rto_per_rev_round );
			}catch (Exception e) {
				e.printStackTrace();
				LOGGER.error("Error while writing to mysql: {}", uidx, e.getMessage());
			}
			
		}
		
		if(!publishCODToQueueEnabled){
			return;
		}
		
		Message msg = new Message();
		msg.setMessageType(msgType);
		MessageDetails msgDetails = new MessageDetails();
		msgDetails.setUidx(uidx);
		
		if(cod.equals("1"))
			msgDetails.setMinCod(Double.valueOf(0.0));
		else
			msgDetails.setMinCod(Double.valueOf(299.0));
		
		msgDetails.setMaxCod(Double.valueOf(labelsTOValues.get(cod)));
		
		if(cod.equals("1") || cod.equals("2")){
			msgDetails.setIsRTOAbuser(Boolean.valueOf(true));
			msgDetails.setCodReason(CODReason.COD102.toString());
		}
		else if(cod.equals("6")){
			msgDetails.setIsRTOAbuser(Boolean.valueOf(false));
			msgDetails.setCodReason(CODReason.COD105.toString());
		}
		else if(cod.equals("7")){
			msgDetails.setIsRTOAbuser(Boolean.valueOf(false));
			msgDetails.setCodReason(CODReason.COD106.toString());
		}
		else if(cod.equals("9")){
			msgDetails.setIsRTOAbuser(Boolean.valueOf(false));
			msgDetails.setCodReason(CODReason.COD107.toString());
		}
		
		msgDetails.setRtoPercentageByRevenue(rto_per_rev_round);
		msgDetails.setRtoPercentageByVolume(rto_per_vol_round);
		LOGGER.debug("msgDetails: {}", msgDetails);
		
		msg.setMessageDetails(msgDetails);
		
		if(!publishCODToQueueEnabled){
			return;
		}
		
		try {
			queueClient.sendCODThrottlingMessage(msg);
		} catch (ShieldException e) {
			//TODO addalert
			LOGGER.error("Exception while publishing to queue. Message: " + msg.getMessageDetails().toString() +". Exception: " + e.getMessage());
		}
		
	}

	private void writeToMysql(String uidx, String cod, double rto_per_vol_round, double rto_per_rev_round) {
		CODThrottlingIncBulk entity = new CODThrottlingIncBulk();
		entity.setUidx(uidx);
		if(cod.equals("1"))
			entity.setMinCod(Double.valueOf(0.0));
		else
			entity.setMinCod(Double.valueOf(299.0));
		
		entity.setMaxCod(Double.valueOf(labelsTOValues.get(cod)));
		
		if(cod.equals("1") || cod.equals("2")){
			entity.setIsRtoAbuser(Boolean.valueOf(true));
			entity.setCodReason(CODReason.COD102.toString());
		}
		else if(cod.equals("6")){
			entity.setIsRtoAbuser(Boolean.valueOf(false));
			entity.setCodReason(CODReason.COD105.toString());
		}
		else if(cod.equals("7")){
			entity.setIsRtoAbuser(Boolean.valueOf(false));
			entity.setCodReason(CODReason.COD106.toString());
		}
		else if(cod.equals("9")){
			entity.setIsRtoAbuser(Boolean.valueOf(false));
			entity.setCodReason(CODReason.COD107.toString());
		}
		
		entity.setRtoPercentageByRevenue(rto_per_rev_round);
		entity.setRtoPercentageByVolume(rto_per_vol_round);
		
		entity.setTimestamp(Utils.getTimestamp());
		
		repository.save(entity);
		
	}



	public Boolean validateCODDetailsFromLabel(String uidx, String label, String rto_vol_perc, String rto_rev_perc){
		String cod = labelsTOValues.get(label);
		
		LOGGER.debug("BI data. uidx: {}, rto_perc_vol: {}, rto_rev_perc: {}, cod: {}, transactions: {}", uidx, rto_vol_perc, rto_rev_perc, cod);
		Double codTobeValidated = Double.valueOf(cod);
		
		OutstandingCODResponse response = atsClient.getOutstandingCODLimit(uidx);
		if(response != null && codTobeValidated != null){
			if(Double.valueOf(codTobeValidated).equals(Double.valueOf(49999)))
				codTobeValidated = 100000.0d;// cumulative cod limit
			if(!Double.valueOf(codTobeValidated).equals(response.getOutstandingCODLimit())){
				LOGGER.warn("Mismatch occurred in script while validating cod details for uidx: {}. Expected cod is {}. ATS cod is {}. rto_perc_vol: {}, rto_rev_perc: {}, transactions: {}", uidx,codTobeValidated, response.getOutstandingCODLimit(), rto_vol_perc, rto_rev_perc);
				return false;
			}
		}
		return true;
	}



	@SuppressWarnings("static-access")
	public void validateFakeEmail(String uidx, Boolean fakeEmail) {
		UserDetailsResponse response = atsClient.getUserDetails(uidx);
		if(!fakeEmail.equals(response.getIsFakeEmail()))
			LOGGER.warn("Mismatch occurred in Customer Activity Validation script while validating email details for uidx: {}. BI email is {}. ATS fake email is {}.", uidx,fakeEmail, response.getIsFakeEmail());
		
	}

	@SuppressWarnings("static-access")
	public void validateLinkedAccountsDetails(String uidx, String linkedBy, String abuserType) {
		try{
			if(StringUtils.equals(abuserType, "RETURN_ABUSER")){
				UserDetailsResponse response = atsClient.getUserDetails(uidx);
				if(!response.getIsReturnAbuser()){
					LOGGER.warn("Mismatch in LA validation script for uidx: {}. BI abuser type: {}. ATS isRA: {}", uidx, abuserType, response.getIsReturnAbuser());
				}
			}
			else if(StringUtils.equals(abuserType, "RTO_ABUSER")){
				OutstandingCODResponse response = atsClient.getOutstandingCODLimit(uidx);
				if(!(response.getOutstandingCODLimit().equals(Double.valueOf(0.0)) || response.getOutstandingCODLimit().equals(Double.valueOf(1500)))){
					LOGGER.warn("Mismatch in LA validation script for uidx: {}. BI abuser type: {}. ATS cod : {}", uidx, abuserType, response.getOutstandingCODLimit());
				}
				
			}
			else if(StringUtils.equals(abuserType, "BLACKLISTED")){
				ProfileResponse response = ideaClient.getByUidx("myntra", uidx);
				if(response!= null && response.getEntry() != null){
					String status = response.getEntry().getStatus();
					if(!(StringUtils.equals(status, "BLACKLISTED"))){
						LOGGER.warn("Mismatch in LA validation script for uidx: {}. BI abuser type: {}. Idea status : {}", uidx, abuserType, status);
					}
				}
				
			}
		}catch(Exception e){
			//TODO addalert
			LOGGER.error("Exception while validating linked accounts details. {}", e);
		}
		
	}



	public Boolean validateCODDetails(String uidx, String maxCod, String rto_perc_vol, String rto_perc_rev) {
		Double codTobeValidated = Double.valueOf(maxCod);
		
		OutstandingCODResponse response = atsClient.getOutstandingCODLimit(uidx);
		if(response != null && codTobeValidated != null){
			if(Double.valueOf(codTobeValidated).equals(Double.valueOf(49999)))
				codTobeValidated = 100000.0d;// cumulative cod limit
			if(!Double.valueOf(codTobeValidated).equals(response.getOutstandingCODLimit())){
				LOGGER.warn("Mismatch occurred in script while validating cod details for uidx: {}. Expected cod is {}. ATS cod is {}. rto_perc_vol: {}, rto_perc_rev: {}", uidx,codTobeValidated, response.getOutstandingCODLimit(), rto_perc_vol, rto_perc_rev);
				return false;
			}
		}
		return true;
		
	}

}
