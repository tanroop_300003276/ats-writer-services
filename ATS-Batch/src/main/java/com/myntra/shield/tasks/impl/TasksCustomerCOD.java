package com.myntra.shield.tasks.impl;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.zip.GZIPOutputStream;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.csvreader.CsvWriter;
import com.myntra.shield.entities.CODThrottling;
import com.myntra.shield.entities.CustomerActivity;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.services.S3;
import com.myntra.shield.tasks.*;

@Component
public class TasksCustomerCOD extends CommonFields implements BatchTasks<T>,S3Tasks<T>, WriteFileTasks{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TasksCustomerCOD.class);
	
	@Autowired
	private EntityManager em;
	
	List<CODThrottling> codResult;

	private String s3FileName;

	public void setCustActivityResult(List<CODThrottling> codResult) {
		this.codResult = codResult;
	}

	@Override
	public String getJobName() {
		return jobConfigs.getJob_name_dashboard_customer_cod();
	}
	
	@Override
	public String getS3Dir(){
		String dir =  jobConfigs.getS3_dir_dashboard_customer_cod();
		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);
		int month = now.get(Calendar.MONTH) + 1; // Note: zero based!
		int day = now.get(Calendar.DAY_OF_MONTH);
		
		String m = "";
		if(month <10)
			m = "0";
		
		return dir + year + "/" + m + month + "/" + day + "/";
	}
	
	@Override
	public String getCurrentTimestamp() {
		return this.currentTimeStamp;
	}

	@Override
	public String getLastRunTimestamp() {
		return this.lastRunTimeStamp;
	}

	@Override
	public BatchTimestampsRepository getBatchRepository() {
		return this.batchTimestampsRepository;
	}

	@Override
	public void setCurrentTimestamp(String timestamp) {
		this.currentTimeStamp = timestamp;
		
	}

	@Override
	public void setLastRunTimestamp(String timestamp) {
		this.lastRunTimeStamp = timestamp;
	}

	@SuppressWarnings("deprecation")
	@Override
	public void writeToFile() throws IOException, Exception {
		
		LOGGER.debug("writeToFile using currentTimeStamp: " + currentTimeStamp );
		
		s3FileName = jobConfigs.getS3FilenameCustomerCOD();
		
		CsvWriter csvOutput = new CsvWriter(new FileWriter(s3FileName.toString(), false), ',');
		
		String[] entries = {"key","value","userid","timestamp","updatedby","remarks","rto_counter", "idcustomer", "idcustomer_idea"};
		csvOutput.writeRecord(entries);

		
		for(CODThrottling activity : codResult){
			//CustomerActivity activity = CustomerActivity.class.cast(act); 
			try{
				csvOutput.write("maxCODLimit");
				
				if(activity.getMaxCod() != null)
					csvOutput.write(activity.getMaxCod().toString());
				else
					csvOutput.write(null);
				
				csvOutput.write(activity.getUidx());
				
				csvOutput.write(currentTimeStamp);
				csvOutput.write("SHIELD");
				csvOutput.write(activity.getCodReason());
				csvOutput.write(null);
				csvOutput.write(null);
				csvOutput.write(null);
				
				csvOutput.endRecord();
			}catch(Exception e){
				
				//TODO addalert
				LOGGER.error("Exception while writing record to customer cod file. {}", e.getMessage());
			}
		}
		csvOutput.close();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void getResults() {
		
		Query q = em.createNativeQuery(jobConfigs.getQueryCustomerCOD(), CODThrottling.class);
		q.setParameter("lastRunTimeStamp", lastRunTimeStamp);
		q.setParameter("currentTimeStamp", currentTimeStamp);
		setCustActivityResult((List<CODThrottling>) q.getResultList());
		LOGGER.debug("CustomerCOD Results count: "+ codResult.size());
	}

	@Override
	public S3 getS3() {
		return this.s3;
	}

	@Override
	public String getS3filename() {
		String filename = this.s3FileName;
		String compressedFileName = filename + ".gz";
		compressGzipFile(filename, compressedFileName);
		return compressedFileName;
	}
	
	private static void compressGzipFile(String file, String gzipFile) {
        try {
            FileInputStream fis = new FileInputStream(file);
            FileOutputStream fos = new FileOutputStream(gzipFile);
            GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
            byte[] buffer = new byte[1024];
            int len;
            while((len=fis.read(buffer)) != -1){
                gzipOS.write(buffer, 0, len);
            }
            //close resources
            gzipOS.close();
            fos.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("Exception while compressing customer cod file. {}", e.getMessage());
        }
	}

	

}
