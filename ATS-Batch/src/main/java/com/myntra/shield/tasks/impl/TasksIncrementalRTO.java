package com.myntra.shield.tasks.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.myntra.shield.cod.MarkAndPublishCOD;
import com.myntra.shield.enums.MessageType;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.services.DDP;
import com.myntra.shield.tasks.BatchTasks;
import com.myntra.shield.tasks.QueryBITasks;

@Component
public class TasksIncrementalRTO<T> extends CommonFields implements BatchTasks<T>, QueryBITasks{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TasksIncrementalRTO.class);
	
	String downloadedFilename;
	
	@Value("${useOldQid.IncrementalRTO}")
	Boolean useOldQid;
	
	@Value("${oldQid.IncrementalRTO}")
	String oldQid;
	
	@Override
	public String getJobName() {
		return jobConfigs.getJob_name_incremental_rto();
	}
	
	@Override
	public String getCurrentTimestamp() {
		return this.currentTimeStamp;
	}

	@Override
	public String getLastRunTimestamp() {
		return this.lastRunTimeStamp;
	}

	@Override
	public BatchTimestampsRepository getBatchRepository() {
		return this.batchTimestampsRepository;
	}

	@Override
	public void setCurrentTimestamp(String timestamp) {
		this.currentTimeStamp = timestamp;
	}

	@Override
	public void setLastRunTimestamp(String timestamp) {
		this.lastRunTimeStamp = timestamp;
	}

	@Override
	public void setTimestamps() {
		updateCurrentTimestamp();
		updateLastRunTimestamp();
	}
	
	@Autowired
	MarkAndPublishCOD markAndPublishCOD;	
	
	@Override
	public void processResults() throws IOException, InterruptedException {
		InputStream in = new GZIPInputStream(new FileInputStream(downloadedFilename));
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(reader);
		String line = br.readLine(); 
		ExecutorService executorService = Executors.newFixedThreadPool(threads);
		while (line != null) { 
			final String[] userdetails = line.replace("\"", "").split(",");
//			try{
//			setCODLimit(userdetails);
//			}catch(Exception e){
//				LOGGER.error("Exception while forming cod data from " + line, e);
//			}
			executorService.execute(new Runnable() {
			    public void run() {
			    	try{
						setCODLimit(userdetails);
					}catch(Exception e){
							LOGGER.error("Exception while forming cod data from {}", userdetails, e);
					}
			    }
			});
			
			 line = br.readLine(); 
		} 
		executorService.shutdown();
		while(true){
			LOGGER.debug("Polling for all threads in executor service to finish...");
			boolean finshed = executorService.awaitTermination(1, TimeUnit.MINUTES);
			LOGGER.debug("ExecutorService status: " + finshed);
			if(finshed)
				break;
		}
		 
		 br.close();
		 return;
	}

	protected void setCODLimit(String[] userdetails) {
		if(userdetails == null)
			return;
		String email = userdetails[0];
		Integer ttl_transactions = null;
		Double rto_shipped = null;
		Double ttl_shipped = null;
		Double rto_rev = null;
		Double ttl_rev = null;
		if(userdetails[10] != null && !userdetails[10].isEmpty())
			ttl_transactions = Integer.valueOf(userdetails[10]);
		
		if(userdetails[9] != null && !userdetails[9].isEmpty())
			rto_shipped = Double.valueOf(userdetails[9]);
		
		if(userdetails[8] != null && !userdetails[8].isEmpty())
			ttl_shipped = Double.valueOf(userdetails[8]);
		
		if(userdetails[6] != null && !userdetails[6].isEmpty())
			rto_rev = Double.valueOf(userdetails[6]);
		
		if(userdetails[7] != null && !userdetails[7].isEmpty())
			ttl_rev = Double.valueOf(userdetails[7]);
		
		if(ttl_transactions == null || rto_shipped == null ||  ttl_shipped == null || rto_rev == null || ttl_rev == null){
			LOGGER.info("The data is not enough for user: {}. Hence ignoring this record.", email);
			return;
		}
		
		markAndPublishCOD.markandpublishCOD(email, ttl_transactions, rto_shipped, ttl_shipped, rto_rev, ttl_rev, MessageType.COD_THROTTLING_INCREMENTAL);
	}

	@Override
	public DDP getDDP() {
		return this.ddp;
	}

	@Override
	public String getQuery() {
		return jobConfigs.getQueryIncrementalRto();
	}

	@Override
	public String getQueryName() {
		return jobConfigs.getQueryNameIncrementalRto();
	}

	@Override
	public String getQueryDescription() {
		return jobConfigs.getQueryDescIncrementalRto();
	}

	@Override
	public void setQid(String qid) {
		this.qid = qid;
	}

	@Override
	public String getQid() {
		return this.qid;
	}

	@Override
	public String getDownloadDirectory() {
		return jobConfigs.getQueryResultsDIRIncrementalRTO();
	}

	@Override
	public String getFilename() {
		return jobConfigs.getQueryResultsFilenameIncrementalRTO();
	}

	@Override
	public void setDownloadedFilename(String downloadedFilename) {
		this.downloadedFilename = downloadedFilename;
		
	}
	
	@Override
	public Boolean useOldQuery(){
		return this.useOldQid;
	}
	
	@Override
	public String getOldQuery(){
		return this.oldQid;
	}

}
