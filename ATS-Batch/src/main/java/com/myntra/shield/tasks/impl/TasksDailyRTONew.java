package com.myntra.shield.tasks.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.myntra.shield.JobConfigs;
import com.myntra.shield.cod.MarkAndPublishCOD;
import com.myntra.shield.enums.MessageType;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.services.DDP;
import com.myntra.shield.tasks.BatchTasks;
import com.myntra.shield.tasks.QueryBITasks;

@Component
public class TasksDailyRTONew<T> implements BatchTasks<T>, QueryBITasks{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TasksDailyRTONew.class);
	
	String currentTimeStamp;
	
	String lastRunTimeStamp;
	
	String qid;
	
	@Value("${IncrementalRTO.queryResultsFilename}")
	String filename;
	
	@Value("${IncrementalRTO.queryResultsDIR}")
	String downloadDirectory;
	
	@Autowired
	DDP ddp;
	
	@Autowired
	BatchTimestampsRepository batchTimestampsRepository;
	
	@Value("${IncrementalRTO.query}")
	String queryIncrementalRTO;
	
	@Value("${IncrementalRTO.queryName}")
	String queryNameIncrementalRTO;
	
	@Value("${IncrementalRTO.queryDescription}")
	String queryDescriptionIncrementalRTO;
	
	@Autowired
	JobConfigs jobConfigs;
	
	@Override
	public String getJobName() {
		return jobConfigs.getJob_name_incremental_rto();
	}
	
	@Override
	public String getCurrentTimestamp() {
		return this.currentTimeStamp;
	}

	@Override
	public String getLastRunTimestamp() {
		return this.lastRunTimeStamp;
	}

	@Override
	public BatchTimestampsRepository getBatchRepository() {
		return this.batchTimestampsRepository;
	}

	@Override
	public void setCurrentTimestamp(String timestamp) {
		this.currentTimeStamp = timestamp;
	}

	@Override
	public void setLastRunTimestamp(String timestamp) {
		this.lastRunTimeStamp = timestamp;
	}

	@Override
	public void setTimestamps() {
		updateCurrentTimestamp();
		updateLastRunTimestamp();
	}
	
	@Autowired
	MarkAndPublishCOD markAndPublishCOD;	
	
	@Override
	public void processResults() throws IOException, InterruptedException {
		InputStream in = new GZIPInputStream(new FileInputStream(filename));
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(reader);
		String line = br.readLine(); 
		ExecutorService executorService = Executors.newFixedThreadPool(20);
		while (line != null) { 
			final String[] userdetails = line.replace("\"", "").split(",");
//			try{
//			setCODLimit(userdetails);
//			}catch(Exception e){
//				LOGGER.error("Exception while forming cod data from " + line, e);
//			}
			executorService.execute(new Runnable() {
			    public void run() {
			    	try{
						setCODLimit(userdetails);
					}catch(Exception e){
							LOGGER.error("Exception while forming cod data from {}", userdetails, e);
					}
			    }
			});
			
			 line = br.readLine(); 
		} 
		executorService.shutdown();
		while(true){
			LOGGER.debug("Polling for all threads in executor service to finish...");
			boolean finshed = executorService.awaitTermination(1, TimeUnit.MINUTES);
			LOGGER.debug("ExecutorService status: " + finshed);
			if(finshed)
				break;
		}
		 
		 br.close();
		 return;
	}

	protected void setCODLimit(String[] userdetails) {
		if(userdetails == null)
			return;
		String email = userdetails[0];
		Integer ttl_transactions = null;
		Double rto_shipped = null;
		Double ttl_shipped = null;
		Double rto_rev = null;
		Double ttl_rev = null;
		if(userdetails[1] != null && !userdetails[1].isEmpty())
			ttl_transactions = Integer.valueOf(userdetails[1]);
		
		if(userdetails[2] != null && !userdetails[2].isEmpty())
			rto_shipped = Double.valueOf(userdetails[2]);
		
		if(userdetails[3] != null && !userdetails[3].isEmpty())
			ttl_shipped = Double.valueOf(userdetails[3]);
		
		if(userdetails[4] != null && !userdetails[4].isEmpty())
			rto_rev = Double.valueOf(userdetails[4]);
		
		if(userdetails[5] != null && !userdetails[5].isEmpty())
			ttl_rev = Double.valueOf(userdetails[5]);
		
		if(ttl_transactions == null || rto_shipped == null ||  ttl_shipped == null || rto_rev == null || ttl_rev == null){
			LOGGER.info("The data is not enough for user: {}. Hence ignoring this record.", email);
			return;
		}
		
		markAndPublishCOD.markandpublishCOD(email, ttl_transactions, rto_shipped, ttl_shipped, rto_rev, ttl_rev, MessageType.COD_THROTTLING);
	}

	@Override
	public DDP getDDP() {
		return this.ddp;
	}

	@Override
	public String getQuery() {
		return queryIncrementalRTO;
	}

	@Override
	public String getQueryName() {
		return queryNameIncrementalRTO;
	}

	@Override
	public String getQueryDescription() {
		return queryDescriptionIncrementalRTO;
	}

	@Override
	public void setQid(String qid) {
		this.qid = qid;
	}

	@Override
	public String getQid() {
		return this.qid;
	}

	@Override
	public String getDownloadDirectory() {
		return this.downloadDirectory;
	}

	@Override
	public String getFilename() {
		return this.filename;
	}

	@Override
	public void setDownloadedFilename(String fileName) {
		this.filename = fileName;
		
	}

}
