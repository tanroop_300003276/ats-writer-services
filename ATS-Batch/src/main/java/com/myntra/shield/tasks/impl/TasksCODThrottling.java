package com.myntra.shield.tasks.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.csvreader.CsvWriter;
import com.myntra.shield.entities.CODThrottling;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.repositories.CODThrottlingRepository;
import com.myntra.shield.services.S3;
import com.myntra.shield.tasks.*;
import com.myntra.shield.utils.Utils;

@Component
public class TasksCODThrottling extends CommonFields implements BatchTasks<T>, S3Tasks<T>, WriteFileTasks{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TasksCODThrottling.class);
	
	@Value("${filename.dashboard.CODThrottling}")
	String filename;
	
	@Value("${s3.directory.dashboard.CODThrottling}")
	String s3Dir;
	
	@Autowired
	CODThrottlingRepository codThrottlingRepository;
	
	List<CODThrottling> codThrottlingResults;

	@Override
	public String getJobName() {
		return jobConfigs.getJob_name_dashboard_cod_throttling();
	}
	
	@Override
	public String getS3Dir(){
		return this.s3Dir;
	}

	@Override
	public String getCurrentTimestamp() {
		return this.currentTimeStamp;
	}

	@Override
	public String getLastRunTimestamp() {
		return this.lastRunTimeStamp;
	}

	@Override
	public BatchTimestampsRepository getBatchRepository() {
		return this.batchTimestampsRepository;
	}
	
	@Override
	public S3 getS3(){
		return this.s3;
	}

	@Override
	public void setCurrentTimestamp(String timestamp) {
		this.currentTimeStamp = timestamp;
		
	}

	@Override
	public void setLastRunTimestamp(String timestamp) {
		this.lastRunTimeStamp = timestamp;
	}

	@Override
	public void writeToFile() throws IOException, Exception {
		
		LOGGER.debug("writeToFile using currentTimeStamp: " + currentTimeStamp );
		
		String fName = Utils.getFilename(filename, currentTimeStamp);
		CsvWriter csvOutput = new CsvWriter(new FileWriter(fName.toString(), false), ',');
		
		for(CODThrottling result : codThrottlingResults){
			
			csvOutput.write(result.getUidx());
			csvOutput.write(result.getMinCod().toString());
			csvOutput.write(result.getMaxCod().toString());
			csvOutput.write(currentTimeStamp);
			csvOutput.endRecord();
		}
		csvOutput.close();
		
	}

	@Override
	public void getResults() {
		codThrottlingResults = codThrottlingRepository.getCODThrottlingResults(lastRunTimeStamp, currentTimeStamp);
		LOGGER.debug("CodThrottling Results count: "+ codThrottlingResults.size());
	}

	@Override
	public String getS3filename() {
		return this.filename;
	}
	
}
