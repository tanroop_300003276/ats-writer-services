package com.myntra.shield.tasks;

import java.io.IOException;

import org.springframework.stereotype.Component;

@Component
public interface WriteFileTasks{

	void getResults();

	void writeToFile() throws IOException, Exception;
	
}
