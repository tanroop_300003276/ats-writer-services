package com.myntra.shield.tasks.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.csvreader.CsvWriter;
import com.myntra.shield.entities.CustomerActivity;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.services.S3;
import com.myntra.shield.tasks.*;
import com.myntra.shield.utils.Utils;

@Component
public class TasksCustomerActivity extends CommonFields implements BatchTasks<T>,S3Tasks<T>, WriteFileTasks{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TasksCustomerActivity.class);
	
	@Autowired
	private EntityManager em;
	
	List<CustomerActivity> custActivityResult;

	private String s3FileName;

	public void setCustActivityResult(List<CustomerActivity> custActivityResult) {
		this.custActivityResult = custActivityResult;
	}

	@Override
	public String getJobName() {
		return jobConfigs.getJob_name_dashboard_customer_activity();
	}
	
	@Override
	public String getS3Dir(){
		return jobConfigs.getS3_dir_dashboard_customer_activity();
	}
	
	@Override
	public String getCurrentTimestamp() {
		return this.currentTimeStamp;
	}

	@Override
	public String getLastRunTimestamp() {
		return this.lastRunTimeStamp;
	}

	@Override
	public BatchTimestampsRepository getBatchRepository() {
		return this.batchTimestampsRepository;
	}

	@Override
	public void setCurrentTimestamp(String timestamp) {
		this.currentTimeStamp = timestamp;
		
	}

	@Override
	public void setLastRunTimestamp(String timestamp) {
		this.lastRunTimeStamp = timestamp;
	}

	@Override
	public void writeToFile() throws IOException, Exception {
		
		LOGGER.debug("writeToFile using currentTimeStamp: " + currentTimeStamp );
		
		s3FileName = Utils.getFilename(jobConfigs.getS3FilenameCustomerActivity(), currentTimeStamp);
		CsvWriter csvOutput = new CsvWriter(new FileWriter(s3FileName.toString(), false), ',');
		
		for(CustomerActivity activity : custActivityResult){
			//CustomerActivity activity = CustomerActivity.class.cast(act); 
			try{
				csvOutput.write(activity.getUidx());
				
				//profile_id
				csvOutput.write(null);
				
				//registered_mobile
				csvOutput.write(null);
				
				//shipping_mobile	
				csvOutput.write(null);
				
				if(activity.getMinCod() != null)
					csvOutput.write(activity.getMinCod().toString());
				else 
					csvOutput.write(null);
				
				if(activity.getMaxCod() != null)
					csvOutput.write(activity.getMaxCod().toString());
				else
					csvOutput.write(null);
				
				//account_status
				csvOutput.write(null);
				
				if(activity.getNetReturnPercentage() != null)
					csvOutput.write(activity.getNetReturnPercentage().toString());
				else
					csvOutput.write(null);
				
				if(activity.getRtoPercentageByVolume() != null)
					csvOutput.write(activity.getRtoPercentageByVolume().toString());
				else
					csvOutput.write(null);
				
				if(activity.getRtoPercentageByRevenue() != null)
					csvOutput.write(activity.getRtoPercentageByRevenue().toString());
				else
					csvOutput.write(null);
				
				if(activity.getIsReturnAbuser() != null)
					csvOutput.write(activity.getIsReturnAbuser().toString());
				else
					csvOutput.write(null);
				
				if(activity.getIsRtoAbuser() != null)
					csvOutput.write(activity.getIsRtoAbuser().toString());
				else
					csvOutput.write(null);
				
				if(activity.getIsFakeEmail() != null)
					csvOutput.write(activity.getIsFakeEmail().toString());
				else
					csvOutput.write(null);
				
				//coupon_abuser
				csvOutput.write(null);
				//is_blocked
				csvOutput.write(null);
				//is_myntra_employee
				csvOutput.write(null);
				//is_flipkart_emp
				csvOutput.write(null);
				//is_reseller
				csvOutput.write(null);
				//is_reg_mob_verified
				csvOutput.write(null);
				csvOutput.write(activity.getCodReason());
				//coupon_abuse_desc
				csvOutput.write(null);
				//block_reason
				csvOutput.write(null);
				csvOutput.write(currentTimeStamp);
				csvOutput.endRecord();
			}catch(Exception e){
				
				//TODO addalert
				LOGGER.error("Exception while writing record to customer activity file. {}", e.getMessage());
			}
		}
		csvOutput.close();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public void getResults() {
		
		Query q = em.createNativeQuery(jobConfigs.getQueryCustomerActivity(), CustomerActivity.class);
		q.setParameter("lastRunTimeStamp", lastRunTimeStamp);
		q.setParameter("currentTimeStamp", currentTimeStamp);
		setCustActivityResult((List<CustomerActivity>) q.getResultList());
		LOGGER.debug("CustomerActivity Results count: "+ custActivityResult.size());
	}

	@Override
	public S3 getS3() {
		return this.s3;
	}

	@Override
	public String getS3filename() {
		return this.s3FileName;
	}

	

}
