package com.myntra.shield.tasks.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.csvreader.CsvWriter;
import com.myntra.shield.JobConfigs;
import com.myntra.shield.entities.LinkedAccounts;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.repositories.LinkedAccountsRepository;
import com.myntra.shield.services.S3;
import com.myntra.shield.tasks.*;
import com.myntra.shield.utils.Utils;

@Component
public class TasksLinkedAccounts extends CommonFields implements BatchTasks<T>, S3Tasks<T>, WriteFileTasks{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TasksLinkedAccounts.class);
	
	@Autowired
	LinkedAccountsRepository linkedAccountsRepository;
	
	List<LinkedAccounts> linkedAccountsResult;
	
	@Autowired
	JobConfigs jobConfigs;
	
	String s3FileName;

	@Override
	public String getJobName() {
		return jobConfigs.getJob_name_dashboard_linked_accounts();
	}
	
	@Override
	public String getS3Dir(){
		return jobConfigs.getS3_dir_dashboard_linked_accounts();
	}
	
	@Override
	public S3 getS3(){
		return this.s3;
	}

	@Override
	public String getCurrentTimestamp() {
		return this.currentTimeStamp;
	}

	@Override
	public String getLastRunTimestamp() {
		return this.lastRunTimeStamp;
	}

	@Override
	public BatchTimestampsRepository getBatchRepository() {
		return this.batchTimestampsRepository;
	}

	@Override
	public void setCurrentTimestamp(String timestamp) {
		this.currentTimeStamp = timestamp;
		
	}

	@Override
	public void setLastRunTimestamp(String timestamp) {
		this.lastRunTimeStamp = timestamp;
	}

	@Override
	public void writeToFile() throws IOException, Exception {
		
		LOGGER.debug("writeToFile using currentTimeStamp: " + currentTimeStamp );
		
		s3FileName = Utils.getFilename(jobConfigs.getS3FilenameLinkedAccounts(), currentTimeStamp);
		CsvWriter csvOutput = new CsvWriter(new FileWriter(s3FileName.toString(), false), ',');
		
		for(LinkedAccounts accounts : linkedAccountsResult){
			try{
				csvOutput.write(accounts.getUidx());
				csvOutput.write(accounts.getLinkedTo());
				csvOutput.write(accounts.getLinkedBy());
				if(accounts.getLinkedBy() == null)
					csvOutput.write(null);
				else if(accounts.getLinkedBy().equals("DEVICE"))
					csvOutput.write("1");
				else
					csvOutput.write("2");
				csvOutput.write(accounts.getAbuserType());
				csvOutput.write(String.valueOf(accounts.getIsFreeUser()));
				csvOutput.write(currentTimeStamp);
				csvOutput.endRecord();
			}
			catch(Exception e){
				LOGGER.error("Exception while writing record to linked accounts file. {}", e.getMessage());
			}
		}
		csvOutput.close();
		
//		uploadToS3(fName.toString());
		
	}

	@Override
	public void getResults() {
		linkedAccountsResult = linkedAccountsRepository.getLinkedAccounts(lastRunTimeStamp, currentTimeStamp);
		LOGGER.debug("LinkedAccounts Results count: "+ linkedAccountsResult.size());
		
	}

	@Override
	public String getS3filename() {
		return this.s3FileName;
	}


}
