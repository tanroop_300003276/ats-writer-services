package com.myntra.shield.tasks;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.myntra.shield.services.S3;

@Component
public interface S3Tasks<T> {
	
	static final Logger LOGGER = LoggerFactory.getLogger(S3Tasks.class);
	
	public String getS3Dir();
	
	public  S3 getS3();
	
	public String getS3filename();
	
	public default void uploadToS3() throws Exception{
		String s3Dir = this.getS3Dir();
		S3 s3 = this.getS3();
		String filename = this.getS3filename();
		s3.upload(s3Dir, filename);
		File file = new File(filename.toString());
		if(file.delete())
			LOGGER.debug("File deleted successfully: " + filename);
		else
			LOGGER.error("ERROR is deleting file: " + filename);
	}
	
	

}
