package com.myntra.shield.tasks.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.myntra.shield.cod.MarkAndPublishCOD;
import com.myntra.shield.enums.MessageType;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.services.DDP;
import com.myntra.shield.tasks.BatchTasks;
import com.myntra.shield.tasks.QueryBITasks;

@Component
public class TasksBulkRTO<T> extends CommonFields implements BatchTasks<T>, QueryBITasks{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TasksBulkRTO.class);
	
	@Value("${useOldQid.BulkRTO}")
	Boolean useOldQid;
	
	@Value("${oldQid.BulkRTO}")
	String oldQid;
	
	@Value("${validationEnabled}")
	Boolean validationEnabled;
	
	@Override
	public String getJobName() {
		return jobConfigs.getJob_name_bulk_rto();
	}
	
	@Override
	public String getCurrentTimestamp() {
		return this.currentTimeStamp;
	}

	@Override
	public String getLastRunTimestamp() {
		return this.lastRunTimeStamp;
	}

	@Override
	public BatchTimestampsRepository getBatchRepository() {
		return this.batchTimestampsRepository;
	}

	@Override
	public void setCurrentTimestamp(String timestamp) {
		this.currentTimeStamp = timestamp;
	}

	@Override
	public void setLastRunTimestamp(String timestamp) {
		this.lastRunTimeStamp = timestamp;
	}

	@Override
	public void setTimestamps() {
		updateCurrentTimestamp();
		updateLastRunTimestamp();
	}
	
	@Autowired
	MarkAndPublishCOD markAndPublishCOD;	
	
	@Override
	public void processResults() throws IOException, InterruptedException {
		InputStream in = new GZIPInputStream(new FileInputStream(downloadedFilename));
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(reader);
		String line = br.readLine(); 
		ExecutorService executorService = Executors.newFixedThreadPool(threads);
		while (line != null) { 
			final String[] userdetails = line.replace("\"", "").split(",");
//			try{
//			setCODLimit(userdetails);
//			}catch(Exception e){
//				LOGGER.error("Exception while forming cod data from " + line, e);
//			}
			executorService.execute(new Runnable() {
			    public void run() {
			    	try{
						setCODLimit(userdetails);
					}catch(Exception e){
							LOGGER.error("Exception while forming cod data from {}", userdetails, e);
					}
			    }
			});
			
			 line = br.readLine(); 
		} 
		executorService.shutdown();
		while(true){
			LOGGER.debug("Polling for all threads in executor service to finish...");
			boolean finshed = executorService.awaitTermination(1, TimeUnit.MINUTES);
			LOGGER.debug("ExecutorService status: " + finshed);
			if(finshed)
				break;
		}
		 
		 br.close();
		 return;
	}

	protected void setCODLimit(String[] userdetails) {
		if(userdetails == null)
			return;
		String uidx = userdetails[1];
		String transactions = userdetails[2];
		String rto_vol_perc = userdetails[4];
		String rto_rev_perc = userdetails[5];
		String cod = userdetails[6];
		if(cod == null || uidx == null)
			return;
		
		markAndPublishCOD.markandpublishCOD(uidx, cod, rto_vol_perc, rto_rev_perc, transactions, MessageType.COD_THROTTLING_BULK);
	}

	@Override
	public DDP getDDP() {
		return this.ddp;
	}

	@Override
	public String getQuery() {
		return jobConfigs.getQueryBulkRto();
	}

	@Override
	public String getQueryName() {
		return jobConfigs.getQueryNameBulkRto();
	}

	@Override
	public String getQueryDescription() {
		return jobConfigs.getQueryDescBulkRto();
	}

	@Override
	public void setQid(String qid) {
		this.qid = qid;
	}

	@Override
	public String getQid() {
		return this.qid;
	}

	@Override
	public String getDownloadDirectory() {
		return jobConfigs.getQueryResultsDIRBulkRTO();
	}

	@Override
	public String getFilename() {
		return jobConfigs.getQueryResultsFilenameBulkRTO();
	}

	@Override
	public void setDownloadedFilename(String downloadedFilename) {
		this.downloadedFilename = downloadedFilename;
		
	}
	
	@Override
	public Boolean useOldQuery(){
		return this.useOldQid;
	}
	
	@Override
	public String getOldQuery(){
		return this.oldQid;
	}

}
