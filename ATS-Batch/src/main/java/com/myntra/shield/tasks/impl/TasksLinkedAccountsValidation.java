package com.myntra.shield.tasks.impl;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.myntra.shield.JobConfigs;
import com.myntra.shield.cod.MarkAndPublishCOD;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.services.DDP;
import com.myntra.shield.tasks.BatchTasks;
import com.myntra.shield.tasks.QueryBITasks;

@Component
public class TasksLinkedAccountsValidation<T> extends CommonFields implements BatchTasks<T>, QueryBITasks{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TasksLinkedAccountsValidation.class);
	
	String downloadedFilename;
	
	@Autowired
	JobConfigs jobConfigs;
	
	@Override
	public String getJobName() {
		return jobConfigs.getJob_name_linked_accounts_validation();
	}
	
	@Override
	public String getCurrentTimestamp() {
		return this.currentTimeStamp;
	}

	@Override
	public String getLastRunTimestamp() {
		return this.lastRunTimeStamp;
	}

	@Override
	public BatchTimestampsRepository getBatchRepository() {
		return this.batchTimestampsRepository;
	}

	@Override
	public void setCurrentTimestamp(String timestamp) {
		this.currentTimeStamp = timestamp;
	}

	@Override
	public void setLastRunTimestamp(String timestamp) {
		this.lastRunTimeStamp = timestamp;
	}

	@Override
	public void setTimestamps() {
		updateCurrentTimestamp();
		updateLastRunTimestamp();
	}
	
	@Autowired
	MarkAndPublishCOD markAndPublishCOD;	
	
	@Override
	public void processResults() throws IOException, InterruptedException {
		InputStream in = new GZIPInputStream(new FileInputStream(downloadedFilename));
		InputStreamReader reader = new InputStreamReader(in);
		BufferedReader br = new BufferedReader(reader);
		String line = br.readLine(); 
		ExecutorService executorService = Executors.newFixedThreadPool(20);
		while (line != null) { 
			final String[] userdetails = line.replace("\"", "").split(",");
//			try{
//			validateDetails(userdetails);
//			}catch(Exception e){
//				LOGGER.error("Exception while forming cod data from " + line, e);
//			}
			executorService.execute(new Runnable() {
			    public void run() {
			    	try{
			    		validateDetails(userdetails);
					}catch(Exception e){
							LOGGER.error("Exception while forming cod data from {}", userdetails, e);
					}
			    }
			});
//			
			 line = br.readLine(); 
		} 
		executorService.shutdown();
		while(true){
			LOGGER.debug("Polling for all threads in executor service to finish...");
			boolean finshed = executorService.awaitTermination(1, TimeUnit.MINUTES);
			LOGGER.debug("ExecutorService status: " + finshed);
			if(finshed)
				break;
		}
		 
		 br.close();
		 return;
	}

	protected void validateDetails(String[] userdetails) {
		if(userdetails == null)
			return;
		String uidx = userdetails[0];
		Boolean isFree = Boolean.valueOf(userdetails[5]);
		if(isFree)
			return;
		String abuserType = userdetails[4];
		String linkedBy = userdetails[2];
		
		markAndPublishCOD.validateLinkedAccountsDetails(uidx, linkedBy, abuserType);
		
	}

	@Override
	public DDP getDDP() {
		return this.ddp;
	}

	@Override
	public String getQuery() {
		return jobConfigs.getQueryLinkedAccountsValidation();
	}

	@Override
	public String getQueryName() {
		return jobConfigs.getQueryNameLinkedAccountsValidation();
	}

	@Override
	public String getQueryDescription() {
		return jobConfigs.getQueryDescLinkedAccountsValidation();
	}

	@Override
	public void setQid(String qid) {
		this.qid = qid;
	}

	@Override
	public String getQid() {
		return this.qid;
	}

	@Override
	public String getDownloadDirectory() {
		return jobConfigs.getQueryResultsDIRLinkedAccountsValidation();
	}

	@Override
	public String getFilename() {
		return jobConfigs.getQueryResultsFilenameLinkedAccountsValidation();
	}

	@Override
	public void setDownloadedFilename(String downloadedFilename) {
		this.downloadedFilename = downloadedFilename;
		
	}

}
