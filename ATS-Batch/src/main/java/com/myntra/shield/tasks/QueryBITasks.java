package com.myntra.shield.tasks;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.springframework.stereotype.Component;

import com.myntra.shield.services.DDP;

@Component
public interface QueryBITasks {
	
	public DDP getDDP();
	
	public String getQuery();
	
	public String getQueryName();
	
	public String getQueryDescription();
	
	public void setQid(String qid);
	
	public String getQid();
	
	public String getDownloadDirectory();
	
	public String getFilename();
	
	public String getCurrentTimestamp();
	
	public void setDownloadedFilename(String fileName);
	
	public void processResults() throws IOException, InterruptedException;
	
	public default Boolean useOldQuery(){
		return false;
	}
	
	public default String getOldQuery(){
		return null;
	}
	
	default void startQuery() throws IOException, InterruptedException{
		DDP ddp = this.getDDP();
		
		String query = this.getQuery();
		String queryName = this.getQueryName();
		String queryDescription = this.getQueryDescription();
		String qid = null;
		if(this.useOldQuery())
			qid = this.getOldQuery();
		else
			qid = ddp.startQuery(query, queryName, queryDescription);
		this.setQid(qid);
		
		while(true)
		{
			if(ddp.checkQueryStatus(qid).equals("COMPLETED"))
			{
				break;
			}
			Thread.sleep(60000);
		}
		
	}
	
	default void downloadResultsFromDDP() throws IOException{
		DDP ddp = this.getDDP();
		String qid = this.getQid();
		String response = ddp.getDownloadLocation(qid);
		
		downloadFile(response);
	}

	default void downloadFile(String downloadUrl) throws MalformedURLException, IOException{
		String downloadDirectory = this.getDownloadDirectory();
		String filename = this.getFilename();
		String currentTimeStamp = this.getCurrentTimestamp();
		String fileName = downloadDirectory + filename + currentTimeStamp.replace(" ", "_") + ".gz";
		
		BufferedInputStream in = null;
		FileOutputStream fout = null;
		try {
			in = new BufferedInputStream(new URL(downloadUrl).openStream());
			fout = new FileOutputStream(fileName);

			byte data[] = new byte[1024];
			int count;
			while ((count = in.read(data, 0, 1024)) != -1) {
				fout.write(data, 0, count);
			}
		} finally {
			if (in != null)
				in.close();
			if (fout != null)
				fout.close();
		}
		
		setDownloadedFilename(fileName);
	}
	
	

}
