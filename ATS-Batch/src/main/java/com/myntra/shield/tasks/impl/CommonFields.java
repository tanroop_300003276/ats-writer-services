package com.myntra.shield.tasks.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.myntra.shield.JobConfigs;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.services.DDP;
import com.myntra.shield.services.S3;

public abstract class CommonFields {
	
	String currentTimeStamp;
	
	String lastRunTimeStamp;
	
	String qid;
	
	String downloadedFilename;
	
	@Autowired
	DDP ddp;
	
	@Autowired
	S3 s3;
	
	@Autowired
	BatchTimestampsRepository batchTimestampsRepository;
	
	@Autowired
	JobConfigs jobConfigs;
	
	@Value("${threads}")
	int threads;

}
