package com.myntra.shield.tasks;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.myntra.shield.entities.BatchTimestamps;
import com.myntra.shield.repositories.BatchTimestampsRepository;
import com.myntra.shield.utils.Utils;

@Component
public interface BatchTasks<T> {
	
	static final Logger LOGGER = LoggerFactory.getLogger(BatchTasks.class);
	
	public String getJobName();
	
	public String getCurrentTimestamp();
	
	public String getLastRunTimestamp();
	
	public BatchTimestampsRepository getBatchRepository();
	
	public default void setTimestamps(){
		updateCurrentTimestamp();
		updateLastRunTimestamp();
	}
	
	public default void updateLastRunTimestamp(){
		BatchTimestamps timestampObj = this.getBatchRepository().findOne(this.getJobName());
		String lastRunTimeStamp;
		if(timestampObj != null)
			lastRunTimeStamp = timestampObj.getTimestamp();
		else{
			Calendar calendar = Calendar.getInstance();
		    calendar.add(Calendar.DATE, -1);
		    
			lastRunTimeStamp = Utils.getTimestamp(calendar.getTime());
			
		}
		
		this.setLastRunTimestamp(lastRunTimeStamp);
		
	}
	
	public default void updateCurrentTimestamp(){
		
		this.setCurrentTimestamp(Utils.getTimestamp());
		
	}
	
	public default void insertLastRunTimestamp(){
		BatchTimestamps batchTimestamps = new BatchTimestamps();
		batchTimestamps.setJobName(this.getJobName());
		batchTimestamps.setTimestamp(this.getCurrentTimestamp());
		
		this.getBatchRepository().save(batchTimestamps);
	}
	
	public void setCurrentTimestamp(String timestamp);
	
	public void setLastRunTimestamp(String timestamp);

}
