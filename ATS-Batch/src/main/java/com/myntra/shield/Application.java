package com.myntra.shield;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.myntra.ats.blocked.client.ATSClient;
import com.myntra.idea.client.IdeaClient;

@SpringBootApplication(exclude={ RedisAutoConfiguration.class, FreeMarkerAutoConfiguration.class})
public class Application extends SpringBootServletInitializer{
	
private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

	@Value("${ats_service.baseUrl}")
	String baseUrl;
	
	@Value("ats_service.auth.password")
	String authPassword;
	
	@Value("ats_service.auth.user")
	String authUser;
	
	@Value("${idea_service.url}")
	String idea_service_url;
	
	public static void main(String[] args){
		
		LOGGER.debug("Application context starting up...");
		SpringApplication.run(Application.class, args);
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	    return application.sources(Application.class);
	}
	
	@SuppressWarnings("static-access")
	@Bean
	public ATSClient atsClient(){
		ATSClient atsClient = new ATSClient();
		atsClient.setBaseUrl(baseUrl);
		atsClient.setAuthUser(authUser);
		atsClient.setAuthPassword(authPassword);
		atsClient.setTimeout(6000);
		return atsClient;
	}
	
	@Bean
	public IdeaClient ideaClient(){
		return new IdeaClient(idea_service_url);
	}
	
	
	
	
	
}
