package com.myntra.shield.services;

import java.io.File;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.PutObjectRequest;

@Component
public class S3{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(S3.class);
	
	@Value("${s3.endpoint}")
	String s3_endpoint;
	
	@Value("${s3.bucket}")
	String s3_bucket;
	
	@Value("${aws.accessKey}")
	String accessKey;
	
	@Value("${aws.secretKey}")
	String secretKey;
	
	AmazonS3 s3client;
	
	@Value("${uploadToS3Enabled}")
	Boolean uploadToS3Enabled;
	
	@PostConstruct
	public void init(){
		AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
		//Region region = Region.getRegion(Regions.US_EAST_1);
		s3client = new AmazonS3Client(credentials);
		s3client.setEndpoint(s3_endpoint);
		//s3client.setRegion(region);
	}
	
	@SuppressWarnings("deprecation")
	public void upload(String directory, String downloadfile) throws Exception {
		if(!uploadToS3Enabled){
			LOGGER.debug("uploadToS3 is disabled. Hence, not uploading the file");
			return;
		}
		File file = new File(downloadfile);
		
		try {
            LOGGER.debug("Uploading a new object to S3 from a file\n");
            s3client.putObject(new PutObjectRequest(
            		s3_bucket, directory + downloadfile, file));

         } catch (AmazonServiceException ase) {
            LOGGER.debug("Caught an AmazonServiceException, which " +
            		"means your request made it " +
                    "to Amazon S3, but was rejected with an error response" +
                    " for some reason.");
            LOGGER.debug("Error Message:    " + ase.getMessage());
            LOGGER.debug("HTTP Status Code: " + ase.getStatusCode());
            LOGGER.debug("AWS Error Code:   " + ase.getErrorCode());
            LOGGER.debug("Error Type:       " + ase.getErrorType());
            LOGGER.debug("Request ID:       " + ase.getRequestId());
            throw new Exception(ase.getMessage());
        } catch (AmazonClientException ace) {
            LOGGER.debug("Caught an AmazonClientException, which " +
            		"means the client encountered " +
                    "an internal error while trying to " +
                    "communicate with S3, " +
                    "such as not being able to access the network.");
            LOGGER.debug("Error Message: " + ace.getMessage());
            throw new Exception(ace.getMessage());
        }
	    
	    
		
 	}
	
}
