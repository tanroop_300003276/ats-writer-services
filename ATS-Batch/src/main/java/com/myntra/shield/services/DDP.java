package com.myntra.shield.services;

import java.io.IOException;
import java.net.URLEncoder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myntra.shield.domain.DdpQueryResult;
import com.myntra.shield.requests.Request;

@Component
public class DDP {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DDP.class);
	
	@Value("${ddp.userName}")
	String user;
	
	@Value("${ddp.ddpApiHost}")
	String DDP_API_HOST;
	
	@Value("${ddp.ddpSubmitQuery}")
	String DDP_SUBMIT_QUERY;
	
	@Value("${ddp.ddpStatus}")
	String DDP_STATUS;
	
	@Value("${ddp.ddpDownload}")
	String DDP_DOWNLOAD;
	
	
	public String startQuery(String query, String queryName, String queryDescription) throws IOException {
		StringBuilder urlParameters = new StringBuilder();
		
		query = query.replace("+", "%2B");
		
		urlParameters.append("query=");
		urlParameters.append(URLEncoder.encode(query,"UTF-8"));
		urlParameters.append("&");
		urlParameters.append("query-name=");
		urlParameters.append(queryName);
		urlParameters.append("&");
		urlParameters.append("query-description=");
		urlParameters.append(queryDescription);
		urlParameters.append("&");
		urlParameters.append("unload=");
		urlParameters.append("true");
		
		
		StringBuilder url = new StringBuilder(DDP_API_HOST);
		url.append(DDP_SUBMIT_QUERY);
		
		String response = com.myntra.shield.requests.Request.postRequest(url.toString(), urlParameters.toString(), user);
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		
		DdpQueryResult ddpQueryResult = gson.fromJson(response.toString(), DdpQueryResult.class);
		if(ddpQueryResult.getSuccess().equals("false"))
			return null;
		
		return ddpQueryResult.getQueryId();
		
	}


	public String checkQueryStatus(String qid) throws IOException {
		StringBuilder url = new StringBuilder();
		url.append(DDP_API_HOST);
		url.append(qid);
		url.append(DDP_STATUS);
		
		String response = Request.getRequest(url.toString(), user);
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		
		DdpQueryResult ddpQueryResult = gson.fromJson(response.toString(), DdpQueryResult.class);
		return ddpQueryResult.getQueryStatus();
	}


	public String getDownloadLocation(String qid) throws IOException {
		StringBuilder url = new StringBuilder();
		url.append(DDP_API_HOST);
		url.append(qid);
		url.append(DDP_DOWNLOAD);
		
		String response = Request.getRequest(url.toString(), user);
		return response;
	}

}
