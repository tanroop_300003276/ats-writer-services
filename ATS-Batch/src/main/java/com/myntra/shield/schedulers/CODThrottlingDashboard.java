package com.myntra.shield.schedulers;

import java.util.Map;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.myntra.shield.utils.SlackUtil;

@DisallowConcurrentExecution
public class CODThrottlingDashboard extends QuartzJobBean{
	
	private String jobName;
	private JobLauncher jobLauncher;
	private JobLocator jobLocator;
	
	public JobLauncher getJobLauncher() {
		return jobLauncher;
	}
	public void setJobLauncher(JobLauncher jobLauncher) {
		this.jobLauncher = jobLauncher;
	}
	public JobLocator getJobLocator() {
		return jobLocator;
	}
	public void setJobLocator(JobLocator jobLocator) {
		this.jobLocator = jobLocator;
	}
	
	final Logger LOGGER = LoggerFactory.getLogger(CODThrottlingDashboard.class);

	@Override
	protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
		
		LOGGER.debug("CODThrottlingDashboard scheduler started...");
		
		Map<String,Object> mapData = context.getMergedJobDataMap();
		
		jobName = (String) mapData.get("jobName");
		SlackUtil.sendMessage("Job started! Job name: " + jobName);
		JobExecution execution = null;
		try{			
			
			execution = jobLauncher.run(jobLocator.getJob(jobName), new JobParameters());
			LOGGER.debug("Execution Status: "+ execution.getStatus());
			SlackUtil.sendMessage("Job name: " + jobName + " Execution status: " + execution.getStatus());
		}catch(Exception e){
			e.printStackTrace();
			LOGGER.error("Encountered job execution exception! " + e.getMessage());
			SlackUtil.sendMessage("Encountered job execution exception! Job name: " + jobName + " Exception: "+ e.getMessage());
		}
		
	}


}
